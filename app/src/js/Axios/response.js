import axios from "axios";
import User from "./user";
import { Modal, Toast } from "antd-mobile";
import history from "../history";

const alert = Modal.alert;
// ************************添加响应拦截器
axios.interceptors.response.use(
  function(response) {
    // 对响应数据做点什么
    return response;
  },
  function(error) {
    // 对响应错误做点什么
    errorMessage(error);
    return Promise.reject(error);
  }
);

// token失效处理
function errorMessage(error) {
  const status = error.response;
  console.log(status);
  if (status.data.code === 400) {
    console.log("状态码为400")
    alert("温馨提示", `请先登录才能进行查看`, [
      {
        text: "取消",
        style: { color: "red" }
      },
      {
        text: "前往",
        onPress: () => {
          history.push("/login");
        },
        style: { background: "red", color: "#fff" }
      }
    ]);
  } else if (status.response.status === 401) {
    const { u_id } = User.getLoginInfo();
    console.log("998")
    User.clearLoginInfo();
    axios({
      url: "/api/login/refreshToken",
      method: "POST",
      data: { u_id }
    })
      .then(res => {
        console.log(res);
        User.setLoginInfo(res.data.message);
      })
      .catch(e => {});
  }
}

// console.log(error);
// let status = error.response ? error.response.status : 400
// console.log('错误状态码', status)

// const u_id = User.getLoginInfo();
// console.log(u_id);
// // 重新登录
// if (!User.getLoginInfo()) {
//   console.log(111111)
//   history.push("/login");
// }
// if (error.response.status === 401) {

//   User.clearLoginInfo();
//   axios({
//     url: "/api/login/refreshToken",
//     method: "POST",
//     data: { u_id }
//   })
//     .then(res => {
//       console.log(res);
//       User.setLoginInfo(res.data.message);
//     })
//     .catch(e => {});
// }
