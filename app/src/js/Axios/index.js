import axios from "axios";
//导入请求处理
// import "./request";
// 导入响应处理
// import "./response";
import { Icon, List, Modal, Toast } from "antd-mobile";
import User from "./user";
import history from "../history";
const alert = Modal.alert;

class AxiosRequest {
  constructor() {
    this.refreshLock = false;
  }
  // 创建一个请求,参数为请求方式,默认get
  async request(url, method = "GET", token = false, params = {}) {
    let config = {
      headers: {},
      url,
      method
    };
    // 判断请求的方式来把参数放在data里面还是params里面
    if (method === "PUT" || method === "POST" || method === "DELETE") {
      config.data = params;
    } else if (method === "GET") {
      config.params = params || {};
    }
    // 如果需求要token
    if(token){
      const _token = await this.getStorageToken();
      // console.log("_token");
      // console.log(_token);
      config.headers.Authorization = `Bearer ${_token}`;
      try{
        return await axios(config);
      }catch(e){
        console.log(e)
        switch (e.response.status){
          case 400:
          alert(
            "温馨提示",
            `请先登录才能进行查看`,
            [
              {
                text: "取消",
                onPress: () => {
                  history.push('/');
                },
                style: { color: "red" }
              },
              {
                text: "前往",
                onPress: () => {
                  history.push('/login');
                },
                style: { background: "red", color: "#fff" }
              }
            ]
          );

          break;
          case 401:
          console.log("状态401")
          history.push("/login");
          break;
        }
      }
    }else{
      try{
        return await axios(config);
      }catch(e){

      }
    }
  }

  // 本地获取Token,并判断是否过期需要刷新
  async getStorageToken() {
    let token = null;
    // 从本地存储中获取token
    const tokendata = await JSON.parse(localStorage.getItem("token"));
    console.log('tokendata')
    console.log(tokendata)

    // 如果有token
    if(tokendata){
      console.log("token11");
      console.log(tokendata.token);
      //如果刷新时间小于当前时间，向服务端换取 token
      console.log("expires_in");
      console.log(tokendata.expires_in)
      if (tokendata.expires_in <= Math.round(new Date().getTime() /1000)) {
        try{
          const {data} = await axios({
            method: "POST",
            url: "/api/login/refreshToken",
            data: {
              u_id: tokendata.u_id
            }
          })
          await localStorage.removeItem("token");
          await localStorage.setItem("token", JSON.stringify(data.message));
          return data.message.token;
        }catch(e){
          console.log(e)
          return null;
        }
      }

      return tokendata.token;

    }
    console.log('token12')
    return token;

  }



// .then(res => {
//         const {message} = res.data;
//         User.clearLoginInfo()
//         User.setLoginInfo(message)
//         return message.token;
//       }).catch(()=>{
//         return null;
//       })
//     } else {
//       return token;
//     }

    // if (expires_in <= Math.round(new Date().getTime() /1000)) {
    //   axios({
    //     method: "POST",
    //     url: "/api/login/refreshToken",
    //     data: {
    //       u_id: User.getUserId()
    //     }
    //   }).then(res => {
    //     const {message} = res.data;
    //     User.clearLoginInfo()
    //     User.setLoginInfo(message)
    //     return message.token;
    //   }).catch(()=>{
    //     return null;
    //   })
    // } else {
    //   return token;
    // }
   
  
}

// 返回一个实例化对象
export default new AxiosRequest();




















     // 如果 token 为 true， 将token添加到headers
    // if(token){
    //   new Promise((response) => {
    //     const _token = this.getStorageToken();
    //     console.log("_token");
    //     console.log(_token);
    //     config.headers.Authorization = `Bearer ${_token}`;
    //     response(config);
    //   }).then((config)=>{
    //       axios(config)
    //       .then(res=>{
    //         typeof cb === "function" && cb(res)
    //       })
    //       .catch((error)=>{
    //         console.log(error)
    //       switch (error.response.status){
    //         case 400:
    //         alert(
    //           "温馨提示",
    //           `请先登录才能进行查看`,
    //           [
    //             {
    //               text: "取消",
    //               onPress: () => {
    //                 history.push('/');
    //               },
    //               style: { color: "red" }
    //             },
    //             {
    //               text: "前往",
    //               onPress: () => {
    //                 history.push('/login');
    //               },
    //               style: { background: "red", color: "#fff" }
    //             }
    //           ]
    //         );

    //         break;
    //         case 401:
    //         console.log("状态401")
    //         history.push("/login");
    //         break;
    //       }
    //     });
        
    //   }).catch((e)=>console.log(e))

    // }else{
    //   return null;
    // }

