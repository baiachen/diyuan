import { CHANGE_HOME_COURSE, CHANGE_HOME_INFO, CHANGE_ADDRESS } from "./actionTypes";
const defaultState = {
  // 首页类型页面之科目
  home_type_Course: {
    list: "", //抽屉内容
    cont: [] //下部主体内容
  },
  // 首页类型页面之科目
  home_type_Info: {
    title: "一年级", //一年级名字头部
    grade_id: 2, //grade_id
    subject: "all", //学科分类
    subject_index: 0, //学科分类下标
    cont: [] //内容
  },
  // 收获地址
  addressInfo:""
};

export default (state = defaultState, action) => {
  if (action.type === CHANGE_HOME_COURSE) {
    // 为了避免会改变state的值,这里要深拷贝一下
    const newState = JSON.parse(JSON.stringify(state));
    newState.home_type_Course.list = action.list;
    newState.home_type_Course.cont = action.cont;
    return newState;
  }
  if (action.type === CHANGE_HOME_INFO) {
    // 为了避免会改变state的值,这里要深拷贝一下
    const newState = JSON.parse(JSON.stringify(state));
    for (let k in newState.home_type_Info) {
      newState.home_type_Info[k] = action[k];
    }
    return newState;
  }
  if (action.type === CHANGE_ADDRESS) {
    // 为了避免会改变state的值,这里要深拷贝一下
    const newState = JSON.parse(JSON.stringify(state));
    newState.addressInfo=action.addressInfo
    return newState;
  }
  return state;
};
