import axios from "axios";
import { Toast } from "antd-mobile";
// get请求
export const getAxios = (params, callback) => {
  axios
    .get(params.api, {
      params: params.data
    })
    .then(res => {
      // console.log(res)
      if (!res.data.status) {
        Toast.fail(res.data.message, 1);
        return false;
      }
      callback(res);
    });
};
// post请求
export const PostAxios = (url, params, callback) => {
  axios.post(url, params).then(res => {
    // console.log(res)
    if (!res.data.status) {
      Toast.fail(res.data.message, 1);
      return false;
    }
    callback(res);
  });
};
//   TokenGET请求
export const getAxiosToken = (param, callback) => {
  axios
    .get(param.api, param.data, {
      headers: {
        Authorization: `Bearer ${param.token}`
      }
    })
    .then(res => {
      // console.log(res)
      if (!res.data.status) {
        Toast.fail(res.data.message, 1);
        return false;
      }
      callback(res);
    });
};
//   TokenPost请求
export const PostAxiosToken = (param, callback) => {
  axios
    .post(param.api, param.info, {
      headers: {
        Authorization: `Bearer ${param.token}`
      }
    })
    .then(res => {
      // console.log(res)
      if (!res.data.status) {
        Toast.fail(res.data.message, 1);
        return false;
      }
      callback(res);
    });
};

// 添加请求拦截器
axios.interceptors.request.use(
  function(config) {
    // 在发送请求之前做些什么
    return config;
  },
  function(error) {
    // 对请求错误做些什么
    return Promise.reject(error);
  }
);



