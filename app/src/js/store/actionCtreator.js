import { CHANGE_HOME_COURSE, CHANGE_HOME_INFO, CHANGE_ADDRESS } from "./actionTypes";
import { getAxios } from "./request";
import axios from "axios";
//改变首页分类
export const handleHomeTypeCourse = (list, cont) => ({
  type: CHANGE_HOME_COURSE,
  list,
  cont
});
//改变首页信息
export const handleHomeTypeInfo = (title,grade_id,subject,subject_index,cont) => ({
  type: CHANGE_HOME_INFO,
  title,//一年级名字头部
  grade_id, //grade_id
  subject,  //学科分类
  subject_index,  //学科分类下标
  cont    //内容
});
//改变收获地址

export const handAddress = (addressInfo) => ({
  type: CHANGE_ADDRESS,
  addressInfo
});

//Home页面请求ajax
export const Home_getList = () => {
  return dispatch => {
    const params = {
      data: {
        category_id: "all", //分类
        course_id: "all", //课程
        grade_id: "all", //年级
        page: 1
      },
      api: "api/index"
    };
    getAxios(params, res => {
      let info = res.data.message.Config;
      window.wx.config({
        debug: info.debug,
        appId: info.appId,
        timestamp: info.timestamp,
        nonceStr: info.nonceStr,
        signature: info.signature,
        jsApiList: info.jsApiList
      });
      // 创建一句话
      const action = handleHomeTypeInfo(
        "一年级",
        2,
        "all",
        0,
        res.data.message
      );
      dispatch(action);
    });
  };
};
