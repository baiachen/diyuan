import React from "react";
import {
  Icon,
  List,
  Grid,
  Button,
  Toast,
  Modal,
  ActionSheet
} from "antd-mobile";
import TabBarExample from "./TestComponent/Tabs";
import history from "../history";
import "../../css/User/user.css";
import axios from "axios";
import cookie from "react-cookies";
// import {tokenInfo} from "./token";
import user from "../Axios/user";
import AxiosRequest from "../Axios";
const dataInfo = [
  { icon: "iconfont icon-daifukuan", text: "待付款" },
  { icon: "iconfont icon-daifahuo", text: "待发货" },
  { icon: "iconfont icon-icon-test", text: "待收货" },
  { icon: "iconfont icon-yiwancheng", text: "已完成" }
];
const userList = [
  // { icon: "iconfont icon-iconfontqianbao", text: "我的钱包" },
  { icon: "iconfont icon-ziliao", text: "我的资料" },
  { icon: "iconfont icon-jiaocai", text: "我的教材" },
  { icon: "iconfont icon-pingtai", text: "平台介绍" },
  { icon: "iconfont icon-kefu", text: "客服信息" },
  { icon: "iconfont icon-icontuichudenglu", text: "退出登录" }
];
const alert = Modal.alert;
export default class User extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      has_cache: JSON.parse(localStorage.getItem("token")) || null,
      userInfo: [],
      cache_userInfo: JSON.parse(localStorage.getItem("userInfo")) || null
    };
    console.log(user.isLogin());
  }

  async componentDidMount() {
    const tokenData = (await JSON.parse(localStorage.getItem("token"))) || null;
    console.log(tokenData);
    //如果本地缓存有数据说明已经是注册用户，需要带token请求
    if (tokenData) {
      try {
        const { data } = await AxiosRequest.request("/api/uc", "GET", true, {});
        await localStorage.setItem(
          "userInfo",
          JSON.stringify(data.message.userInfo)
        );
        if (data.status) {
          this.setState({
            userInfo: data.message.userInfo,
            phone: data.message.phone
          });
        }
      } catch (e) {
        console.log(e);
      }
    }
  }
  // 去登录
  toLogin() {
    history.push(`/login`);
  }
  // 跳转全部订单
  toAllOrder() {
    console.log(this.state.has_cache);
    if (!this.state.has_cache) {
      Toast.fail("请先去登录再查看", 1);
      return false;
    }
    history.push(`/UserOrderList/0`);
  }
  // 点击跳转
  toClick(index) {
    if (!this.state.has_cache) {
      Toast.fail("请先去登录再查看", 1);
      return false;
    }
    switch (index) {
      case 0:
        history.push(`/UserOrderList/1`);
        break;
      case 1:
        history.push(`/UserOrderList/2`);
        break;
      case 2:
        history.push(`/UserOrderList/3`);
        break;
      case 3:
        history.push(`/UserOrderList/4`);
        break;
    }
  }
  // 跳转相关事件
  toEvent(index) {
    if (!this.state.has_cache && index <= 1) {
      Toast.fail("请先去登录再查看", 1);
      return false;
    }
    switch (parseInt(index)) {
      // case 0:
      //   history.push(`/wallet`);
      //   break;
      case 0:
        history.push(`/myInfo`);
        break;
      case 1:
        history.push(`/myTextbook`);
        break;
      case 2:
        history.push(`/platformIntroduction`);
        break;
      case 3:
        // 客服
        // history.push(`/customerService`);
        const BUTTONS = [this.state.phone, "呼叫", "取消"];
        ActionSheet.showActionSheetWithOptions(
          {
            options: BUTTONS,
            cancelButtonIndex: BUTTONS.length - 1,
            // title: 'title',
            maskClosable: true,
            "data-seed": "logId"
          },
          buttonIndex => {
            console.log(buttonIndex);
            if (buttonIndex === 1) {
              window.location.href = "tel:120";
            }
          }
        );
        break;
      case 4:
        alert("警告", "退出将清除全部缓存记录", [
          { text: "取消", onPress: () => console.log("cancel") },
          {
            text: "确认",
            onPress: () => {
              cookie.remove("is_login", { path: "/" });
              localStorage.clear();
              Toast.success("已成功退出", 1.5);
              history.replace("/");
            }
          }
        ]);
        break;
    }
  }
  render() {
    return (
      <div>
        <div className="user-title">
          {!this.state.has_cache ? (
            <Button
              className="user-title-btn"
              onClick={this.toLogin.bind(this)}
              size="small"
            >
              点击去登录
            </Button>
          ) : (
            <div>
              <img
                src={
                  this.state.cache_userInfo
                    ? this.state.cache_userInfo.avatarUrl
                    : this.state.userInfo.avatarUrl
                }
              />
              <p>
                {this.state.cache_userInfo
                  ? this.state.cache_userInfo.nickName
                  : this.state.userInfo.nickName}
              </p>
            </div>
          )}
        </div>
        {/* 选集 */}
        <List>
          <List.Item
            onClick={this.toAllOrder.bind(this)}
            className="user-arrow"
            arrow="horizontal"
          >
            全部订单
          </List.Item>
        </List>
        <Grid
          data={dataInfo}
          hasLine={false}
          className="userGrid-cont"
          renderItem={(dataItem, index) => (
            <div className="grid-cont" onClick={this.toClick.bind(this, index)}>
              <i className={dataItem.icon} />
              <div style={{ color: "#888" }}>{dataItem.text}</div>
            </div>
          )}
        />
        {/* 点击进入各项 */}
        <List>
          {userList.map((item, index) => {
            return (
              <List.Item
                key={index}
                onClick={this.toEvent.bind(this, index)}
                thumb={<i className={item.icon} />}
                className="user-arrow"
                arrow={index === userList.length - 1 ? "empty" : "horizontal"}
              >
                {item.text}
              </List.Item>
            );
          })}
        </List>
        {/* <a href="tel:120">呼叫</a> */}
        <TabBarExample selectedTab="2" />
      </div>
    );
  }
}
