import React from "react";
import "../../css/Login/login.css";
import {Link} from 'react-router-dom';
import { Button,Toast} from 'antd-mobile';
import history from '../history';
import store from '../store';
import axios from 'axios';
import cookie from 'react-cookies'
export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      username:"",
      password:"",
      cookies:{}
    }
    this.handleInputChange=this.handleInputChange.bind(this);
  }
  async componentDidMount() { 
    // 加载cookie
    const cookies= await cookie.loadAll();
    console.log(cookies)
    if(cookies.is_login){ 
      await localStorage.setItem("token",JSON.stringify(cookies));
      history.replace('/');
      // new Promise((resolve, reject) => {
      //   console.log("11111111")
      //   localStorage.setItem("token",JSON.stringify(cookies));
      //   resolve();
      // }).then(()=>{
      //   console.log("2222222222")
      //   history.replace('/');
      // })
    }
  }
  async submitForm(e){
    e.preventDefault();
    const param={
      mobile:this.state.username,
      password:this.state.password,
      mode:'password',
      from:history.location.pathname,
    };

    if(param.username==''){
      Toast.fail('用户名不能为空', 1);
      return false
    }
    if(param.password==''){
      Toast.fail('密码不能为空', 1);
      return false
    }
    const {data} = await axios.post('api/login', param)
    if(!data.status){
      Toast.fail(data.message, 1);
      return false;
    }
    Toast.success("登录成功", 1);
    const userInfo = data.message;

    await localStorage.setItem("token",JSON.stringify(userInfo));
    history.replace('/');

    
    // .then((res)=>{
    //   if(!res.data.status){
    //     Toast.fail(res.data.message, 1);
    //     return false;
    //   }
    //   Toast.success("登录成功", 1);
    //   const userInfo = res.data.message;
    //   new Promise((resolve, reject) => {
    //     userInfo.time=new Date().getTime();
    //     localStorage.setItem("token",JSON.stringify(userInfo));
    //     resolve();
    //   }).then(()=>{
    //     history.replace('/');
    //   })
    // });
  }
  // 跳转注册
  reset(){
    history.push('/legister');
  }
  // 改变input
  handleInputChange(e){
    console.log(e.target.name+":"+e.target.value);
    this.setState({
      [e.target.name]:e.target.value
    })
  }
  // 微信登录
  weixinLogin(){
    const  path=history.location.pathname;
    const params={
      mode:'wechat',
      from:path,
    }
    axios.post('api/login', params).then((res)=>{
      console.log(res)
      if(res.status!=200){
        Toast.fail('网络连接错误', 1);
        return false;
      }
      window.location.href=res.data.redirect_uri;
    });
  }
  render() {
    return (
      <form >
        <div className="login-cont">
          <img src={require("../../image/dyxt.png")} />
          {/* <ul>
            <li>
              <i className="iconfont icon-zhanghao"/>
              <input  value={this.state.inputValue}  type="text" placeholder="手机号" name="username"
                onChange={this.handleInputChange}/>
            </li>
            <li>
              <i className="iconfont icon-mima"/>
              <input  type="password" placeholder="密码" name="password" onChange={this.handleInputChange}/>
            </li>
            <Button  activeClassName="activeClassBtn"  onClick={this.submitForm.bind(this)}   className="login-btn">登录</Button>
          </ul> */}
          {/*  */}
          {/* <p>
            <Link to="/legister">注册</Link>
            <span onClick={this.reset}>忘记密码,立即<span>重置</span></span>
          </p> */}
          <div className="login-foot" onClick={this.weixinLogin.bind(this)}>
              <span>第三方账户登录</span>
              <img src={require("../../image/weixing.png")} />
          </div>
        </div>
      </form>
    );
  }
}
