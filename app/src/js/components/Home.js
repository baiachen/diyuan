import React from "react";
import { Drawer, Icon, Carousel, Toast } from "antd-mobile";
import TabBarExample from "./TestComponent/Tabs";
import SwiperComponent from "./TestComponent/Home-Component/swiper-component";
import GridComponent from "./TestComponent/Home-Component/Grid-component";
import TabComponent from "./TestComponent/Home-Component/Nav-component";
import "../../css/Home/Home.css";
import axios from "axios";
import store from "../store";
import { connect } from "react-redux";
import { getAxios } from "../store/request";
import {
  handleHomeTypeCourse,
  handleHomeTypeInfo,
  Home_getList
} from "../store/actionCtreator";
import AxiosRequest from '../Axios';

// stroe用setState
let that="";
class Home extends React.Component {
  constructor(props) {
    super(props);
    that=this;
    this.state = {
      open: false
    };
  }
  componentDidMount() {

    // 页面请求
    this.props.getUserInfo();
  }
  // 切换开关
  onOpenChange() {
    this.setState({ open: !this.state.open });
  }
  render() {
    const {
      defaltGrade, // 默认年级
      grade_id, //grade_id
      subject, // 学科分类
      subject_index, // 学科分类
      userInfo//主体内容
    } = this.props;
    if (userInfo.length<=0) {
      return false;
    }
    const sidebar = (
      <div className="grade-container">
        {userInfo.Grade.map((items, i) => {
          return (
            <div key={i} className="grade-item">
              <div>{items.title}</div>
              <ul>
                {items.children.map((item, index) => {
                  return (
                    <li
                      key={item.id}
                      onClick={() => this.props.geadeClick(item,subject,subject_index)}
                      className={
                        this.props.defaltGrade == item.title
                          ? "gradeChird active"
                          : "gradeChird"
                      }
                    >
                      {item.title}
                    </li>
                  );
                })}
              </ul>
            </div>
          );
        })}
      </div>
    );
    return (
      <div>
        {/* 抽屉 */}
        <Drawer
          className="my-drawer"
          style={{ minHeight: document.documentElement.clientHeight }}
          enableDragHandle
          sidebar={sidebar}
          open={this.state.open}
          onOpenChange={this.onOpenChange.bind(this)}
          position="top"
        >
          {/* 箭头 */}
          <div className="Drawer-arrow" onClick={this.onOpenChange.bind(this)}>
            {this.props.defaltGrade}
            <Icon type="down" size="sm" />
          </div>
          {/* 轮播图 */}
          <SwiperComponent swiperData={userInfo.Banner} />
          {/* 分类 */}
          <GridComponent gridData={userInfo.Category} />
          {/* 下半主体内容 */}
          <TabComponent
            tabBarData={userInfo}
            grade_id={grade_id}
            handleChild={(tab,index) => this.props.handleChild(tab,index,grade_id,defaltGrade)}
          />
          <TabBarExample selectedTab="0" />
        </Drawer>
      </div>
    );
  }
}

// 映射规则
const mapStateToProps = state => {
  return {
    defaltGrade: state.home_type_Info.title, // 默认年级
    grade_id: state.home_type_Info.grade_id, //grade_id
    subject: state.home_type_Info.subject, // 学科分类
    subject_index: state.home_type_Info.subject_index, // 学科分类
    userInfo: state.home_type_Info.cont // 主体内容
  };
};
// 提交dispatch
const mapDispatchToProps = dispatch => {
  return {
    // 页面请求
    getUserInfo() {
      //清空分类redux
      dispatch(handleHomeTypeCourse("", ""));
      //页面请求
      if(that.props.userInfo.length<=0){
        dispatch(Home_getList());
      }
    },
    // 点击年级
    geadeClick(item,subject,subject_index) {
      const params = {
        data: {
          category_id: "all", //分类
          course_id: "all", //课程
          grade_id: item.id, //年级
          page: 1
        },
        api: "api/index"
      };
      getAxios(params, res => {
        that.setState({
          open: false
        });
        // 创建一句话
        const action = handleHomeTypeInfo(
          item.title,
          item.id,
          subject,
          subject_index,
          res.data.message
        );
        dispatch(action);
      });
    },
    // 子传父通信
    handleChild(tab, index,grade_id,defaltGrade) {
      const params = {
        data: {
          category_id: "all", //分类
          course_id: tab.id, //课程
          grade_id: grade_id, //年级
          page: 1
        },
        api: "api/index"
      };
      getAxios(params, res => {
        // 创建一句话
        const action = handleHomeTypeInfo(
          defaltGrade,
          grade_id,
          tab.id,
          index,
          res.data.message
        );
        
        dispatch(action);
      });
    }
  };
};
// 连接
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
