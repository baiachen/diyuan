import React from 'react';
import { Icon,List,SearchBar,Tabs} from 'antd-mobile';
import history from '../history';
import '../../css/Mall/mall.css';
import SwiperComponent from './TestComponent/Home-Component/swiper-component'
import MallTabItemComponent from './TestComponent/Mall-Component/tabItem';
import  TabBarExample from './TestComponent/Tabs';
const tabs = [
  { title: "全部" },
  { title: "销量" },
  { title: "价格" },
];


export default class Mall extends React.Component {
  constructor(props) {
    super(props);
  }
  toTitle(){
    // window.location.href="http://www.w3school.com.cn";
    // console.log(window.screen);
    // console.log(window.history);
  }
  // 点击跳转
  toClick(index){
    history.push(`/mallTabItemDetail/${index}`);
  }
  render() {
    return (
      <div className="mall-tab">
        <SearchBar  placeholder="Search" maxLength={8} />
        <SwiperComponent/>
        <div className="mall-btm-btn">
          <div className="Mall-price">
            <i className="iconfont icon-arrow-up"></i>
            <i className="iconfont icon-arrow-down-copy"></i>
          </div>
          {/* 选项卡 */}
          <Tabs tabs={tabs}
            className="Tab-cont"
            tabBarActiveTextColor="#f7463e"
            initialPage={0}
            swipeable={false}
            onChange={(tab, index) => { console.log('onChange', index, tab); }}
            onTabClick={(tab, index) => { console.log('onTabClick', index, tab); }}
          > 
            {/* 全部 */}
            <div className="mallTab-cont">
                {tabs.map((items,i)=>{
                  return ( 
                    <div  className="malltabIndex" key={i} onClick={this.toClick.bind(this,i)}>
                        <MallTabItemComponent  itemData={items}></MallTabItemComponent>
                    </div>
                  )
                })}
            </div>
            {/* 数学 */}
            <div className="TabItem">
                {tabs.map((items,i)=>{
                  return ( 
                    <div key={i} onClick={this.toClick.bind(this,i)}>
                        222
                    </div>
                  )
                })}
            </div>
            {/* 语文 */}
            <div className="TabItem">
              {tabs.map((items,i)=>{
                  return ( 

                    <div key={i} onClick={this.toClick.bind(this,i)}>
                        333
                    </div>
                  )
                })}
            </div>
          </Tabs>
        </div>
        {/* 底部选项卡 */}
        <TabBarExample selectedTab="1"/>
      </div>
    );
  }
}