import React from "react";
import { Button, Toast } from "antd-mobile";
import history from "../history";
import axios from "axios";
import AxiosRequest from "../Axios";
var interval = null; //倒计时函数
export default class Legister extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mobile: "",
      code: "",
      password: "",
      sendCode_btn: false, //发送按钮
      time: "获取验证码", //倒计时
      currentTime: 61
    };
    this.handleInputChange = this.handleInputChange.bind(this);
  }
  // 跳转登录
  onLogin() {
    history.goBack();
  }
  // 发送验证码
  send_code() {
    let param = { mobile: this.state.mobile },
      myreg = /^[1][3,4,5,7,8,9][0-9]{9}$/;
    if (param.mobile == "") {
      Toast.fail("手机号不能为空", 1);
      return false;
    }
    if (!myreg.test(param.mobile)) {
      Toast.fail("手机号码格式错误", 1);
      return false;
    }
    Toast.loading("发送中...", 10);
    this.getCode();
    axios.post("api/login/send_msg", param).then(res => {
      console.log(res);
      Toast.hide();
      if (!res.data.status) {
        Toast.fail(res.data.message, 1);
        return false;
      }
      Toast.info("验证码为" + res.data.message.code, 5);
      // Toast.success('发送验证码成功', 1);
      this.setState({
        sendCode_btn: true
      });
    });
  }
  // 倒计时
  getCode() {
    let currentTime = this.state.currentTime;
    interval = setInterval(() => {
      currentTime--;
      this.setState({
        time: "再次获取" + "(" + currentTime + ")"
      });
      if (currentTime <= 0) {
        clearInterval(interval);
        this.setState({
          time: "重新发送",
          currentTime: 61,
          sendCode_btn: false
        });
      }
    }, 1000);
  }
  // 改变input
  handleInputChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }
  // 提交注册
  async submitForm(e) {
    e.preventDefault();
    let param = { mobile: this.state.mobile },
      myreg = /^[1][3,4,5,7,8,9][0-9]{9}$/;
    if (param.mobile == "") {
      Toast.fail("手机号不能为空", 1);
      return false;
    }
    if (!myreg.test(param.mobile)) {
      Toast.fail("手机号码格式错误", 1);
      return false;
    }
    // if (param.code == "") {
    //   Toast.fail("验证码不能为空", 1);
    //   return false;
    // }
    // if(param.password==''){
    //   Toast.fail('密码不能为空', 1);
    //   return false
    // }
    const { data } = await AxiosRequest.request(
      "api/auth/register",
      "POST",
      true,
      param
    );
    if (!data.status) {
      return false;
    }
    // this.setState({
    //   userInfo: data.message
    // });
    history.goBack();
    // axios.post('api/auth/register', param).then((res)=>{
    //   console.log(res)
    //   Toast.hide();
    //   if(!res.data.status){
    //     Toast.fail(res.data.message, 1);
    //     return false;
    //   }

    //   // Toast.success("注册成功", 1);
    //   // const userInfo = res.data.message;
    //   // new Promise((resolve, reject) => {
    //   //   userInfo.time=new Date().getTime();
    //   //   localStorage.setItem("token",JSON.stringify(userInfo));
    //   //   resolve();
    //   // }).then(()=>{

    //   // })
    // });
  }
  render() {
    return (
      <div>
        <form>
          {/* onSubmit={this.submitForm.bind(this)} */}
          <div className="login-cont">
              <img src={require("../../image/dyxt.png")} />
            <ul>
              <li>
                <i className="iconfont icon-zhanghao" />
                <input
                  type="text"
                  placeholder="手机号"
                  name="mobile"
                  onChange={this.handleInputChange}
                />
              </li>
              {/* <li>
                <i className="iconfont icon-yanzhengma" />
                <input
                  type="text"
                  placeholder="验证码"
                  name="code"
                  onChange={this.handleInputChange}
                />
                <Button
                  disabled={this.state.sendCode_btn}
                  activeClassName="activeClassBtn"
                  className="obtainCode"
                  onClick={this.send_code.bind(this)}
                >
                  {this.state.time}
                </Button>
              </li> */}
              {/* <li>
                <i className="iconfont icon-mima" />
                <input
                  type="password"
                  placeholder="登录密码"
                  name="password"
                  onChange={this.handleInputChange}
                />
              </li> */}
              <Button
                activeClassName="activeClassBtn"
                onClick={this.submitForm.bind(this)}
                type="sumbit"
                className="login-btn"
              >
                修改
              </Button>
            </ul>
            {/*  */}
            {/* <p>
              <span />
              <span onClick={this.onLogin}>
                已有账号,立即<span>登录</span>
              </span>
            </p> */}
          </div>
        </form>
      </div>
    );
  }
}
