import React  from 'react';
import history from '../../../history';

export default class MallTabItemComponent extends React.Component {
  constructor(props) {
    super(props);
    
  }
  componentDidMount() {
    // console.log(this.props.itemData.type)
  }
  render() {
    return (
      <ul className="mallTabItem">
        <li>
            <img src={require('../../../../image/book.png')}/>
        </li>
        <li>书本名称</li>
        <li>描述文字</li>
        <li>
            <span>￥169</span>
            <span>已销售123件</span>
        </li>
        <li>
          <div>语文</div>
          <div>一年级</div>
          <div>97.8%好评</div>
        </li>
      </ul>
    );
  }
}