import React from "react";
import { Icon, Button, List, Grid } from "antd-mobile";
import SwiperComponent from "../Home-Component/swiper-component";
import PublicBtn from "../publicBtn";
import history from "../../../history";
// import '../../../../css/Mall/mall.css'
const data = Array.from(new Array(8)).map((_val, i) => ({
  icon: "https://gw.alipayobjects.com/zos/rmsportal/nywPmnTAvTmLusPxHPSu.png",
  text: `name${i}`
}));

export default class MallTabItemDetailComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal1: false
    };
  }
  componentDidMount() {
    // console.log(this.props.match.params.id)
  }
  //   跳转全部章节
  toListInfo() {
    history.push("/homeAllUserList/0");
  }
  render() {
    return (
      <div>
        <SwiperComponent />
        {/* 金额&观看人数 */}
        <ul className="HTD-title">
          <li>
            <span>
              ￥<span>30</span>
            </span>
          </li>
          <li>
            <span style={{ color: "#ababab" }}>已销售123件</span>
          </li>
        </ul>
        <ul className="HomeItemCont HTD-info ttd-cont">
          <li>
            <div>数</div>
            <div>一年级</div>
            <div>标题文字</div>
          </li>
          <li />
          <li>
            <span>标题介绍</span>
            <i className="iconfont icon-fenxiang" />
          </li>
          <li>
            <span>快递：0.00</span>
          </li>
        </ul>
        {/* 选集
        <List>
          <List.Item
            className="book-userLIst TTD-xuanji"
            onClick={this.toListInfo.bind(this)}
            arrow="horizontal"
          >
            <span>参数</span>
          </List.Item>
        </List> */}
        {/* 更多推荐 */}
        <div className="HTD-userLIst">
          <div className="HTD-Recommend">
            <img src={require("../../../../image/tuijian.png")} />
            <span>更多推荐</span>
            <Icon type="right" size="sm" />
          </div>
          <Grid
            data={data}
            carouselMaxRow={1}
            isCarousel
            dots={false}
            itemStyle={{"height":"35vw"}}
            renderItem={(item, index) => (
              <div className="TTD-moreRecommend">
                <img src={require('../../../../image/book.png')}/>
                <span>书籍名称</span>
                <span>￥310</span>
              </div>
            )}
          />
        </div>
        {/* 底部按钮 */}
        <PublicBtn />
      </div>
    );
  }
}
