import React from "react";
import { Icon, List, Modal, Toast } from "antd-mobile";
import HomeTypeDetail from "./HomeTypeDetail";
import history from "../../../../history";
import axios from "axios";
import { getAxios } from "../../../../store/request";
import AxiosRequest from "../../../../Axios";
import { handleLogin, handleDetail } from "./public";
const alert = Modal.alert;
export default class AudioDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userInfo: [],
      // 当前路径
      currentSrc: "",
      // 是否免费
      is_free: ""
    };
  }
  async componentDidMount() {
    document.title = "音频";
    const status = (await JSON.parse(localStorage.getItem("token")))
      ? true
      : false;
    const { data } = await AxiosRequest.request(
      "/api/topics/detail",
      "GET",
      status,
      { id: this.props.match.params.id }
    );
    if (!data.status) {
      return false;
    }
    let userInfo = data.message;
    let more = {
      title: "更多"
    };
    userInfo.resources.push(more);
    this.setState({
      currentSrc: userInfo.resources[0].resource_path,
      is_free: userInfo.resources[0].is_free,
      userInfo: data.message
    });
  }
  // 点击相关章节
  changeItem(item, index) {
    if (index == this.state.userInfo.resources.length - 1) {
      history.push(
        `/homeAllUserList/${this.state.userInfo.resource_type}/${
          this.state.userInfo.id
        }/${this.state.userInfo.price}`
      );
    } else {
      let userInfo = this.state.userInfo;
      let id = this.props.match.params.id;
      // 处理购买
      handleDetail(userInfo, id, item.is_free, () => {
        // userInfo.read_counts = item.read_counts;
        Toast.success("切换成功", 1.5);
        this.setState({
          currentSrc: item.resource_path,
          is_free: item.is_free,
          userInfo: userInfo
        });
      },()=>{
        // 已经购买过
        // userInfo.read_counts = item.read_counts;
        Toast.success("切换成功", 1.5);
        this.setState({
          currentSrc: item.resource_path,
          is_free: item.is_free,
          userInfo: userInfo
        });
      });
    }
  }
  //   跳转全部章节
  toListInfo() {
    history.push(
      `/homeAllUserList/${this.state.userInfo.resource_type}/${
        this.state.userInfo.id
      }/${this.state.userInfo.price}`
    );
  }
  // 更多推荐列表点击
  moreRecommend(item) {
    new Promise((resolve, reject) => {
      history.push(`/videoDetail/${item.id}`);
      resolve();
    }).then(() => {
      history.go(0);
    });
  }
  render() {
    if (this.state.userInfo.length <= 0) {
      return false;
    }
    return (
      <div>
        {/* 公共组件 */}
        <HomeTypeDetail data={this.state.userInfo} free={this.state.is_free}>
          <div>
            <img className="book-img" src={this.state.userInfo.thumbnail} />
            {/* 音频 */}
            <audio
              className="audio-class"
              controls
              controlsList="nodownload"
              src={this.state.currentSrc}
            >
              音频
            </audio>
          </div>

          <div>
            {/* 选集 */}
            <List>
              <List.Item
                className="book-userLIst"
                onClick={this.toListInfo.bind(this)}
                arrow="horizontal"
              >
                选集
              </List.Item>
            </List>
            <ul className="video-list">
              {this.state.userInfo.resources.map((item, index) => {
                return (
                  <li
                    key={index}
                    onClick={this.changeItem.bind(this, item, index)}
                  >
                    {item.title}
                    {/* 0是免费1是收费 */}
                    {item.is_free == 0 && item.title !== "更多" ? (
                      <span className="freeItem">免费</span>
                    ) : (
                      ""
                    )}
                  </li>
                );
              })}
            </ul>
            {/* 更多推荐 */}
            <div className="HTD-userLIst">
              <div className="HTD-Recommend">
                <img src={require("../../../../../image/tuijian.png")} />
                <span>更多推荐</span>
                <Icon type="right" size="sm" />
              </div>
              <div>
                {this.state.userInfo.recommend.map((item, index) => {
                  return (
                    <div
                      key={index}
                      onClick={this.moreRecommend.bind(this, item)}
                      className="HTD-list"
                    >
                      <span>{item.title}</span>
                      <span>
                        ￥<span>{item.price}</span>
                      </span>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </HomeTypeDetail>
      </div>
    );
  }
}
