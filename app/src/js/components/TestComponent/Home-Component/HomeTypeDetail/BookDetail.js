import React, { Fragment } from "react";
import { Icon, List, Modal, Toast, Button } from "antd-mobile";
import HomeTypeDetail from "./HomeTypeDetail";
import history from "../../../../history";
import AxiosRequest from "../../../../Axios";
import { handleLogin, handleDetail } from "./public";
const alert = Modal.alert;
export default class BookDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userInfo: [],
      // 当前路径
      currentSrc: "",
      // 是否免费
      is_free: ""
    };
  }
  async componentDidMount() {
    // console.log(this.props.match.params.id)
    const status = (await JSON.parse(localStorage.getItem("token")))
      ? true
      : false;
    document.title = "电子书";
    const { data } = await AxiosRequest.request(
      "/api/topics/detail",
      "GET",
      status,
      { id: this.props.match.params.id }
    );
    if (!data.status) {
      return false;
    }
    // let userInfo = data.message;
    // let more = {
    //   title: "更多"
    // };
    // userInfo.resources.push(more);
    this.setState({
      currentSrc: data.message.resources[0].resource_path,
      is_free: data.message.resources[0].is_free,
      userInfo: data.message
    });
  }
  // 点击相关章节
  changeItem(item, index) {
    // if (index == this.state.userInfo.resources.length - 1) {
    //   history.push(
    //     `/homeAllUserList/${this.state.userInfo.resource_type}/${
    //       this.state.userInfo.id
    //     }`
    //   );
    // } else {
    // }
    // 是免费的话
    let userInfo = this.state.userInfo;
    let id = this.props.match.params.id;
    // 处理购买
    handleDetail(
      userInfo,
      id,
      item.is_free,
      () => {
        // userInfo.read_counts = item.read_counts;
        Toast.success("切换成功", 1.5);
        this.setState({
          currentSrc: item.resource_path,
          is_free: item.is_free,
          userInfo: userInfo
        });
      },
      () => {
        // 已经购买过
        // userInfo.read_counts = item.read_counts;
        Toast.success("切换成功", 1.5);
        this.setState({
          currentSrc: item.resource_path,
          is_free: item.is_free,
          userInfo: userInfo
        });
      }
    );
  }
  //   跳转全部章节
  toListInfo() {
    history.push(
      `/homeAllUserList/${this.state.userInfo.resource_type}/${
        this.state.userInfo.id
      }/${this.state.userInfo.price}`
    );
  }
  // 点击下载
  download() {
    // this.iframe.href="https://codeload.github.com/douban/douban-client/legacy.zip/master"
    // window.open(
    //   "https://codeload.github.com/douban/douban-client/legacy.zip/master"
    // );
    // handleLogin(() => {
    //   this.iframe.href = this.state.currentSrc;
    // });
    let userInfo = this.state.userInfo;
    let id = this.props.match.params.id;
    console.log(this.iframe.download);
    // 处理购买
     handleDetail(
      userInfo,
      id,
      this.state.is_free,
      () => {
        this.iframe.href = this.state.currentSrc;
        // window.open(this.state.currentSrc);
        // this.iframe.download = "w3logo.jpg";
        // console.log(this.iframe.href);
        // console.log(this.iframe.download);
        //  this.iframe.download="文件名.jpg";
        //      window.open(
        //       this.state.currentSrc
        // );
      },
      () => {
        this.iframe.href = this.state.currentSrc;
        // window.open(this.state.currentSrc);
        // this.iframe.download="文件名.txt";
      }
    );
  }
  render() {
    if (this.state.userInfo.length <= 0) {
      return false;
    }
    return (
      <div>
        {/* 公共组件 */}
        <HomeTypeDetail data={this.state.userInfo} free={this.state.is_free}>
          {/* 电子书 */}
          <img className="book-img" src={this.state.userInfo.thumbnail} />
          <div>
            {/* 更多推荐 */}
            {/* <div className="">
            <Button type="primary" size="small">点击下载</Button>
            </div> */}
            <List>
              <List.Item
                className="book-userLIst"
                extra={
                  <Fragment>
                    {/* <Button type="ghost"   onClick={this.download.bind(this)}   size="small" inline>
                </Button> */}
                    <a
                      ref={iframe => {
                        this.iframe = iframe;
                      }}
                      className="btn-blank"
                      href="#"
                      download=""
                      onClick={this.download.bind(this)}
                    >
                      点击下载
                    </a>
                  </Fragment>
                }
              >
                下载
              </List.Item>
              <List.Item
                className="book-userLIst"
                arrow="horizontal"
                onClick={this.toListInfo.bind(this)}
              >
                章节列表
              </List.Item>
            </List>
            <ul className="bookList">
              {/* {this.state.userInfo.recommend.map((item, index) => {
                return (
                  <li key={index} onClick={this.changeItem.bind(this, index)}>
                    {item}
                  </li>
                );
              })} */}
              {this.state.userInfo.resources.map((item, index) => {
                return (
                  <li
                    key={index}
                    onClick={this.changeItem.bind(this, item, index)}
                  >
                    {item.title}
                    {/* 0是免费1是收费 */}
                    {item.is_free == 0 && item.title !== "更多" ? (
                      <span className="freeItem">免费</span>
                    ) : (
                      ""
                    )}
                  </li>
                );
              })}
            </ul>
          </div>
        </HomeTypeDetail>
      </div>
    );
  }
}
