import React from "react";
import { Icon, Button, Modal } from "antd-mobile";
import "../../../../../css/Home/HomeTypeDetail/HomeTypeDetail.css";
import PublicBtn from "../../publicBtn";
import history from "../../../../history";
import axios from "axios";
import { getAxios } from "../../../../store/request";
export default class HomeTypeDetail extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      // course:this.props.data.course.slice(0,1)
    };
  }
  componentDidMount() {
    console.log(this.props)
    console.log(this.props.data.course.slice(0,1));
  }
  render() {
    return (
      <div>
        {/* 区分 */}
        {this.props.children[0]}
        {/* 金额&观看人数 */}
        <ul className="HTD-title">
          <li>
            <span>
              ￥<span>{this.props.data.price}</span>
            </span>
          </li>
          <li>
            <i className="iconfont icon-guankan" />
            <span>{this.props.data.read_counts}人</span>
          </li>
        </ul>
        <ul className="HTD-info">
          <li>
            <div>{this.props.data.course.slice(0,1)}</div>
            <div>{this.props.data.grade}</div>
            <div>{this.props.data.title}</div>
          </li>
          <li>{this.props.data.description}</li>
          <li>
            <span>{this.props.data.teacher_name}</span>
            {/* <i className="iconfont icon-fenxiang" /> */}
          </li>
          <li>
            <span>更新时间：{this.props.data.updated_at}</span>
          </li>
        </ul>
        <span className="HTD-icont">
          {this.props.data.tags.map((item, i) => {
            return <span key={i}>{item.name}</span>;
          })}
        </span>
        {/* 区分 */}
        {this.props.children[1]}
        {/* 底部按钮 */}
        <PublicBtn data={this.props.data} free={this.props.free}/>
      </div>
    );
  }
}
