import React from "react";
import { Icon, List } from "antd-mobile";
import history from "../../../../history";
import { getAxios } from "../../../../store/request";
import AxiosRequest from "../../../../Axios";
import { handleLogin, handleDetail } from "./public";
export default class AllUserList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userInfo: []
    };
  }
  async componentDidMount() {
    console.log("1111111");
    console.log(this.props.match.params);
    const { data } = await AxiosRequest.request(
      "/api/topics/resources",
      "GET",
      true,
      {
        id: this.props.match.params.id
      }
    );
    if (!data.status) {
      return false;
    }
    this.setState({
      userInfo: data.message
    });
  }
  // componentDidMount() {

  //   const params = {
  //     data: {
  //       id: this.props.match.params.id
  //     },
  //     api: "/api/topics/resources"
  //   };
  //   getAxios(params, res => {
  //     this.setState({
  //       userInfo: res.data.message
  //     });
  //   });
  // }
  //   跳转相关章节
  toListInfo(item, index) {
    console.log(item.is_free);
    let userInfo = {
      is_buy: item.is_buy,
      price: this.props.match.params.price
    };
    let id = this.props.match.params.id;
    // 处理购买
    handleDetail(userInfo, id, item.is_free, () => {
      this.to_index(item)
    },()=>{
      this.to_index(item)
    });
  }
  to_index(item){
    switch (parseInt(this.props.match.params.type)) {
      case 1:
        history.push(`/videoDetail/${item.topic_id}`);
        break;
      case 2:
        history.push(`/audioDetail/${item.topic_id}`);
        break;
      case 3:
        history.push(`/bookDatil/${item.topic_id}`);
        break;
    }
  }
  render() {
    if (this.state.userInfo.length <= 0) {
      return false;
    }
    return (
      <div>
        <List >
          {this.state.userInfo.map((item, index) => {
            return (
              <List.Item
                key={index}
                className="book-userLIst"
                arrow="horizontal"
                onClick={this.toListInfo.bind(this, item, index)}
              >
                {item.title}
              </List.Item>
            );
          })}
        </List>
      </div>
    );
  }
}
