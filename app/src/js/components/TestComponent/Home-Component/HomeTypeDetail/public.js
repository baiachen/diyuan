import history from "../../../../history";
import { Modal, Toast } from "antd-mobile";
const alert = Modal.alert;
export const handleLogin = cb => {
  if (
    JSON.parse(localStorage.getItem("token")) == null ||
    !JSON.parse(localStorage.getItem("token"))
  ) {
    alert("温馨提醒", "请先去登录才能进行查看或购买", [
      {
        text: "取消",
        style: { color: "red" }
      },
      {
        text: "前往",
        onPress: () => {
          history.push(`/login`);
        },
        style: { background: "red", color: "#fff" }
      }
    ]);
  } else {
    // console.log(JSON.parse(localStorage.getItem("token")))
    cb();
  }
};

export const handleDetail = (userInfo, id, is_free, cb,alreadyBuy) => {
    console.log(is_free)
  // 如果是收费视频的话,才弹出购买窗
  if (is_free == 1) {
    console.log("收费视频");
    // 必须先登录
    handleLogin(() => {
      //   如果没有购买过的话
      console.log(userInfo.isBuy)
      if (!userInfo.isBuy) {
        console.log("未购买");
        alert("支付提醒", `本次需要支付费用￥${userInfo.price}元,才可收看收费内容`, [
          {
            text: "取消",
            style: { color: "red" }
          },
          {
            text: "确认支付",
            onPress: () => {
              history.push(`/settlement/${id}/0`);
            },
            style: { background: "red", color: "#fff" }
          }
        ]);
      } else {
        // 如果购买过了
        console.log("已购买")
        if (typeof alreadyBuy == 'function') {
          alreadyBuy()
        }
      }
    });
    // 是免费的话
  } else if (is_free == 0) {
    console.log("免费视频");
    if (typeof cb == 'function') {
      cb()
    }
  }
};
