import { Tabs } from "antd-mobile";
import React from "react";
import history from "../../../history";
import NavItemComponent from "../Home-Component/Nav-Item-component";
import PropTypes from "prop-types";
import { connect } from "react-redux";
const tabs = [
  { title: "全部", id: "all" },
  { title: "数学", id: 1 },
  { title: "语文", id: 2 },
  { title: "英语", id: 3 }
];

class TabExample extends React.Component {
  constructor(props) {
    super(props);
  }
  // 点击跳转
  toClick(item) {
    switch (parseInt(item.category.id)) {
      case 1:
        history.push(`/videoDetail/${item.id}`);
        break;
      case 2:
        history.push(`/audioDetail/${item.id}`);
        break;
      case 3:
        history.push(`/bookDatil/${item.id}`);
        break;
    }
  }
  render() {
    const {
      defaltGrade, // 默认年级
      grade_id, //grade_id
      subject, // 学科分类
      subject_index, // 学科分类下标
      userInfo//主体内容
    } = this.props;
    return (
      <div className="TabCont">
        <Tabs
          tabs={tabs}
          className="Tab-cont"
          tabBarActiveTextColor="#f7463e"
          initialPage={subject_index}
          swipeable={false}
          onTabClick={(tab,index)=>this.props.handleChild(tab,index)}
        >
          {tabs.map((item, index) => {
            return (
              <div key={item}>
                {/* 全部 */}
                <div className="TabItem">
                  {userInfo.Topic.data.length > 0 ? (
                    userInfo.Topic.data.map((items, i) => {
                      return (
                        <div key={items.id} onClick={this.toClick.bind(this, items)}>
                          <NavItemComponent {...items} />
                        </div>
                      );
                    })
                  ) : (
                    <div className="no-data">暂无数据</div>
                  )}
                </div>
              </div>
            );
          })}
        </Tabs>
      </div>
    );
  }
}

// TabExample.propTypes = {
//   // tabBarData: PropTypes.number.isRequired
// }
// TabExample.defaultProps = {
//   name: [1,2,3]
// };
// 映射规则
const mapStateToProps = state => {
  return {
    defaltGrade: state.home_type_Info.title, // 默认年级
    grade_id: state.home_type_Info.grade_id, //grade_id
    subject: state.home_type_Info.subject, // 学科分类
    subject_index: state.home_type_Info.subject_index, // 学科分类
    userInfo: state.home_type_Info.cont // 主体内容
  };
};
// 提交dispatch
const mapDispatchToProps = dispatch => {
  return {

  };
};
// 连接
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TabExample);