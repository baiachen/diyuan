import { Grid} from 'antd-mobile';
import React  from 'react';
import history from '../../../history';
import '../../../../css/Home/Grid.css';
import img1 from "../../../../image/2.png";
import img2 from '../../../../image/4.png';
import img3 from '../../../../image/23.png';
import img4 from '../../../../image/3.png';
const data = [
    {icon:img1,text:"视频"},
    {icon:img2,text:"音频"},
    {icon:img3,text:"电子书"},
    {icon:img4,text:"全部"},
];
// 自定义样式

export default class GridComponent extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    // console.log(this.props.itemData.type)
  }
  // 点击跳转
  toClick(index){
    history.push(`/home/${index}`);
  }
  render() {        
    return (
    <div className="user-conts">
        <Grid data={this.props.gridData} hasLine={false}  renderItem={(dataItem,index) => (
        <div className="grid-cont" onClick={this.toClick.bind(this,dataItem.id)}>
          <img src={dataItem.icon} className="grid-icon" alt={dataItem.name} />
          <div style={{ color: '#888'}}>{dataItem.name}</div>
        </div>
        )}/>
    </div>);
  }
}