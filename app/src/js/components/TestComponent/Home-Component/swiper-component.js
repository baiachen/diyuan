import { Carousel} from 'antd-mobile';
import React from 'react';
import '../../../../css/Home/swiper.css';


export default class SwiperComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      
    }
  }
  componentDidMount() {
    
  }
 
  render() {        
    return (<div>
      <Carousel
          autoplay={true}
          infinite
          className="swiper-class">
          {this.props.swiperData.map((item,index) => (
            <a
              key={index}
              href="#"
            >
              <img
                src={item}
                alt=""
                style={{ width: '100%', verticalAlign: 'top',}}
                onLoad={() => {
                  // fire window resize event to change height
                  // window.dispatchEvent(new Event('resize'));
                  // this.setState({ imgHeight: 'auto' });
                }}
              />
            </a>
          ))}
        </Carousel>
    </div>);
  }
}

