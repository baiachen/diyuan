import React from "react";
import "../../../../css/Home/navItem.css";
import history from "../../../history";
export default class NavItemComponent extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {}
  render() {
    return (
      <ul className="HomeItemCont">
        <li>
          <div>{this.props.course.name.slice(0,1)}</div>
          <div>{this.props.category.name.slice(0,1)}</div>
          <div>{this.props.grade.title}</div>
          <div>{this.props.title}</div>
        </li>
        <li>{this.props.description}</li>
        <li>
          <span>{this.props.teacher_name}</span>
          <span>
            ￥<span>{this.props.price}</span>
          </span>
        </li>
        <li>
          <span>
            {this.props.tags.map((item, i) => {
              return (
                <span key={i}>{item.name}</span>
              );
            })}
          </span>
          <p>
            <i className="iconfont icon-guankan" />
            <span>{this.props.read_counts}人</span>
          </p>
        </li>
      </ul>
    );
  }
}
