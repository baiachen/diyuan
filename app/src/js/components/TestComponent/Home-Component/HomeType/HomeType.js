import React from "react";
import { Icon, Drawer } from "antd-mobile";
import NavItemComponent from "../Nav-Item-component";
import history from "../../../../history";
import "../../../../../css/Home/HomeType/HomeType.css";
import { getAxios } from "../../../../store/request";
import { handleHomeTypeCourse } from "../../../../store/actionCtreator";
import { connect } from "react-redux";

// stroe用setState
let that=""
class HomeType extends React.Component {
  constructor(props) {
    super(props);
    that=this;
    this.state = {
      // 开关
      open: false,
      // 抽屉内容
      selectType: [],
      // 区别学科和年级
      selectId: 0
    };
  }
  componentDidMount() {
    // 请求
    this.props.HomeType_getUserInfo();
  }
  // 点击显示
  changClick(item, index) {
    this.setState({
      open: !this.state.open,
      selectId: index, //菜单默认下标
      selectType: item.cont //默认抽屉内容
    });
  }
  // 切换开关
  onOpenChange() {
    this.setState({ open: !this.state.open });
  }

  // 点击跳转详情
  to_HomeTypeDetail(item) {
    switch (parseInt(item.category.id)) {
      case 1:
        history.push(`/videoDetail/${item.id}`);
        break;
      case 2:
        history.push(`/audioDetail/${item.id}`);
        break;
      case 3:
        history.push(`/bookDatil/${item.id}`);
        break;
    }
  }
  render() {
    const { List, userInfo,match} = this.props;
    // console.log(this.props);
    // return false;
    if (userInfo.length <= 0) {
      return false;
    }
    // 抽屉内容
    const sidebarS = (
      <div className="grade-container">
        <div className="grade-item">
          <ul>
            {this.state.selectType.map((item, index) => {
              return (
                <li key={index}>
                  <p
                    onClick={()=>this.props.HomeType_geadeClick(item,this.state.selectId)}
                    className={
                      List[this.state.selectId].title === item.name
                        ? "gradeChird HTH-graderChird active "
                        : "gradeChird HTH-graderChird"
                    }
                  >
                    {item.name}
                  </p>
                </li>
              );
            })}
          </ul>
        </div>
      </div>
    );
    return (
      <div>
        {/* 头部 */}
        <ul className="HT-header">
          {List.map((item, index) => {
            return (
              <li key={index} onClick={this.changClick.bind(this, item, index)}>
                <span>{item.title}</span>
                <Icon
                  type="down"
                  size="xxs"
                  className={
                    this.state.open && index == this.state.selectId
                      ? "upDown"
                      : "topDown"
                  }
                />
              </li>
            );
          })}
        </ul>
        {/* 抽屉 */}
        <Drawer
          className="my-drawer HTH-title"
          style={{ minHeight: document.documentElement.clientHeight }}
          enableDragHandle
          sidebar={sidebarS}
          open={this.state.open}
          onOpenChange={this.onOpenChange.bind(this)}
          position="top"
        >
          {userInfo.Topic.data.length > 0 ? (
            userInfo.Topic.data.map((items, i) => {
              return (
                <div key={i} onClick={this.to_HomeTypeDetail.bind(this, items)}>
                  <NavItemComponent {...items} />
                </div>
              );
            })
          ) : (
            <div className="no-data">暂无数据</div>
          )}
        </Drawer>
      </div>
    );
  }
}

// 映射规则
const mapStateToProps = state => {
  return {
    List: state.home_type_Course.list,
    userInfo: state.home_type_Course.cont
  };
};
// 提交dispatch
const mapDispatchToProps = dispatch => {
  return {
    // 页面请求
    HomeType_getUserInfo() {
      let List = [
        { title: "语文", cont: "", id: 1 },
        { title: "一年级", cont: "", id: 2 }
      ];
      const params = {
        data: {
          category_id: this.match.params.type, //分类
          course_id: List[0].id, //课程
          grade_id: List[1].id, //年级
          page: 1
        },
        api: "/api/category"
      };
      getAxios(params, res => {
        let userinfo = res.data.message;
        for (let i = 0; i < userinfo.Menu.length; i++) {
          List[i].cont = userinfo.Menu[i];
        }
        //创建一句话
        const action = handleHomeTypeCourse(List, userinfo);
        dispatch(action);
      });
    },
    // 点击年级
    HomeType_geadeClick(item,selectId) {  
      let list=this.List;
      list[selectId].title = item.name;
      list[selectId].id = item.id;
      // console.log(list)
      // // 学科
      const params = {
        data: {
          category_id: this.match.params.type, //分类
          course_id: list[0].id, //课程
          grade_id: list[1].id, //年级
          page: 1
        },
        api: "/api/category"
      };
      getAxios(params, res => {
        that.setState({
          open: false    
        })
        //创建一句话
        const action = handleHomeTypeCourse(list, res.data.message);
        dispatch(action);
      });
    }
  };
};
// 连接
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeType);
