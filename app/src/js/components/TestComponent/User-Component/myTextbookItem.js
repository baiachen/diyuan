import React from "react";
import history from "../../../history";
export default class myTextbookItem extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    // console.log(this.props.itemData.type)
  }
  render() {
    const { title, course, category,description,teacher_name ,grade} = this.props.itemData;
    return (
      <ul className="HomeItemCont">
        <li>
          <div>{category.name.slice(0, 1)}</div>
          <div>{course.name}</div>
          <div>{grade.title}</div>
          <div>{title}</div>
        </li>
        <li>{description}</li>
        <li>
          <span>{teacher_name}</span>
          <span />
        </li>
        <li>
          <span>
            {this.props.itemData.tags.map((item, i) => {
              return <span key={i}>{item.name}&nbsp;</span>;
            })}
          </span>
          <p />
        </li>
      </ul>
    );
  }
}
