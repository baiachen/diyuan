import React from "react";
import history from "../../../history";
import { Button, List, Modal } from "antd-mobile";
import "../../../../css/User/myInfo.css";
import axios from "axios";
import AxiosRequest from "../../../Axios";
const prompt = Modal.prompt;
export default class MyTextbook extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userInfo: []
    };
  }
  // 点击跳转
  toClick(index) {
    history.push(`/mallTabItemDetail/${index}`);
  }
  async componentDidMount() {
    const {data} = await AxiosRequest.request("/api/uc/userInfo", 'GET', true);
    if(!data.status){
      return false;
    }
    this.setState({
      userInfo: data.message
    });
  }
  // 跳转手机更改
  change_mobile(){
    history.push('/legister')
  }
  render() {
    if (this.state.userInfo.length <= 0) {
      return false;
    }
    return (
      <div>
        {/* 选集 */}
        <List className="myUserInfo">
          <List.Item
            className="user-arrow"
            arrow="horizontal"
            extra={
              <img className="userimg" src={this.state.userInfo.avatarUrl} />
            }
          >
            头像
          </List.Item>
          <List.Item
            className="user-arrow"
            arrow="horizontal"
            extra={this.state.userInfo.nickName}
          >
            昵称
          </List.Item>
          <List.Item
            onClick={
              this.change_mobile.bind(this)
              // () =>
              // prompt(
              //   "电话号码",
              //   "",
              //   [
              //     { text: "取消" },
              //     {
              //       text: "提交",
              //       onPress: value => console.log(`输入的内容:${value}`)
              //     }
              //   ],
              //   "default",
              //   `${this.state.userInfo.mobile}`
              // )
            }
            className="user-arrow"
            arrow="horizontal"
            extra={this.state.userInfo.mobile}
          >
            电话
          </List.Item>
          <List.Item
            onClick={() => {
              history.push("/address");
            }}
            className="user-arrow"
            arrow="horizontal"
            extra={
              this.state.userInfo.address.province +
              this.state.userInfo.address.city +
              this.state.userInfo.address.area +
              this.state.userInfo.address.address
            }
          >
            收获地址
          </List.Item>
        </List>
      </div>
    );
  }
}
