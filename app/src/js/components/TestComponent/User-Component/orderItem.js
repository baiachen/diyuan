import React from "react";
import history from "../../../history";
import { Button } from "antd-mobile";
export default class OrderItem extends React.Component {
  constructor(props) {
    super(props);
  }
  // 做相关事情
  to_event() {
    const info = this.props.orderData;
    switch (parseInt(this.props.orderData.status)) {
      // 待付款0 ,待发货1,待收货2,已完成3
      case 0:
        console.log(0);
        // history.push(`/settlement/${info.userorderable_id}`, "state":{ name:1});
        const location = {
          pathname: `/settlement/${info.userorderable_id}/${info.order_id}`
        };
        // history.push(`/settlement/${info.userorderable_id}?${info.order_id}`)
        history.push(location);
        break;
      case 1:
        console.log(1);
        break;
      case 2:
        console.log(2);
        break;
      case 3:
        console.log(3);
        break;
    }
  }
  render() {
    // 待付款0 ,待发货1,待收货2,已完成3
    const {
      title,
      thumbnail,
      created_at,
      price,
      status
    } = this.props.orderData;
    let is_status = "",
      btn_txt = "";
    switch (parseInt(status)) {
      case 0:
        is_status = "待付款";
        btn_txt = "立即支付";
        break;
      case 1:
        is_status = "待发货";
        btn_txt = "立即发货";
        break;
      case 2:
        is_status = "待收货";
        btn_txt = "立即收获";
        break;
      case 3:
        is_status = "已完成";
        btn_txt = "查看详情";
        break;
    }
    return (
      <ul className="orderItem">
        <li>
          <span>{created_at}</span>
          <span>{is_status}</span>
        </li>
        <li>
          <img src={thumbnail} />
          <div>
            <span>{title}</span>
            <span>
              总价：<strong>{price}元</strong>
            </span>
          </div>
          <Button
            onClick={this.to_event.bind(this)}
            className={status!=3?"orderItem-btn":"orderItem-btn btn-opacity"}
            size="small"
          >
            {btn_txt}
          </Button>
        </li>
      </ul>
    );
  }
}
