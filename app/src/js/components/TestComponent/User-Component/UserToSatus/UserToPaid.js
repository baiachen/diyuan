import React from "react";
import history from "../../../../history";
import PublicSettLement from "../../publicSettLement";
import {Button} from "antd-mobile";
// 待支付
export default class UserToPaid extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="UTH-btns">
      {/* 公共结算界面 */}
      <PublicSettLement/>
      <ul className="UTH-btn">
        <li>
            <span>商品总价</span>
            <span>￥：30</span>
        </li>
        <li>
            <span>运费</span>
            <span>￥：5</span>
        </li>
        <li>
            <span>订单总价</span>
            <span>￥：35</span>
        </li>
      </ul>
      <ul className="UTH-time">
        <li>订单编号：123123123123</li>
        <li>创建时间：2018-06-06</li>
      </ul>
      <div className="UTH-btm-btn">
          <Button  className="UTH-btnOne"  type="primary" size="small">取消订单</Button>
          <Button  className="UTH-btnOne"  type="warning" size="small">付款</Button>
      </div>
    </div>
    );
  }
}
