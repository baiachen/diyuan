import React from "react";
import history from "../../../history";
import { Button, Tabs } from "antd-mobile";
import MyTextbookItem from "./myTextbookItem";
import axios from "axios";
import user from "../../../Axios/user";
import AxiosRequest from "../../../Axios";
const tabs = [
  { title: "全部", id: "all" },
  { title: "数学", id: 0 },
  { title: "语文", id: 1 },
  { title: "英语", id: 2 }
];

export default class MyTextbook extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      TabIndex: 0,
      userInfo: []
    };
  }
  // 点击跳转详情
  toClick(item) {
    switch (parseInt(item.category.id)) {
      case 1:
        history.push(`/videoDetail/${item.id}`);
        break;
      case 2:
        history.push(`/audioDetail/${item.id}`);
        break;
      case 3:
        history.push(`/bookDatil/${item.id}`);
        break;
    }
  }
  async componentDidMount() {
    const { data } = await AxiosRequest.request(
      "/api/uc/material",
      "GET",
      true,
      {
        type: tabs[this.state.TabIndex].id
      }
    );
    console.log(data);
    if (!data.status) {
      return false;
    }
    this.setState({
      userInfo: data.message
    });
  }
  //点击列表子项目
  async itemClick(tab, index) {
    console.log("onTabClick", index, tab);
    console.log(tabs[index].id);

    const { data } = await AxiosRequest.request(
      "/api/uc/material",
      "GET",
      true,
      {
        type: tabs[index].id
      }
    );
    console.log(data);
    if (!data.status) {
      return false;
    }
    this.setState({
      userInfo: data.message,
      TabIndex: index
    });
  }
  render() {
    if (this.state.userInfo.length <= 0) {
      return false;
    }
    return (
      <div>
        {/* 选项卡 */}
        <Tabs
          tabs={tabs}
          tabBarActiveTextColor="#f7463e"
          initialPage={0}
          swipeable={false}
          onChange={(tab, index) => {
            // console.log("onChange", index, tab);
          }}
          onTabClick={(tab, index) => {
            // console.log(tab,index)
            this.itemClick(tab, index);
          }}
        >
          {/* 全部 */}
          <div>
            {this.state.userInfo.data.map((items, i) => {
              return (
                <div key={i} onClick={this.toClick.bind(this, items)}>
                  <MyTextbookItem itemData={items} />
                </div>
              );
            })}
          </div>
          {/* 数学 */}
          <div>
            {this.state.userInfo.data.map((items, i) => {
              return (
                <div key={i} onClick={this.toClick.bind(this, items)}>
                  <MyTextbookItem itemData={items} />
                </div>
              );
            })}
          </div>
          {/* 语文 */}
          <div>
            {this.state.userInfo.data.map((items, i) => {
              return (
                <div key={i} onClick={this.toClick.bind(this, items)}>
                  <MyTextbookItem itemData={items} />
                </div>
              );
            })}
          </div>
          {/* 英文 */}
          <div>
            {this.state.userInfo.data.map((items, i) => {
              return (
                <div key={i} onClick={this.toClick.bind(this, items)}>
                  <MyTextbookItem itemData={items} />
                </div>
              );
            })}
          </div>
        </Tabs>
      </div>
    );
  }
}
