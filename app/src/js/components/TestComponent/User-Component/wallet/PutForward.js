import React from "react";
import history from "../../../../history";
import { Button } from "antd-mobile";
// 提现
export default class PutForward extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="wallet">
        <ul className="wallet-cont">
          <li>
            <img src={require("../../../../../image/qianbi.png")} />
            <span>账户总额</span>
            <h1>￥10.00</h1>
          </li>
          <li>
            <div className="wallet-input">
              <div>金额</div>
              <input placeholder="本次最多可提现40.00元" autoFocus/>
            </div>
            <Button type="warning">确认提现</Button>
          </li>
        </ul>
      </div>
    );
  }
}
