import React from "react";
import history from "../../../../history";
import "../../../../../css/User/wallet.css";
import { Button } from "antd-mobile";
// 待发货
export default class Wallet extends React.Component {
  constructor(props) {
    super(props);
  }
  to_Recharge(){
    history.push('/recharge')
  }
  to_PutForward(){
    history.push('/putForward')
  }
  render() {
    return (
      <div className="wallet">
        <ul className="wallet-cont">
          <li>
            <img src={require("../../../../../image/qianbi.png")} />
            <span>账户总额</span>
            <h1>￥10.00</h1>
          </li>
          <li>
            <Button onClick={this.to_Recharge.bind(this)} className="wallet-Recharge" type="warning">
              充值
            </Button>
            <Button onClick={this.to_PutForward.bind(this)}  >提现</Button>
          </li>
        </ul>
      </div>
    );
  }
}
