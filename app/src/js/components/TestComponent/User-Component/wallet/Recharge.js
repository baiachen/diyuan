import React from "react";
import history from "../../../../history";
import { Button } from "antd-mobile";
import "../../../../../css/User/wallet.css";
// 充值
export default class Recharge extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="Recharge">
        <ul className="Recharge-cont">
          <li>充值方式</li>
          <li>
            <img src={require("../../../../../image/weixing.png")} />
            <div>
              <p>微信支付</p>
              <p>推荐安装微信5.0以上版本用户使用</p>
            </div>
          </li>
        </ul>
        {/*  */}
        <div className="Recharge-title">充值金额</div>
        <div className="Recharge-input">
            <span>￥</span>
            <input type="text" name="fname" autoFocus/>
        </div>
        <Button style={{"color":"#ababab"}}>充值</Button>
      </div>
    );
  }
}
