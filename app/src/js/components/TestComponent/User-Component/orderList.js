import React from "react";
import { Tabs, Badge } from "antd-mobile";
import history from "../../../history";
import OrderItem from "./orderItem";
import AxiosRequest from "../../../Axios";
const itemNum = [
  { title: "全部", id: "all" },
  // { title: <Badge text={"3"}>待支付</Badge>, id: 0 },
  // { title: <Badge text={"3"}>待发货</Badge>, id: 1 },
  // { title: <Badge text={"3"}>待收货</Badge>, id: 2 },
  // { title: <Badge text={"3"}>已完成</Badge>, id: 3 }
  { title: "待支付", id: 0 },
  { title: "待发货", id: 1 },
  { title: "待收货", id: 2 },
  { title: "已完成", id: 3 }
];
export default class OrderList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      TabIndex: this.props.match.params.id,
      userInfo: []
    };
    this.itemClick = this.itemClick.bind(this);
  }
  // 点击请求
  toClick(items, i) {}
  //点击列表子项目
  async itemClick(tab, index) {
    console.log("onTabClick", index, tab);
    const { data } = await AxiosRequest.request(
      "/api/uc/orders",
      "GET",
      true,
      {
        type: itemNum[index].id
      }
    );
    console.log(data);
    if (!data.status) {
      return false;
    }
    this.setState({
      userInfo: data.message,
      TabIndex: index
    });
  }
  async componentDidMount() {
    const { data } = await AxiosRequest.request("/api/uc/orders", "GET", true, {
      type: itemNum[this.state.TabIndex].id
    });
    if (!data.status) {
      return false;
    }
    this.setState({
      userInfo: data.message
    });
  }
  render() {
    if (this.state.userInfo.length <= 0) {
      return false;
    }
    console.log(this.state.userInfo.data.data);
    // console.log(this.state.TabIndex);
    return (
      <div className="TabCont orderList">
        <Tabs
          tabs={itemNum}
          className="Tab-cont"
          tabBarActiveTextColor="#f7463e"
          initialPage={parseInt(this.state.TabIndex)}
          swipeable={false}
          onChange={(tab, index) => {
            // console.log("onChange", index, tab);
            // this.itemClick.bind(this, tab, index);
          }}
          onTabClick={(tab, index) => {
            // console.log(tab,index)
            this.itemClick(tab, index);
          }}
        >
          {/* 全部 */}
          <div className="TabItem">
            {this.state.userInfo.data.data.map((items, i) => {
              return (
                <div key={i} onClick={this.toClick.bind(this, items, i)}>
                  <OrderItem orderData={items} />
                </div>
              );
            })}
          </div>
          {/* 待支付 */}
          <div className="TabItem">
            {this.state.userInfo.data.data.map((items, i) => {
              return (
                <div key={i} onClick={this.toClick.bind(this, items, i)}>
                  <OrderItem orderData={items} />
                </div>
              );
            })}
          </div>
          {/* 待发货 */}
          <div className="TabItem">
            {this.state.userInfo.data.data.map((items, i) => {
              return (
                <div key={i} onClick={this.toClick.bind(this, items, i)}>
                  <OrderItem orderData={items} />
                </div>
              );
            })}
          </div>
          {/* 待收货 */}
          <div className="TabItem">
            {this.state.userInfo.data.data.map((items, i) => {
              return (
                <div key={i} onClick={this.toClick.bind(this, items, i)}>
                  <OrderItem orderData={items} />
                </div>
              );
            })}
          </div>
          {/* 待评价 */}
          <div className="TabItem">
            {this.state.userInfo.data.data.map((items, i) => {
              return (
                <div key={i} onClick={this.toClick.bind(this, items, i)}>
                  <OrderItem orderData={items} />
                </div>
              );
            })}
          </div>
        </Tabs>
      </div>
    );
  }
}
