import React from "react";
import history from "../../../history";
import { Button, List, Tabs, Modal } from "antd-mobile";
import store from "../../../store";
import { handleTabchange } from "../../../store/actionCtreator";
import AxiosRequest from "../../../Axios";

const tabs = [
  { title: "公司介绍" },
  { title: "公司展示" },
  { title: "联系我们" }
];
export default class PlatformIntroduction extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userInfo: []
    };
  }
  // 点击跳转
  toClick(index) {
    history.push(`/mallTabItemDetail/${index}`);
  }
  async componentDidMount() {
    const {data} = await AxiosRequest.request("/api/uc/notes", 'GET', true);
    if(!data.status){
      return false;
    }
    this.setState({
      userInfo: data.message
    });
  }
  render() {
    if (this.state.userInfo.length <= 0) {
      return false;
    }
    return (
      <div>
        <Tabs
          tabs={tabs}
          className="PFT-cont"
          tabBarActiveTextColor="#f7463e"
          initialPage={0}
          swipeable={false}
          onTabClick={(tab, index) => {}}
        >
          {/* 公司介绍 */}
          {/* <div className="PFT-TabItem">
            <img src={require("../../../../image/2.jpg")} />
            <div className="PFT-TabItem-cont">
              <h3>标题</h3>
              <div
                dangerouslySetInnerHTML={{
                  __html: `${this.state.userInfo[0].content}`
                }}
              />
            </div>
          </div> */}
          <div
            dangerouslySetInnerHTML={{
              __html: `${this.state.userInfo[0].content}`
            }}
          />
          <div
            dangerouslySetInnerHTML={{
              __html: `${this.state.userInfo[1].content}`
            }}
          />
          <div
            dangerouslySetInnerHTML={{
              __html: `${this.state.userInfo[2].content}`
            }}
          />
          {/* 公司展示 */}
          {/* <div className="PFT-TabItem">
            <img src={require("../../../../image/4271002_mwsvt1el.jpg")} />
           
          </div> */}
          {/* 联系我们 */}
          {/* <div className="PFT-TabItem">
            <List className="myUserInfo">
              <List.Item className="user-arrow"  extra="钱先生">
                联系人
              </List.Item>
              <List.Item className="user-arrow"  extra="15057978044">
                联系电话
              </List.Item>
              <List.Item className="user-arrow"  extra="08：00—20：00">
                营业时间
              </List.Item>
              <List.Item className="user-arrow"  extra="浙江省金华市浙中信息产业园">
                地址
              </List.Item>
            </List>
          </div> */}
        </Tabs>
      </div>
    );
  }
}
