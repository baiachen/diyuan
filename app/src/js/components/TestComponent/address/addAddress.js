import React, { Fragment } from "react";
import { List, Picker, Toast, Button, Checkbox } from "antd-mobile";
import { connect } from "react-redux";
import { PostAxiosToken } from "../../../store/request";
import history from "../../../history";
import addressData from "./addressData";
import { handAddress } from "../../../store/actionCtreator";
import AxiosRequest from "../../../Axios";

// stroe用setState
let that = "";
class addAddress extends React.Component {
  constructor(props) {
    super(props);
    that = this;
    this.state = {
      pickerValue: [],
      is_check: true,
      is_disabled: false
    };
    this.ChangeCheckBox = this.ChangeCheckBox.bind(this);
  }
  componentDidMount() {
    // 请求类型
    console.log(this.props.match.params.type);
    if(this.props.match.params.type==2){
      this.props.getUserInfo(this.props.match.params.id);
    }
  }
  ChangeCheckBox(e) {
    this.setState({
      is_check: e.target.checked
    });
  }
  render() {
    return (
      <Fragment>
        <div className="address-container">
          <img
            className="xiantiao"
            src={require("../../../../image/xiantiao.png")}
          />
        </div>
        <List style={{ backgroundColor: "white" }} className="picker-list">
          <List.Item
            extra={
              <input
                ref={input => {
                  this.username = input;
                }}
                className="address-input"
                placeholder="姓名"
              />
            }
          >
            姓名
          </List.Item>
          <List.Item
            extra={
              <input
                ref={input => {
                  this.mobile = input;
                }}
                className="address-input"
                placeholder="手机号码"
              />
            }
          >
            手机号码
          </List.Item>
          <Picker
            title="选择地区"
            extra="请选择"
            data={addressData}
            value={this.state.pickerValue}
            onChange={v => this.setState({ pickerValue: v })}
            onOk={v => this.setState({ pickerValue: v })}
          >
            <List.Item arrow="horizontal">收获地址</List.Item>
          </Picker>
          <List.Item
            extra={
              <input
                ref={input => {
                  this.addressDetail = input;
                }}
                className="address-input"
                placeholder="详细地址"
              />
            }
          >
            详细地址
          </List.Item>
        </List>
        <Checkbox
          className="Addaddress-checkbox"
          onChange={this.ChangeCheckBox}
          checked={this.state.is_check}
        >
          设为默认地址
        </Checkbox>
        <div className="addAddress-btn" onClick={this.props.submit}>
          <Button type="warning" disabled={this.state.is_disabled}>
            确认
          </Button>
        </div>
      </Fragment>
    );
  }
}

// 映射规则
const mapStateToProps = state => {
  return {};
};
// 提交dispatch
const mapDispatchToProps = dispatch => {
  return {
    // 页面请求(编辑)
    async getUserInfo(id) {
      const { data } = await AxiosRequest.request(
        "/api/user/address_edit",
        "GET",
        true,
        {
          id: id
        }
      );
      if (!data.status) {
        return false;
      }
      const msg = data.message;
      that.setState({
        pickerValue: [msg.province, msg.city, msg.area]
      });
      that.username.value = msg.consignee;
      that.mobile.value = msg.mobile;
      that.addressDetail.value = msg.address;
      // that.state.is_check;
    },
    //   表单提交
    async submit() {
      // console.log(this.state.pickerValue);
      const myreg = /^[1][3,4,5,7,8,9][0-9]{9}$/;
      const typeList=that.props.match.params.type==2;//编辑地址
      const param = {
        username: that.username.value,
        mobile: that.mobile.value,
        address: that.state.pickerValue,
        addressDetail: that.addressDetail.value,
        is_default: that.state.is_check
      };
      typeList?param.id = that.props.match.params.id:"";
      if (param.username == "") {
        Toast.fail("姓名不能为空", 1);
        return false;
      }
      if (param.mobile == "") {
        Toast.fail("手机号不能为空", 1);
        return false;
      }
      if (!myreg.test(param.mobile)) {
        Toast.fail("手机号码格式错误", 1);
        return false;
      }
      if (param.address.length <= 0) {
        Toast.fail("收获地址不能为空", 1);
        return false;
      }
      if (param.addressDetail == "") {
        Toast.fail("详细地址不能为空", 1);
        return false;
      }
      that.setState({
        is_disabled: true
      });
      const url =
      typeList
          ? "/api/user/address_update"
          : "/api/user/address";
      const text =
      typeList ? "编辑地址成功" : "添加地址成功";
      const { data } = await AxiosRequest.request(url, "POST", true, param);
      if (!data.status) {
        that.setState({
          is_disabled: false
        });
        return false;
      }
      Toast.success(text, 1);
      history.go(-1);
    }
  };
};
// 连接
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(addAddress);
