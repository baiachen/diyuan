import React, { Fragment } from "react";
import { Checkbox, Icon, Toast, Button,Modal } from "antd-mobile";
import axios from "axios";
import { connect } from "react-redux";
import history from "../../../history";
import "./style.css";
import AxiosRequest from "../../../Axios";
const alert = Modal.alert;
let that = "";
// const CheckboxItem = Checkbox.CheckboxItem;
class Address extends React.Component {
  constructor(props) {
    super(props);
    that = this;
    this.state = {
      userInfo: []
    };
    this.deleteAddress = this.deleteAddress.bind(this);
    this.editAddress = this.editAddress.bind(this);
  }
  async componentDidMount() {
    // 页面请求
    const { data } = await AxiosRequest.request(
      "/api/user/address",
      "GET",
      true
    );
    if (!data.status) {
      return false;
    }
    that.setState({
      userInfo: data.message
    });
  }
  // 添加地址
  to_addAddress() {
    history.push("/addAddress/1/0");
  }
  //   编辑地址
  editAddress(item, index) {
    history.push(`/addAddress/2/${item.id}`);
  }
  //   删除地址
  async deleteAddress(item, index) {
    const that = this;
    alert("温馨提醒", "确认删除?", [
      {
        text: "取消",
        style: { color: "red" }
      },
      {
        text: "确认",
        onPress: async () => {
          const { data } = await AxiosRequest.request(
            "/api/user/address_destroy",
            "POST",
            true,
            { id: item.id }
          );
          if (!data.status) {
            return false;
          }
          let bankCardList = that.state.userInfo;
          bankCardList.splice(index, 1);
          if (bankCardList.length > 0) {
            bankCardList[0].is_default = 1;
          }
          that.setState({
            userInfo: bankCardList
          });
        },
        style: { background: "red", color: "#fff" }
      }
    ]);
  }
  // 选择默认值
  async hangdleCheckbox(item, index) {
    // 点击谁谁的默认值就为1,没有默认值的才能点
    if(item.is_default==0){
      const { data } = await AxiosRequest.request(
        "/api/user/address_change",
        "POST",
        true,
        { id: item.id }
      );
      if (!data.status) {
        return false;
      }
      let Ba_Lists = this.state.userInfo;
      for (let i of Ba_Lists) {
        i.is_default = 0;
      }
      Ba_Lists[index].is_default = 1;
      this.setState({
        userInfo: Ba_Lists
      });
    }
  }
  render() {
    // const {} = this.props;
    // if (this.state.userInfo.length <= 0) {
    //   return false;
    // }
    return (
      <div className="address-page">
        {this.state.userInfo.map((item, index) => {
          return (
            <div className="address-container" key={index}>
              <img
                className="xiantiao"
                src={require("../../../../image/xiantiao.png")}
              />
              <div className="address-cont">
                <span>{item.consignee}</span>
                <span>{item.mobile}</span>
                <p>
                  {String(item.province + item.city + item.area + item.address)}
                </p>
                <div className="address-btm">
                  <Checkbox
                    className="address-checkbox"
                    onChange={this.hangdleCheckbox.bind(this, item, index)}
                    checked={item.is_default === 1}
                  >
                    设为默认地址
                  </Checkbox>
                  <ul>
                    <li
                      onClick={() => {
                        this.editAddress(item, index);
                      }}
                    >
                      <i className="iconfont icon-bbgbianji" />
                      <span>编辑</span>
                    </li>
                    <li
                      onClick={() => {
                        this.deleteAddress(item, index);
                      }}
                    >
                      <i className="iconfont icon-shanchu" />
                      <span>删除</span>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          );
        })}
        <div className="addAddress-btn" onClick={this.to_addAddress}>
          <Button type="warning">添加地址</Button>
        </div>
      </div>
    );
  }
}

// 连接
export default Address;
