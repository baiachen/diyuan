import React from "react";
import { Icon, Button, Modal, Toast } from "antd-mobile";
import { getAxios } from "../../store/request";
import history from "../../history";
import AxiosRequest from "../../Axios";
import {
  handleLogin,
  handleDetail
} from "../TestComponent/Home-Component/HomeTypeDetail/public";
const alert = Modal.alert;
export default class PublicBtn extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // 是否收藏
      isCollect: this.props.data.isCollect
    };
  }
  // 点击购买
  clickConfirm(e) {
    let userInfo = this.props.data;
    let id = this.props.data.id;
    let is_free = this.props.free;
    if (userInfo.price <= 0) {
      console.log("免费视频");
      Toast.info("此产品为免费产品,无需购买", 1);
      // 是免费的话
    } else {
      console.log("收费视频");
      // 必须先登录
      handleLogin(() => {
        //   如果没有购买过的话
        if (!userInfo.isBuy) {
          console.log("未购买");
          alert("支付提醒", `本次需要支付费用￥${userInfo.price}元是否继续`, [
            {
              text: "取消",
              style: { color: "red" }
            },
            {
              text: "确认支付",
              onPress: () => {
                history.push(`/settlement/${id}/0`);
              },
              style: { background: "red", color: "#fff" }
            }
          ]);
        } else {
          // 如果购买过了
          console.log("已购买");
          Toast.info("此产品您已购买过", 1);
        }
      });
    }
  }
  // 咨询客服
  clickBtn_tell() {
    alert("温馨提醒", "是否拨打电话咨询客服", [
      {
        text: "取消",
        onPress: () => console.log("取消"),
        style: { color: "red" }
      },
      {
        text: "前往",
        onPress: () => {
          console.log("确认");
          console.log(this.props.data.phone)
          window.location.href = `tel:${this.props.data.phone}`;
        },
        style: { background: "red", color: "#fff" }
      }
    ]);
  }
  // 收藏
  clickBtn_collection() {
    // 必须登录
    handleLogin(() => {
      console.log("收藏登录验证");
      alert("温馨提醒", "是否确认收藏", [
        {
          text: "取消",
          onPress: () => console.log("取消"),
          style: { color: "red" }
        },
        {
          text: "确认",
          onPress: async () => {
            console.log("确认");
            const { data } = await AxiosRequest.request(
              "/api/topics/collect",
              "GET",
              true,
              {
                id: this.props.data.id
              }
            );
            if (!data.status) {
              return false;
            }
            Toast.success(data.message, 1.5);
            this.setState({
              isCollect: !this.state.isCollect
            });
          },
          style: { background: "red", color: "#fff" }
        }
      ]);
    });
  }
  render() {
    return (
      <div>
        {/* 底部按钮 */}
        <div className="HTD-btm">
          <ul>
            <li onClick={this.clickBtn_collection.bind(this)}>
              {!this.state.isCollect ? (
                <p>
                  <i className="iconfont icon-shoucang" />
                  <span>收藏</span>
                </p>
              ) : (
                <p>
                  <i className="iconfont icon-shoucang-yellow" />
                  <span className="HTD-already">已收藏</span>
                </p>
              )}
            </li>
            <li onClick={this.clickBtn_tell.bind(this)}>
              <i className="iconfont icon-zixun" />
              <span>咨询</span>
            </li>
          </ul>
          <div onClick={this.clickConfirm.bind(this)}>立即购买</div>
        </div>
      </div>
    );
  }
}
