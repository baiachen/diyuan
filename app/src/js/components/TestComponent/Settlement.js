import React from "react";
import { Icon, Button, Modal, Stepper, List, Toast } from "antd-mobile";
import "../../../css/settlement/settlement.css";
import history from "../../history";
import PublicSettLement from "./publicSettLement";
import AxiosRequest from "../../Axios";
const alert = Modal.alert;
// 支付界面
export default class Settlement extends React.Component {
  constructor(props) {
    super(props);
    document.title = "支付订单";
    this.state = {
      modal1: false,
      userInfo: [],
      // 备注
      remarks: "",
      // 地址信息
      addressInfo: {},
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.to_address = this.to_address.bind(this);
  }

  async componentDidMount() {
    console.log(this.props)
    const { data } = await AxiosRequest.request(
      "/api/topics/order",
      "GET",
      true,
      {
        id: this.props.match.params.id
      }
    );
    if (!data.status) {
      return false;
    }
    console.log(data.message.config)
    window.wx.config({
      debug: data.message.config.debug,
      appId: data.message.config.appId,
      timestamp:data.message.config.timestamp,
      nonceStr:data.message.config.nonceStr,
      signature: data.message.config.signature,
      jsApiList:data.message.config.jsApiList
    });

    this.setState({
      userInfo: data.message
    });
  }
  onClose = key => () => {
    this.setState({
      [key]: false
    });
  };
  showModal = key => e => {
    e.preventDefault(); // 修复 Android 上点击穿透
    this.setState({
      [key]: true
    });
  };
  // 改变input
  handleInputChange(e) {
    console.log(e.target.name + ":" + e.target.value);
    this.setState({
      [e.target.name]: e.target.value
    });
  }
  // 设置默认地址
  to_address() {
    console.log(1111111);
    history.push("/address");
  }
  // 点击提交订单
  async clickConfirm(e) {
    
    let that = this,
      userInfo = this.state.userInfo;
      
    const params = {
      remarks: this.state.remarks,
      id: this.props.match.params.id,
      price: userInfo.price,
      // address:
      //   userInfo.province + userInfo.city + userInfo.area + userInfo.address,
      consignee: userInfo.consignee,
      mobile: userInfo.mobile,
      type: 1, //首页下支付,商城是2
      order_id: this.props.match.params.order_id
    };
    // if(parseInt(this.props.match.params.order_id)>0){
    //   params.order_id= this.props.match.params.order_id;
    // }
    if (!userInfo.hasDefaultAddress) {
      this.onClose("modal1")();
      alert("温馨提醒", "您当前未设置默认地址,是否去前往设置", [
        {
          text: "取消",
          onPress: () => console.log("取消"),
          style: { color: "red" }
        },
        {
          text: "前往",
          onPress: () => {
            console.log("确认");
            history.push("/address");
          },
          style: { background: "red", color: "#fff" }
        }
      ]);
      return false;
    }

    const { data } = await AxiosRequest.request(
      "/api/order/pay",
      "POST",
      true,
      params
    );
    if (!data.status) {
      return false;
    }
    window.wx.chooseWXPay({

      timestamp: data.message.timestamp, // 支付签名时间戳，注意微信jssdk中的所有使用timestamp字段均为小写。但最新版的支付后台生成签名使用的timeStamp字段名需大写其中的S字符
      nonceStr: data.message.nonceStr, // 支付签名随机串，不长于 32 位
      package: data.message.package, // 统一支付接口返回的prepay_id参数值，提交格式如：prepay_id=\*\*\*）
      signType: data.message.signType, // 签名方式，默认为'SHA1'，使用新版支付需传入'MD5'
      paySign: data.message.paySign, // 支付签名
      success: function(res) {
        // 支付成功后的回调函数
        console.log(res);
        Toast.success("购买成功", 1);
        history.push("/");
      }
    });

    this.onClose("modal1")();
  }
  render() {
    return (
      <div>
        {/* 公共组件 */}
        <PublicSettLement
          data={this.state.userInfo}
          to_address={() => this.to_address}
        />
        {/* 计步器 */}
        {/* <List>
          <List.Item
            className="book-userLIst sm-list"
            extra={
              <Stepper
                style={{ width: "100%" }}
                showNumber
                size="small"
                defaultValue={20}
              />
            }
          >
            <span>购买数量</span>
          </List.Item>
        </List> */}
        {/* 运费 */}
        {/* <List>
          <List.Item
            className="book-userLIst sm-list"
            extra={<strong style={{ color: "#f7463e" }}>￥5</strong>}
          >
            <span>运费</span>
          </List.Item>
        </List> */}
        <List>
          <List.Item
            className="book-userLIst sm-list sm-Remarks"
            extra={
              <input
                onChange={this.handleInputChange}
                value={this.state.remarks}
                className="sm-input"
                placeholder="请输入"
                name="remarks"
              />
            }
          >
            <span>备注</span>
          </List.Item>
        </List>
        {/* <div className="PurchaseNotice">购买须知：文字文字文字</div> */}
        <div>
          {/* 底部按钮 */}
          <div className="HTD-btm sm-btm-btn">
            <p>
              合计金额：<span>￥{this.state.userInfo.price}</span>
            </p>
            <div onClick={this.showModal("modal1")}>提交订单</div>
          </div>
          <Modal
            visible={this.state.modal1}
            transparent
            maskClosable={false}
            onClose={this.onClose("modal1")}
            title="支付提示"
            wrapClassName="payment-wrapmodal"
            footer={[
              {
                text: "取消",
                onPress: () => {
                  console.log("取消");
                  this.onClose("modal1")();
                }
              },
              {
                text: "确认",
                onPress: this.clickConfirm.bind(this),
                style: { background: "red", color: "#fff" }
              }
            ]}
          >
            是否确认提交订单
          </Modal>
        </div>
      </div>
    );
  }
}
