import React from 'react';
import { TabBar ,Toast} from 'antd-mobile';
import history from '../../history';
import { Link} from "react-router-dom";
import Home from '../Home';
import Mall from '../Mall';
import User from '../User';
export default class TabBarExample extends React.Component {
  constructor(props) {
    super(props);
    // console.log(this.props.selectedTab);
    this.state={
      selectedTab:parseInt(this.props.selectedTab)
    }
  }
  render() {
    return (
      <div className="TabBarCont">
        <TabBar
          unselectedTintColor="#949494"
          tintColor="#f7473e"
          barTintColor="white"
        >
          <TabBar.Item
            title="首页"
            key="index"
            icon={<i className="iconfont icon-boshimao"></i>}
            selectedIcon={<i className="iconfont icon-boshimao"></i>}
            selected={this.state.selectedTab === 0}
            onPress={() => {
              this.setState({
                selectedTab: 0,
              });
              history.push('/');
            }}
            data-seed="logId"
          >
          </TabBar.Item>
          <TabBar.Item
            icon={<i className="iconfont icon-store"></i>}
            selectedIcon={<i className="iconfont icon-store"></i>}
            title="商城"
            key="shop"
            selected={this.state.selectedTab === 1}
            onPress={() => {
              this.setState({
                selectedTab: 1,
              });
              Toast.info('敬请期待', 2);
              // history.push('/mall');
            }}
            data-seed="logId1"
          >
          </TabBar.Item>
          <TabBar.Item
            icon={<i className="iconfont icon-sousuoxiawu-"></i>}
            selectedIcon={<i className="iconfont icon-sousuoxiawu-"></i>}
            title="我的"
            key="user"
            selected={this.state.selectedTab ===2}
            onPress={() => {
              this.setState({
                selectedTab: 2,
              });
              history.push('/user');
            }}
          >
          </TabBar.Item>
        </TabBar>
      </div>
    );
  }
}