import React from "react";
import { Icon, Button, Modal } from "antd-mobile";
import axios from "axios";
import { getAxios } from "../../store/request";
export default class PublicSettLement extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const {data} =this.props;
    return (
      <div>
        {/* 是否有默认地址 */}
        {data.hasDefaultAddress ? (
          <div className="sm-top" onClick={this.props.to_address()}>
            <span>{data.address.consignee}&nbsp;&nbsp;{data.address.mobile}</span>
            <div>
            {data.address.province+data.address.city + data.address.area + data.address.address }
            
            </div>
            <i className="iconfont icon-arrow-right" />
          </div>
        ) : (
          <div className="sm-top">
            <Button onClick={this.props.to_address()}>
              点击前往设置默认地址
            </Button>
          </div>
        )}
        <img
          className="xiantiao"
          src={require("../../../image/xiantiao.png")}
        />
        <div className="sm-item">
          <img src={data.thumbnail} />
          <ul>
            <li>{data.title}</li>
            <li>{data.description}</li>
            <li>
              <span>￥{data.price}</span>
              <span>X1</span>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}
