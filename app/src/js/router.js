import React from "react";
import { Router, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./store";
import TabBarExample from "./components/TestComponent/Tabs";
import createHistory from "./history";
import Login from "./components/Login"; //登录
import Legister from "./components/Legister"; //注册
import Settlement from "./components/TestComponent/Settlement"; //结算
import Home from "./components/Home"; //首页
import Address from "./components/TestComponent/address/index"; //地址
import addAddress from "./components/TestComponent/address/addAddress"; //添加地址
import HomeType from "./components/TestComponent/Home-Component/HomeType/HomeType"; //首页分类
import VideoDetail from "./components/TestComponent/Home-Component/HomeTypeDetail/VideoDetail"; //视频分类详情
import AudioDetail from "./components/TestComponent/Home-Component/HomeTypeDetail/AudioDetail"; //音频分类详情
import BookDetail from "./components/TestComponent/Home-Component/HomeTypeDetail/BookDetail"; //电子书分类详情
import AllUserList from "./components/TestComponent/Home-Component/HomeTypeDetail/AllUserList"; //全部章节列表
import HomeRelevant from "./components/TestComponent/Home-Component/HomeTypeDetail/HomeRelevant"; //全部章节列表
import Mall from "./components/Mall"; //商城
import MallTabItemDetailComponent from "./components/TestComponent/Mall-Component/tabItemDetail"; //选项详情
import User from "./components/User"; //用户
import OrderList from "./components/TestComponent/User-Component/orderList"; //用户
import UserToCompleted from "./components/TestComponent/User-Component/UserToSatus/UserToCompleted"; //用户
import UserToDelivery from "./components/TestComponent/User-Component/UserToSatus/UserToDelivery"; //用户
import UserToHarvested from "./components/TestComponent/User-Component/UserToSatus/UserToHarvested"; //用户
import UserToPaid from "./components/TestComponent/User-Component/UserToSatus/UserToPaid"; //用户
import Wallet from "./components/TestComponent/User-Component/wallet"; //钱包
import Recharge from "./components/TestComponent/User-Component/wallet/Recharge"; //充值
import PutForward from "./components/TestComponent/User-Component/wallet/PutForward"; //提现
import CustomerService from "./components/TestComponent/User-Component/customerService"; //客服
import MyTextbook from "./components/TestComponent/User-Component/myTextbook"; //我的教材
import MyInfo from "./components/TestComponent/User-Component/myInfo"; //我的资料
import PlatformIntroduction from "./components/TestComponent/User-Component/PlatformIntroduction"; //平台介绍
import "../css/style.css";
import "../css/video.css";
const BasicExample = () => (
  <Provider store={store}>
    <Router history={createHistory}>
      {/* 路由配置 */}
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/Mall" component={Mall} />
        <Route path="/User" component={User} />
        <Route path="/legister" component={Legister} />
        <Route path="/login" component={Login} />
        <Route path="/address" component={Address} />
        <Route path="/addAddress/:type/:id"  component={addAddress} />
        <Route path="/settlement/:id/:order_id" component={Settlement} />
        <Route path="/home/:type" component={HomeType} />
        <Route path="/homeAllUserList/:type/:id/:price" component={AllUserList} />
        <Route path="/homeRelevant/:id" component={HomeRelevant} />
        <Route path="/videoDetail/:id" component={VideoDetail} />
        <Route path="/audioDetail/:id" component={AudioDetail} />
        <Route path="/bookDatil/:id" component={BookDetail} />
        <Route
          path="/mallTabItemDetail/:id"
          component={MallTabItemDetailComponent}
        />
        <Route path="/UserOrderList/:id" component={OrderList} />
        <Route path="/UserToPaid" component={UserToPaid} />
        {/*待支付*/}
        <Route path="/UserToDelivery" component={UserToDelivery} />
        {/* 待发货 */}
        <Route path="/UserToHarvested" component={UserToHarvested} />
        {/* 待收获 */}
        <Route path="/UserToCompleted" component={UserToCompleted} />
        {/* 已完成 */}
        <Route path="/wallet" component={Wallet} />
        {/* 钱包 */}
        <Route path="/recharge" component={Recharge} />
        {/* 充值 */}
        <Route path="/putForward" component={PutForward} />
        {/* 提现 */}
        <Route path="/myTextbook" component={MyTextbook} />
        {/* 我的教材 */}
        <Route path="/myInfo" component={MyInfo} />
        {/* 我的资料 */}
        <Route path="/platformIntroduction" component={PlatformIntroduction} />
        {/* 平台介绍 */}
        <Route path="/customerService" component={CustomerService} />
        {/* 客服信息 */}
      </Switch>
    </Router>
  </Provider>
);

export default BasicExample;
