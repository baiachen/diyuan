

import axios from 'axios'
import User from './user';
import { Modal,Toast} from "antd-mobile";
import history from "../history";
// console.log(User)


// ************************添加响应拦截器
axios.interceptors.response.use(function (response) {
  // 对响应数据做点什么
  errorMessage(response)
  return response;
}, function (error) {
  // 对响应错误做点什么
  console.log(error)
  let status = error.response ? error.response.status : 400
  console.log('错误状态码', status)
  return Promise.reject(error);
});


// token失效处理
function errorMessage(response) {
  if (response.data.code === 401) {
    const {u_id} = User.getLoginInfo();
    User.clearLoginInfo()
    axios({
      url:'/api/login/refreshToken',
      method:'POST',
      data:{u_id},
    }).then((res)=>{
      console.log(res)
      User.setLoginInfo(res.data.message)

    }).catch(e=>{

    })

  } else if (response.data.code === 0) {
    // console.log("状态码等于0");
    // Toast.info(res.data.message, 1);
  }
}

