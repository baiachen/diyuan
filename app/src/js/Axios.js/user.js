class User {
    constructor() {
      try{
        let token = JSON.parse(localStorage.getItem("token")) || {};
      }catch(e){
        alert('token 获取失败')
      }
      this.loginInfo = token;
    }


    isLogin() {
      let {
        is_login,
        // u_id,
        // time,
        token
      } = JSON.parse(localStorage.getItem('token')) || {}
      if ( is_login === undefined||token===undefined) {
        return false
      } else {
        return true
      }
    }

    
    getLoginInfo() {
      return this.loginInfo
    }
    setLoginInfo(obj) {
      localStorage.setItem('token', JSON.stringify(obj))
      this.loginInfo = obj
    }
    clearLoginInfo() {
      localStorage.removeItem('token')
      // localStorage.removeItem('userInfo')
    }
    updateInfo(obj) {
      this.setLoginInfo(obj)
    }
    getUserInfo() {
      return this.loginInfo.userInfo || {}
    }
    getAccessToken() {
      return this.loginInfo || {}
    }

    getToken(){
      return this.loginInfo.token || ''
    }

    setAccessToken(refreshToken) {
      console.log(refreshToken)
      this.loginInfo.is_login = refreshToken
      this.setLoginInfo(this.loginInfo)
    }
    getFileServerPath() {
      return this.loginInfo.fileServerPath || "";
      // {"is_login":"1",
      // "token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjksImlzcyI6Imh0dHBzOi8vd3guNzMzMDUuY29tL2FwaS9sb2dpbi9jYWxsYmFjayIsImlhdCI6MTUzMjM1Mzc2NSwiZXhwIjoxNTMyMzU3MzY1LCJuYmYiOjE1MzIzNTM3NjUsImp0aSI6IjR6QzBaa3F6bHhqV3RESHMifQ.vu7XlnClV2vw4GH4LoMvMzHAp4OjyyxJ6MpKu3j78_Y",
      // "u_id":"dJy1MVg6GX2NKlQ98kAm8On4ZjwLPoYr",
      // "XSRF-TOKEN":"eyJpdiI6Imk0eThadW81b1Jrenkrb1JTQVVScWc9PSIsInZhbHVlIjoiYTZyWVdzRyt4Y21vYnV4OGU1OXcxcEV2UkplSW9vYTJGS0ZlRXdhcXZKZXVPbFJOTHdQcXI1cTVyWko4dlRsWjIyNnkrZ3N6TGlvVVVOSmZTMllNSlE9PSIsIm1hYyI6IjUyYzNhN2MxMThiM2JkZGE5MGUxYWJkNTlhMTEzOWU0NGViN2NjODY0Nzk2ZDFkNzcxNjM3MzllMTU5NjRjZjkifQ=="
      // ,"time":1532353765871}
    }
  }
  
  export default new User()
  


