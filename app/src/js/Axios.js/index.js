import axios from "axios";
// 导入请求处理
import "./interceptors/request";
// 导入响应处理
import "./interceptors/response";
import User from "./user";
import { Modal, Toast } from "antd-mobile";
import history from "../history";


class AxiosRequest {
  constructor() {
    this.refreshLock = false;
  }
  // 创建一个请求,参数为请求方式,默认get
  request(url, params, method = "GET", token=false) {
    let config = {
      headers: {},
      url,
      method,
    };

    // 判断请求的方式来把参数放在data里面还是params里面
    if (method === "PUT" || method === "POST" || method === "DELETE") {
      config.data = params;
    } else if (method === "GET") {
      config.params = params || {};
    } else {
    }
    // 将token添加到headers
    if (token) {
      config.headers.Authorization = `Bearer ${token}`
    }
    return axios.request(config);
  }

  // // 从本地获取Token,并判断是否过期需要刷新
  // getStorageToken() {
  //   // 从本地存储中获取token
  //   let tokenData = User.getLoginInfo();
  //   if (tokenData.is_login) {
  //     if (tokenData.time - new Date().getTime() > 60 * 60 * 1000) {
  //       Toast.fail("Token已过期", 1);
  //       axios({
  //         method: "POST",
  //         url: "/api/login/refreshToken",
  //         data: {
  //           u_id: tokenData.u_id
  //         }
  //       }).then(res => {
  //         tokenData.token = res.data.message.token;
  //         localStorage.setItem("token", JSON.stringify(tokenData));
  //       });
  //     }
  //   }
  // }
}

// 返回一个实例化对象
export default new AxiosRequest();









/**
 *    AxiosRequest 请求对象
 *    方法:
 *
 *    request(url, params, method = 'GET').then(res => {}).catch(err => {})
 *
 *
 *
 *                                                      ----------->  request normal
 *                                                      |
 *                                                      |
 *    request ------> getStorageToken ------> refresh ? |
 *                                                      |
 *                                                      |
 *                                                      ----------->  request error
 *
 *
 */

//   // 刷新token,在过期前刷新
//   refresh(params) {
//     console.log("触发了refresh刷新Token");
//     return axios.request({
//       url: "/api/user/refreshToken",
//       params
//     });
//   }


// axios({
//     method: "get",
//     url: "/api/uc",
//     headers: {
//       Authorization: `Bearer ${tokenData.token}`
//     }
//   }).then(res => {
//     if (res.status != 200) {
//       Toast.fail("网络连接错误", 1);
//       return false;
//     }
//     localStorage.setItem(
//       "userInfo",
//       JSON.stringify(res.data.message.userInfo)
//     );
//   });

// getStorageToken() {
//     let Authorization = ''
//     let TokenKey = ''
//     // 从本地存储中获取token
//     let tokenData = User.getAccessToken()
//     // 解构赋值获得tokenData对应的值
//     let {
//       accessToken, //  有效的token
//       expireTime, //  有效token截至时间
//       key, //  TokenKey值
//       refreshToken, //  用于刷新的token
//       refreshTokenValidTime //  用于刷新的Token值的截至时间
//     } = tokenData
//     // 判断是否解构成功,即获取到token值
//     if (accessToken) {
//       let nowDate = new Date()
//       let access = nowDate - expireTime
//       let refresh = nowDate - refreshTokenValidTime
//       if (access < 0) {
//         // 没有过期
//         Authorization = 'auth ' + accessToken
//         TokenKey = key
//       } else if (access > 0 && refresh < 0) {
//         // 过期了,但是可以刷新
//         Authorization = 'auth ' + refreshToken
//         TokenKey = key
//         // 是否可以加锁
//         if (!this.refreshLock) {
//           // 锁住,防止多次请求刷新
//           this.refreshLock = true
//           this.refresh({
//             accessToken: refreshToken,
//             key: TokenKey
//           }).then(response => {
//             if (response.data.code === 1) {
//               User.setAccessToken(response.data.data)
//             } else {

//               User.clearLoginInfo()
//             }
//             // 释放锁
//             this.refreshLock = false
//           }).catch(err => {
//             console.log("token刷新失败", err)
//             User.clearLoginInfo()
//             this.refreshLock = false
//           })
//         }
//       } else {
//         // 刷新的也过期了, 提示重新登陆
//         User.clearLoginInfo()
//         return false
//       }
//     } else {
//       // 未获取到token
//       User.clearLoginInfo()
//       return false
//     }
//     return {
//       Authorization,
//       TokenKey
//     }
//   }
