import axios from "axios";
import { getAxios, PostAxios } from "./store/request";
export default wxJSDK = () => {
  const params = {
    data: {},
    api: "/api/user/address"
  };
  getAxios(params, res => {
    let info = res.data.message;
    window.wx.config({
      debug: info.debug,
      appId: info.appId,
      timestamp: info.timestamp,
      nonceStr: info.nonceStr,
      signature: info.signature,
      jsApiList: info.jsApiList
    });
    window.wx.ready(function() {});
  });
};
