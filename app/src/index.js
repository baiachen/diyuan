import React from 'react';
import ReactDOM from 'react-dom';
import BasicExample from './js/router';

ReactDOM.render(<BasicExample/>, document.getElementById('root'));

