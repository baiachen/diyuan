<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([], function (\Illuminate\Routing\Router $router){

    //index
    $router->get('index', 'IndexController@index');
    $router->get('category', 'CategoryController@index');

    //topic
    $router->get('topics/resources', 'TopicController@resources');
    $router->get('topics/detail', 'TopicController@show');
    $router->get('topics/collect', 'TopicController@collect');
    $router->get('topics/order', 'TopicController@order');

    //shop
    $router->get('books', 'BookController@index');
    $router->get('books/detail', 'BookController@show');
    $router->get('books/resources', 'BookController@resources');
    $router->get('books/collect', 'BookController@collect');
    $router->get('shop/search', 'ShopController@search');

    //address
    $router->get('user/address', 'AddressController@index');
    $router->post('user/address', 'AddressController@store');
    $router->get('user/address_edit', 'AddressController@edit');
    $router->post('user/address_update', 'AddressController@update');
    $router->post('user/address_destroy', 'AddressController@destroy');
    $router->post('user/address_change', 'AddressController@change');

    //order
    $router->any('order/notifies', 'OrderNotifyController@notify')->name('notify');
    $router->post('order/pay', 'OrderController@pay');

    //auth
    $router->any('login', 'LoginController@login');
    $router->post('login/refreshToken', 'LoginController@refreshToken');
    $router->post('login/send_msg', 'LoginController@send_msg');
    $router->any('login/callback', 'LoginController@login_callback')->name('login_callback');
    $router->post('auth/register', 'RegisterController@store');
    $router->post('bind_mobile', 'RegisterController@bindMobile');

    //user_center
    $router->get('uc', 'UserCenterController@index');
    $router->get('uc/orders', 'UserCenterController@orders');
    $router->get('uc/userInfo', 'UserCenterController@userInfo');
    $router->get('uc/material', 'UserCenterController@material');
    $router->get('uc/notes', 'UserCenterController@note');
});
