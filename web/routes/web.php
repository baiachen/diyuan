<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'RootController@root');
Route::get('/mall', 'RootController@root');
Route::get('/user', 'RootController@root');
Route::get('/legister', 'RootController@root');
Route::get('/login', 'RootController@root');
Route::get('/settlement/{id}/{sid}', 'RootController@root');
Route::get('/home/{type}', 'RootController@root');
Route::get('/homeAllUserList/{type}', 'RootController@root');
Route::get('/homeRelevant/{id}', 'RootController@root');
Route::get('/videoDetail/{id}', 'RootController@root');
Route::get('/audioDetail/{id}', 'RootController@root');
Route::get('/bookDatil/{id}', 'RootController@root');
Route::get('/mallTabItemDetail/{id}', 'RootController@root');
Route::get('/UserOrderList/{id}', 'RootController@root');
Route::get('/UserToPaid', 'RootController@root');
Route::get('/UserToDelivery', 'RootController@root');
Route::get('/UserToHarvested', 'RootController@root');
Route::get('/UserToCompleted', 'RootController@root');
Route::get('/wallet', 'RootController@root');
Route::get('/recharge', 'RootController@root');
Route::get('/putForward', 'RootController@root');
Route::get('/myTextbook', 'RootController@root');
Route::get('/myInfo', 'RootController@root');
Route::get('/platformIntroduction', 'RootController@root');
Route::get('/customerService', 'RootController@root');
Route::get('/address', 'RootController@root');
Route::get('/addAddress/{type}', 'RootController@root');


//Route::get('/', function () {
//    return view_put('welcome');
//});
//
//Route::get('/mall', function () {
//    return view_put('welcome');
//});
//
//Route::get('/user', function () {
//    return view_put('welcome');
//});
//
//Route::get('/legister', function () {
//    return view_put('welcome');
//});
//
//Route::get('/login', function () {
//    return view_put('welcome');
//});
//
//Route::get('/settlement/{id}/{sid}', function () {
//    return view_put('welcome');
//});
//
//Route::get('/home/{type}', function () {
//    return view_put('welcome');
//});
//
//Route::get('/homeAllUserList/{type}', function () {
//    return view_put('welcome');
//});
//
//Route::get('/homeRelevant/{id}', function () {
//    return view_put('welcome');
//});
//
//Route::get('/videoDetail/{id}', function () {
//    return view_put('welcome');
//});
//
//Route::get('/audioDetail/{id}', function () {
//    return view_put('welcome');
//});
//
//Route::get('/bookDatil/{id}', function () {
//    return view_put('welcome');
//});
//
//Route::get('/mallTabItemDetail/{id}', function () {
//    return view_put('welcome');
//});
//
//Route::get('/UserOrderList/{id}', function () {
//    return view_put('welcome');
//});
//
//Route::get('/UserToPaid', function () {
//    return view_put('welcome');
//});
//
//Route::get('/UserToDelivery', function () {
//    return view_put('welcome');
//});
//
//Route::get('/UserToHarvested', function () {
//    return view_put('welcome');
//});
//
//Route::get('/UserToCompleted', function () {
//    return view_put('welcome');
//});
//
//Route::get('/wallet', function () {
//    return view_put('welcome');
//});
//
//Route::get('/recharge', function () {
//    return view_put('welcome');
//});
//
//Route::get('/putForward', function () {
//    return view_put('welcome');
//});
//
//Route::get('/myTextbook', function () {
//    return view_put('welcome');
//});
//
//Route::get('/myInfo', function () {
//    return view_put('welcome');
//});
//
//Route::get('/platformIntroduction', function () {
//    return view_put('welcome');
//});
//
//Route::get('/customerService', function () {
//    return view_put('welcome');
//});
//
//Route::get('/address', function () {
//    return view_put('welcome');
//});
//
//Route::get('/addAddress/{type}', function () {
//    return view_put('welcome');
//});

























