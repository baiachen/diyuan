<?php

namespace Tests\Unit;

use App\Services\IndexService;
use App\Services\Login\WeChat;
use App\Services\LoginService;
use App\Services\WeChatService;
use App\Topic;
use EasyWeChat\Factory;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Vinkla\Hashids\Facades\Hashids;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {


        $expected = '454';



        $act = array_first(Hashids::decode('8qy14veoJwPO2nxpagmk37ZK1XRgjBLMY'));




        $this->assertEquals($expected, $act);
    }
}
