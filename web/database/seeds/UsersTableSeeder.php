<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //factory(App\User::class, 50)->create()
        factory(App\User::class, 50)->create();
        factory(\App\Category::class, 5)->create();
        factory(\App\Banner::class, 3)->create();
        factory(\App\Grade::class, 7)->create();
        factory(\App\Course::class, 5)->create();
        factory(\App\Topic::class, 55)->create();
        factory(\App\Resource::class, 55)->create();
        factory(\App\Book::class, 55)->create();
    }
}
