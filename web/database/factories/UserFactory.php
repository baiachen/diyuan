<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'pid' => rand(1,100),
        'unionid' => str_random(10),
        'openId' => str_random(10),
        'nickName' => $faker->name,
        'realName' => $faker->name,
        'avatarUrl' => str_random(10),
        'sex'=>rand(0,1),
        'mobile' => 13111111111,
        'password' => str_random(10),
        'money' => 0,
    ];
});

$factory->define(\App\Banner::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'imgUrl' => str_random(10),
        'order' => 0,
    ];
});


$factory->define(\App\Category::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'icon' => str_random(10),
        'order' => 0,
    ];
});

$factory->define(\App\Grade::class, function (Faker $faker) {
    return [
        'parent_id' => 1,
        'title' => str_random(10),
        'order' => 0,
    ];
});

$factory->define(\App\Course::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'order' => 0,
    ];
});

$factory->define(\App\Topic::class, function (Faker $faker) {
    return [
        'category_id' => $faker->randomElement(\App\Category::all()->pluck('id')->toArray()),
        'grade_id' => $faker->randomElement(\App\Grade::all()->pluck('id')->toArray()),
        'course_id' =>$faker->randomElement(\App\Course::all()->pluck('id')->toArray()),
        'title' => $faker->title,
        'description' => $faker->text,
        'teacher_name' => $faker->name,
        'thumbnail' => 'ss',
        'price' => 30,
        'read_counts' => 1,
        'resource_type' => 1,
    ];
});

$factory->define(\App\Resource::class, function (Faker $faker) {
    return [
        'topic_id' => $faker->randomElement(\App\Topic::all()->pluck('id')->toArray()),
        'title' => str_random(10),
        'resource_path' => 'sss',
        'is_free' => 1,
        'read_counts' => 33,
        'order' => 1,
    ];
});

$factory->define(\App\Book::class, function (Faker $faker) {
    return [
        'grade_id' => $faker->randomElement(\App\Grade::all()->pluck('id')->toArray()),
        'course_id' =>$faker->randomElement(\App\Course::all()->pluck('id')->toArray()),
        'title' => str_random(10),
        'thumbnail' => str_random(10),
        'content' => $faker->text,
        'description' => $faker->text,
        'sales' => rand(1,99),
        'likes' =>rand(1,99),
        'price' => rand(1,99),
        'status' => 0,
    ];
});
