<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('grade_id')->default(0)->index()->comment('年级id');
            $table->unsignedInteger('course_id')->default(0)->index()->comment('课目id');
            $table->string('title')->comment('书本标题');
            $table->string('thumbnail')->comment('书本缩略图');
            $table->text('content')->nullable()->comment('书本介绍');
            $table->string('description')->comment('描述');
            $table->unsignedInteger('sales')->default(0)->comment('销售数量');
            $table->double('likes')->default(0)->comment('好评');
            $table->decimal('price')->default(0.00)->comment('价格');
            $table->unsignedTinyInteger('status')->default(0)->comment('上线状态')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
