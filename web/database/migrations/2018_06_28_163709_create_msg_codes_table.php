<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMsgCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('msg_codes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mobile', 20)->default('');
            $table->integer('code' )->default(0);
            $table->string('description')->nullable()->comment('发送短信回执信息');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('msg_codes');
    }
}
