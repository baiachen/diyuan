<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->comment('用户id')->index();
            $table->string('openid')->comment('用户openid')->default('');
            $table->string('title')->comment('商品名称')->default('');
            $table->string('order_sn')->unique()->comment('订单编号');
            $table->string('consignee',20)->comment('收货人');
            $table->string('mobile', 20)->comment('联系号码');
            $table->string('address')->nullable()->comment('详细地址');
            $table->decimal('total_price',10,2)->default(0.00)->comment('订单总价格');
            $table->string('payment_method')->nullable()->comment('支付方式');
            $table->string('is_paid')->default(0)->comment('是否支付成功0未支付，1支付成功，2支付失败，3取消订单');
            $table->string('ship_status')->default(0)->comment('货运状态0未发货，1发货中，2已确认');
            $table->timestamp('paid_at')->nullable()->comment('支付时间');
            $table->timestamp('ship_at')->nullable()->comment('发货时间');
            $table->timestamp('success_at')->nullable()->comment('确认收获时间');
            $table->unsignedTinyInteger('is_comment')->default(0)->comment('是否评价');
            $table->integer('orderable_id')->index();
            $table->string('orderable_type');
            $table->integer('number')->default(0)->comment('件数');
            $table->string('remarks')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
