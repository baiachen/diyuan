<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->comment('用户id');
            $table->string('order_sn');
            $table->decimal('money',10,2)->comment('交易金额');
            $table->decimal('current_money',10,2)->comment('当前结余');
            $table->unsignedTinyInteger('change_type')->default(0)->comment('改变类型0支付1收入');
            $table->string('description')->comment('描述');
            $table->unsignedTinyInteger('type')->comment('资金类型0支付，1提现，2退回')->default(0);
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
