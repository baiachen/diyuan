<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_id');
            $table->unsignedInteger('user_id')->index();
            $table->string('title');
            $table->string('thumbnail')->nullable();
            $table->decimal('price', 10,2)->default(0.00);
            $table->tinyInteger('status')->default(0)->comment('状态：0待支付，1待发货，2待收货，3已完成');
            $table->boolean('isLook')->default(false);
            $table->integer('userorderable_id')->index();
            $table->string('userorderable_type');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_orders');
    }
}
