<?php

namespace App\Listeners;

use App\Account;
use App\Events\LogAccountEvent;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Exception\HttpException;

class LogAccountEventListener
{

    private $user;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
        //
    }

    /**
     * Handle the event.
     *
     * @param  LogAccountEvent  $event
     * @return void
     */
    public function handle(LogAccountEvent $event)
    {
        $account = $event->get();

        $account->current_money = $this->user::find($account->user_id)->money;

        $account->save();

    }








//`user_id` int(10) unsigned NOT NULL COMMENT '用户id',
//`money` decimal(10,2) NOT NULL COMMENT '交易金额',
//`current_money` decimal(10,2) NOT NULL COMMENT '当前结余',
//`change_type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '改变类型0支付1收入',
//`description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '描述',
//`type` tinyint(3) unsigned NOT NULL COMMENT '资金类型0支付，1提现，2退回',
}
