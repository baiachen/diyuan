<?php

namespace App\Admin\Extensions;

use Encore\Admin\Form\Field;

class WangEditor extends Field
{
    protected $view = 'admin.wang-editor';

    protected static $css = [
        '/vendor/laravel-admin/wangEditor-3.1.0/release/wangEditor.min.css',
    ];

    protected static $js = [
        '/vendor/laravel-admin/wangEditor-3.1.0/release/wangEditor.min.js',
    ];

    public function render()
    {
        $name = $this->formatName($this->column);

        $this->script = <<<EOT

var E = window.wangEditor
var editor = new E('#{$this->id}');
editor.customConfig.zIndex = 0
editor.customConfig.uploadImgShowBase64 = false
editor.customConfig.uploadImgServer = '/admin/upload_img'
editor.customConfig.uploadImgMaxSize = 3 * 1024 * 1024
editor.customConfig.uploadImgMaxLength = 10
editor.customConfig.uploadFileName = 'images'
editor.customConfig.uploadImgHeaders = {
    'Accept': 'text/x-json',
    'X-CSRF-TOKEN': '{$this->getToken()}'
}
editor.customConfig.onchange = function (html) {
    $('input[name=$name]').val(html);
}
editor.create()

EOT;
        return parent::render();
    }

    protected function getToken()
    {
        return csrf_token();
    }
}