<?php

namespace App\Admin\Extensions\Nav;

use Encore\Admin\Facades\Admin;

class Links
{





    public function __toString()
    {


        Admin::script(
            '$(function(){
                $(".clean-cache").on("click", function(){
                    swal({ 
                      title: "确定清除缓存？", 
                      text: "", 
                      type: "info", 
                      showCancelButton: true, 
                      closeOnConfirm: false, 
                      showLoaderOnConfirm: true, 
                    },
                    function(){ 
                        $.ajax({
                            type:"post",
                            url:"/admin/act/clean_cache",
                            data:{_token:LA.token},
                            success:function(res){
                                if(res.status){
                                    swal("清除完成！"); 
                                }
                            },
                            error:function(res){
                            }
                        })
                    
                    
                    
//                      setTimeout(function(){ 
//                        swal("Ajax请求完成！"); 
//                      }, 2000);
                    });
                })
            })'



        );


        return <<<HTML

<li>
    <a href="javascript:;" class="clean-cache">
      <span>清除缓存</span>
    </a>
</li>

HTML;
    }
}