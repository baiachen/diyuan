<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index');

    $router->post('/act/clean_cache', 'HomeController@clean_cache')->name('clean_cache');


    $router->resource('topic', 'TopicController');
    $router->resource('grade', 'GradeController');
    $router->resource('category', 'CategoryController');
    $router->resource('config', 'ConfigController');
    $router->resource('course', 'CourseController');
    $router->resource('tag', 'TagController');
    $router->resource('banner', 'BannerController');

    $router->post('upload_img', 'BookController@uploadImg');
    $router->resource('book', 'BookController');
    $router->resource('notes', 'NoteController');
    $router->resource('orders', 'OrderController');
    $router->resource('users', 'UserController');
});
