<?php

namespace App\Admin\Controllers;

use App\Order;
use App\Services\OrderService;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class OrderController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Order::class, function (Grid $grid) {
            $grid->model()->orderBy('created_at', 'desc');

            $grid->id('ID')->sortable();
            $grid->user_id('用户id');
            $grid->user()->nickName('微信昵称');
            $grid->title('标题');
            $grid->order_sn('订单号');
            $grid->consignee('收件人');
            $grid->mobile('联系电话');
            $grid->address('邮寄地址');
            $grid->total_price('总价');
            $grid->payment_method('支付方式')->display(function ($value){
                return Order::PAYMENT_METHOD[$value] ?? '未知';
            });
            $grid->is_paid('支付状态')->display(function ($value){
                return Order::ORDER_STATUS[$value] ?? '未知';
            });
            $grid->ship_status('货运状态')->display(function ($value){
                return Order::SHIP_STATUS[$value] ?? '未知';
            });
            //            $grid->orderable();
            $grid->number('订单数量');
            $grid->remarks('备注');
            $grid->orderable_type('订单来源')->display(function ($value){
                return OrderService::TYPE[$value] ?? '未知';
            });
            $grid->paid_at('支付时间');
            $grid->ship_at('发货时间');
            $grid->success_at('收货时间');



            $grid->created_at('创建时间');

        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Order::class, function (Form $form) {

            $form->display('id', 'ID');

            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }
}
