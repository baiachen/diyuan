<?php

namespace App\Admin\Controllers;

use App\Book;
use App\Course;
use App\Grade;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BookController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Book::class, function (Grid $grid) {

            $grid->model()->orderBy('id','desc');
            $grid->id('ID')->sortable();
            $grid->grade()->title();
            $grid->course()->name();
            $grid->title();
            $grid->thumbnail()->image('', 60);
            $grid->images()->display(function ($values){
               $values =  array_map(function ($value){
                   $url = config('admin.upload.host'). DIRECTORY_SEPARATOR. $value['imgUrl'];
                   return "<img style='margin: 2px' src='{$url}' width='30' height='30'/>";
               }, $values);

               return join('', $values);
            });
            $grid->description();
            $grid->sales();
            $grid->likes();
            $grid->price();
            $grid->status();
            $grid->created_at('创建时间')->display(function ($value){
                return  date('Y-m-d', strtotime($value));
            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Book::class, function (Form $form) {

            $form->display('id', 'ID');
            $form->select('grade_id')->options(Grade::all()->pluck('title', 'id'))->rules('required');
            $form->select('course_id', '课目')->options(Course::all()->pluck('name', 'id'))->rules('required');
            $form->text('title')->rules('required');
            $form->image('thumbnail')->uniqueName()->removable()->rules('required');

            $form->textarea('description')->rules('required');
            $form->currency('price')->rules('required');
            $form->switch('status')->rules('required');
            $form->editor('content')->rules('required');
            $form->hasMany('images', 'images',function (Form\NestedForm $form){
                $form->image('imgUrl')->removable()->rules('required');
            });
        });
    }

    public function uploadImg(Request $request)
    {
        if ($request->hasFile('images')) {

            //
            $file = $request->file('images');


            $rules = [
                'images'    => 'max:5120',
            ];
            $messages = [
                'images.max'    => '文件过大,文件大小不得超出5MB',
            ];
            $validator = Validator::make($request->all(), $rules, $messages);

            $res = ['errno' => 1, 'message' => 'error|失败原因为：非法传参'];

            if ($validator->passes()) {
                $realPath = $file->getRealPath();
                $destPath = 'uploads/content/';
                $savePath = $destPath.''.date('Ymd', time());
                is_dir($savePath) || @mkdir($savePath);  //如果不存在则创建目录
                $name = $file->getClientOriginalName();
                $ext = $file->getClientOriginalExtension();
                $check_ext = in_array($ext, ['gif', 'jpg', 'jpeg', 'png'], true);
                if ($check_ext) {
                    $uniqid = uniqid().'_'.date('s');
                    $oFile = $uniqid.'o.'.$ext;
                    $fullfilename = config('app.url') .'/'.$savePath.'/'.$oFile;  //原始完整路径
                    if ($file->isValid()) {
                        $uploadSuccess = $file->move($savePath, $oFile);  //移动文件
                        $oFilePath = $savePath.'/'.$oFile;
                        $res = ['errno' => 0, 'data' => [$fullfilename]];
                    } else {
                        $res = ['errno' => 1, 'message' => 'error|失败原因为：非法传参'];
                    }
                } else {
                    $res = ['errno' => 1, 'message' => 'error|失败原因为：文件类型不允许,请上传常规的图片(gif、jpg、jpeg与png)文件'];
                }
            } else {
                $res = ['errno' => 1, 'message' => 'error|失败原因为：文件类型不允许,请上传常规的图片(gif、jpg、jpeg与png)文件'];
            }

            return $res;
        }


    }
}
