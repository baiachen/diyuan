<?php

namespace App\Admin\Controllers;

use App\Category;
use App\Course;
use App\Grade;
use App\Tag;
use App\Topic;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Widgets\Table;

class TopicController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Topic::class, function (Grid $grid) {


            $grid->model()->orderBy('id', 'desc');
            $grid->model()->with('resources');
            $grid->id('ID')->sortable();
            $grid->grade()->title('年级段');
            $grid->category()->name('分类');
            $grid->course()->name('课目');
            $grid->title('标题');
            $grid->thumbnail('缩略图')->image('', 30);
            $grid->price('金额');
            $grid->read_counts('购买数');
//            $grid->resource_type();

            $grid->column('资源列表')->expand(function () {
                return new Table(['id','文章id','标题','资源地址','是否免费','阅读数','排序','创建时间','更新时间'], $this->resources);
            }, 'resources');

            $states = [
                'on' => ['text' => '下架'],
                'off' => ['text' => '正常'],
            ];
            $grid->status('状态')->switch($states);
            $grid->tags('标签')->pluck('name')->label();
            $grid->created_at('创建时间')->display(function ($value){
               return  date('Y-m-d', strtotime($value));
            });
//            $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Topic::class, function (Form $form) {

            $form->display('id', 'ID');
            $form->select('category_id', '分类')->options(Category::all()->pluck('name', 'id'))->rules('required');
            $form->select('grade_id', '年级段')->options(Grade::selectOptions())->rules('required');
            $form->select('course_id', '课目')->options(Course::all()->pluck('name', 'id'))->rules('required');
            $form->text('title', '标题')->rules('required');
            $form->image('thumbnail', '缩略图')->uniqueName()->removable()->rules('required');
            $form->currency('price', '价格')->rules('required');
            $form->text('teacher_name', '教师名称')->rules('required');
            $form->textarea('description', '描述')->rules('required');
            $form->multipleSelect('tags', '标签')->options(Tag::all()->pluck('name', 'id'))->rules('required');
            $states = [
                'on' => ['text' => '下架'],
                'off' => ['text' => '正常'],
            ];

            $form->switch('status', '状态')->states($states);

            $form->hasMany('resources', '资源列表', function (Form\NestedForm $form) {
                $states = [
                    'on' => ['text' => '收费'],
                    'off' => ['text' => '免费'],
                ];
                $form->text('title')->rules('required');
                $form->text('resource_path')->rules('required');
                $form->switch('is_free')->states($states);
                $form->number('order')->default(0);
            });




        });
    }
}
