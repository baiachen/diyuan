<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\Dashboard;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;

class HomeController extends Controller
{
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Dashboard');
            $content->description('Description...');

            $content->row(Dashboard::title());

            $content->row(function (Row $row) {

                $row->column(4, function (Column $column) {
                    $column->append(Dashboard::environment());
                });

                $row->column(4, function (Column $column) {
                    $column->append(Dashboard::extensions());
                });

                $row->column(4, function (Column $column) {
                    $column->append(Dashboard::dependencies());
                });
            });
        });
    }

    public function clean_cache()
    {
        cache()->tags(['books','books_detail', 'topic', ])->flush();

        $this->clean(function ($cache){
            cache()->forget($cache);
        });

        return json_put(['status'=>true,'message'=>'success']);
    }


    protected function clean(callable $callable)
    {
        foreach ($this->caches() as $cache){
            $callable($cache);
        }
    }

    protected function caches() : array
    {
        return ['banner', 'category', 'grade', 'SETTINGS', 'course', 'grade_child'];
    }
}
