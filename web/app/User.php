<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function mine()
    {
        return $this->hasOne(User::class, 'id', 'pid');
    }


    public function addresses()
    {
        return $this->hasMany(Address::class);
    }

    public function topics()
    {
        return $this->belongsToMany(Topic::class, 'topic_user');
    }

    public function userOrders()
    {
        return $this->hasMany(UserOrder::class);
    }
}
