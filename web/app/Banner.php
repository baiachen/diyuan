<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    //


    public function getImgUrlAttribute($imgUrl)
    {
        if($imgUrl){
            return config('admin.upload.host') .DIRECTORY_SEPARATOR.$imgUrl;
        }

    }
}
