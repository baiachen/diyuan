<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Collect extends Model
{


    protected $guarded = [];
    //

    public function collectable()
    {
        return $this->morphTo();
    }
}
