<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //

    const UN_PAY = 0;
    const PAY_SUCCESS = 1;
    const PAY_FAIL = 2;
    const PAY_CANCEL = 3;

    const SHIP_UN = 0;
    const SHIP_ING = 1;
    const SHIP_SUCCESS = 2;

    const WECAHT_PAY = 'wechat';
    const CASH_PAY = 'cash';

    const  PAYMENT_METHOD = [
        self::WECAHT_PAY => '微信支付',
        self::CASH_PAY => '余额支付'
    ];

    const ORDER_STATUS = [
        self::UN_PAY => '未支付',
        self::PAY_SUCCESS => '支付成功',
        self::PAY_FAIL => '支付失败',
        self::PAY_CANCEL => '取消订单'
    ];

    const SHIP_STATUS = [
        self::SHIP_UN => '未发货',
        self::SHIP_ING => '发货中',
        self::SHIP_SUCCESS => '已确认'
    ];



    protected $guarded = [];

    public function orderable()
    {
        return $this->morphTo();
    }



    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
