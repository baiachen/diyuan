<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Book extends Model
{
    //

    const STATUS_ON = 0;

    const STATUS_OFF = 1;

    const STATUS = [
        self::STATUS_ON => '正常',
        self::STATUS_OFF => '下架'
    ];


    /**
     * 书籍下面的所有评论
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    /**
     * 文章下面的用户收藏
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function collect()
    {
        return $this->morphMany(Collect::class, 'collectable');
    }



    /**
     * 书记下面对应的订单
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function orders()
    {
        return $this->morphMany(Order::class, 'orderable');
    }

    public function userOrders()
    {
        return $this->morphMany(UserOrder::class, 'userorderable');
    }


    public function grade()
    {
        return $this->belongsTo(Grade::class);
    }

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function images()
    {
        return $this->hasMany(BookImage::class);
    }


}
