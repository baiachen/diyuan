<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserOrder extends Model
{


    const ORDER_STATUS_WAIT_PAID = 0;
    const ORDER_STATUS_WAIT_SHIP = 1;
    const ORDER_STATUS_WAIT_RECEIPT = 2;
    const ORDER_STATUS_SUCCESS = 3;


    const ORDER_STATUS = [
        self::ORDER_STATUS_WAIT_PAID => '待支付',
        self::ORDER_STATUS_WAIT_SHIP => '待发货',
        self::ORDER_STATUS_WAIT_RECEIPT => '待收货',
        self::ORDER_STATUS_SUCCESS => '已完成',
     ];
    //

    protected $guarded = [];






    public function userorderable()
    {
        return $this->morphTo();
    }
}
