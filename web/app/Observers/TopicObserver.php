<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/29 0029
 * Time: 上午 10:20
 */

namespace App\Observers;


use App\Topic;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class TopicObserver
{

    public function saving(Topic $topic)
    {
        $topic->resource_type = $topic->category_id;
    }

    public function saved()
    {

        $this->flush();
    }

    public function updating()
    {

    }

    public function updated()
    {

        $this->flush();
    }

    private function flush()
    {
        Cache::tags('topic')->flush();
    }

}