<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/29 0029
 * Time: 上午 10:20
 */

namespace App\Observers;


use App\Topic;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class ConfigObserver
{

    public function saved()
    {
        $this->flush();
    }

    public function deleted()
    {
        $this->flush();
    }

    private function flush()
    {
        cache()->forget('SETTINGS');
    }

}