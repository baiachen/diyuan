<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/7/9 0009
 * Time: 下午 5:10
 */

namespace App\Services;



use App\Services\Order\BookPayment;
use App\Services\Order\PaymentInterface;
use App\Services\Order\TopicPayment;


use Illuminate\Support\Facades\App;


class OrderService
{



    /**
     * $type
     */
    const TOPIC_PAY = 1;   //文章购买
    const BOOK_PAY = 2;    //商城购买

    const TYPE = [
        'App\\Topic' => '内容购买',
        'App\\Book' => '商城购买'
    ];


    private $lut = [
        self::TOPIC_PAY => TopicPayment::class,
        self::BOOK_PAY => BookPayment::class,

    ];




    public function pay(array $params)
    {
        $payment = $this->bind($params['type']);

        if(request()->has('order_id') && request()->get('order_id') > 0){
            return $payment->hasOrderPay($params);
        }


        return $payment->pay($params);

    }

    public function bind(int $type)
    {

        $className = collect($this->lut)->get($type, TopicPayment::class);

        App::bind(PaymentInterface::class, $className);

        return  app(PaymentInterface::class);

    }








}