<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/7/9 0009
 * Time: 下午 5:38
 */

namespace App\Services\Order;


use App\Order;
use App\Topic;

class TopicPayment extends Payment
{

    public function pay(array $params)
    {

        $Topic = Topic::findOrFail($params['id']);

        return $this->config($Topic);

    }

    public function hasOrderPay(array $params)
    {
        $Order = Order::findOrFail($params['order_id']);

        return $this->wePay($Order);

    }

}