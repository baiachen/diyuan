<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/7/9 0009
 * Time: 下午 5:39
 */

namespace App\Services\Order;


interface PaymentInterface
{


    /**
     * 新订单支付
     * @param array $params
     * @return mixed
     */
    public function pay(array $params);


    /**
     * 已存在的订单支付
     * @return mixed
     */
    public function hasOrderPay(array $params);






}