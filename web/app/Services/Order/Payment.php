<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/7/9 0009
 * Time: 下午 5:41
 */

namespace App\Services\Order;


use App\Order;
use App\Services\Traits\LogTrait;
use App\Services\WeChatService;
use App\UserOrder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\HttpException;

abstract class Payment implements PaymentInterface
{

    use LogTrait;

    protected function config($payment)
    {

        return DB::transaction(function () use($payment){

            $user = Auth::user();

            if(! $user){
                throw new HttpException(410, '用户不存在');
            }

            $defaultAddress = $user->addresses()->where('is_default', 1)->first();

            if(! $defaultAddress){
                throw new HttpException(410, '默认地址不存在');
            }

            $order = new Order([
                'user_id' => $user->id,
                'openid' => $user->openId,
                'title' => $payment->title,
                'order_sn' => $this->makeOrderSn(),
                'consignee' => $defaultAddress->consignee,
                'mobile' => $defaultAddress->mobile,
                'address' => $defaultAddress->province.$defaultAddress->city.$defaultAddress->area.$defaultAddress->address,
                'total_price' => $payment->price,
                'payment_method' => 'wechat',
                'is_paid' => 0,
                'ship_status' => 0,
                'number' =>1,
                'remarks' => request()->remarks,
            ]);

            $Order = $payment->orders()->save($order);

            if(! $Order){
                throw new HttpException(410, '订单创建失败');
            }

            $userOrder = new UserOrder([
                'order_id' => $Order->id,
                'user_id' => $user->id,
                'title' => $payment->title,
                'thumbnail' => $payment->thumbnail,
                'price' => $payment->price,
                'status' => 0,
                'isLook' => 0,
            ]);

            //记录用户订单
            $payment->userOrders()->save($userOrder);


            $this->write(['user_id' => $user->id, 'user_name' => $user->nickName, 'description' =>
                "生成购买订单:{$Order->title},订单号:{$Order->order_sn},价格:{$Order->total_price},支付方式:{$Order->payment_method}"]);


            return $this->wePay($Order);
        });

    }

    protected function makeOrderSn()
    {
        return date('Ymd', time()) . strtoupper(dechex(date('m'))) . date('d') . substr(time(), -5) . substr(microtime(), 2, 5) . sprintf('%02d', rand(0, 99));
    }

    protected function wePay(Order $order)
    {
        return app(WeChatService::class)->make('payment')->payment($order);
    }

}