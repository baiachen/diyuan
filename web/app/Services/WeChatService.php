<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/7/2 0002
 * Time: 上午 8:56
 */

namespace App\Services;


use App\Order;
use App\Repositories\UserRepository;
use EasyWeChat\Factory;
use EasyWeChat\Kernel\BaseClient;
use Illuminate\Support\Facades\Log;
use Vinkla\Hashids\Facades\Hashids;

class WeChatService
{
    private $app;
    private $factory;
    private $config;

    public function __construct( Factory $factory )
    {

        $this->config =  [
            'app_id' => setting('APP_ID'),
            'secret' => setting('APP_SECRET'),
            'mch_id' => setting('MCH_ID'),
            'key' => setting('MCH_KEY'),
            'cert_path' => base_path('cert/apiclient_cert.pem'),
            'key_path' => base_path('cert/apiclient_key.pem'),
        ];

        $this->factory = $factory;



    }

    /**
     * 微信登录
     * @param string $from
     * @return array
     */
    public function login(string $from)
    {

        $uri = urlencode(route('login_callback',['from' => $from])) ;

        $redirect_uri = "https://open.weixin.qq.com/connect/oauth2/authorize?appid={$this->config['app_id']}&redirect_uri={$uri}&response_type=code&scope=snsapi_userinfo&state=STATE&connect_redirect=1#wechat_redirect";
//        $redirect_uri = "https://open.weixin.qq.com/connect/oauth2/authorize?appid={$this->config['app_id']}&redirect_uri={$uri}&response_type=code&scope=snsapi_base&state=STATE&connect_redirect=1#wechat_redirect";


        return json_put([
            'status' => true,
            'redirect_uri' =>$redirect_uri
        ]);
    }

    /**
     * 微信登录回调
     * @param string $from
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function loginCallBack(string $from = '/')
    {



        $original_user = $this->make()->app->oauth->user();

        $userInfo = $original_user->getOriginal();

        if(! $userInfo){
            json_put([
                'status' => false,
                'message' => '获取用户信息失败,稍后再试！'
            ]);
        }

        //save userInfo

        $user = app(UserRepository::class)->save($userInfo);

        $token = app(LoginService::class)->fromUser($user);

        $queries =  '?'.http_build_query([
                'status' => true,
                'redirect_uri' => $from,
                'token' => $token,
            ]);

        return redirect(url($from). $queries)
            ->cookie('is_login', '1', time()+3600, "/", '',false, false)
            ->cookie('token', $token, time()+3600, "/", '', false,false)
            ->cookie('u_id', Hashids::encode($user->id), time()+3600, "/", '', false,false)
            ->cookie('expires_in', time() + config('jwt.ttl') * 50, time()+3600, "/", '', false,false);
    }


    /**
     * 公众号，微信支付 sdk
     * @param string $name
     * @return $this
     */
    public function make(string $name = 'officialAccount')
    {
        switch ($name){
            case 'payment':
                $this->app = $this->factory::payment($this->config);
                return $this;
            case 'officialAccount':
                $this->app = $this->factory::officialAccount($this->config);
                return $this;
            default:
                $this->app = $this->factory::officialAccount($this->config);
                return $this;
        }
    }

    public function jsk()
    {
        $this->app->jssdk->setUrl(config('app.url') . DIRECTORY_SEPARATOR);
        return $this->app->jssdk->buildConfig(['onMenuShareTimeline', 'onMenuShareAppMessage','chooseWXPay'], $debug = false, $beta = false, $json = false);
    }

    public function payment(Order $order)
    {
        $result =  $this->app->order->unify([
            'body' => str_limit($order->title, 20),
            'out_trade_no' => $order->order_sn,
//            'total_fee' => $order->price * 100,
            'total_fee' => setting('DEBUG') ? 1 : $order->price * 100,
            'notify_url' => route('notify'),
            'trade_type' => 'JSAPI',
            'openid' => $order->openid,
        ]);

        return $this->app->jssdk->sdkConfig($result['prepay_id']); // 返回数组
    }

    public function getApp()
    {
        return $this->app;
    }



}