<?php
/**
 * Created by PhpStorm.
 * User: baia
 * Date: 2018/7/23
 * Time: 上午11:24
 */

namespace App\Services\Traits;

use App\Log;


trait LogTrait
{

    protected $log;

    public function __construct(Log $log)
    {
        $this->log = $log;
    }



    protected function write(array $param)
    {
        $this->log->create([
            'user_id' => $param['user_id'],
            'user_name' => $param['user_name'],
            'description' => $param['description']
        ]);

    }

}