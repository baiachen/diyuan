<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/7/2 0002
 * Time: 上午 9:30
 */

namespace App\Services\Login;


use App\Services\WeChatService;

class WeChat implements LoginInterface
{

    /**
     * 微信登录
     * @param array $params
     * @return mixed
     */
    public function login(array $params)
    {

        $weChat = app(WeChatService::class);

        $from = $params['from'] ?? '/';

        return $weChat->make()->login($from);

    }
}