<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/7/2 0002
 * Time: 上午 9:31
 */

namespace App\Services\Login;


use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Vinkla\Hashids\Facades\Hashids;

class Password implements LoginInterface
{

    /**
     * 手机号登录
     * @param array $params
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function login(array $params)
    {


        if(! $this->reg($params['mobile'])){
            throw new HttpException(410,'手机号码有误');
        }

        if(JWTAuth::attempt(['mobile'=>$params['mobile'], 'password' => $params['password']])){

            return json_put(['status' => true, 'message' =>
                ['is_login' => 1, 'token' => JWTAuth::fromUser(Auth::user()), 'u_id'=> Hashids::encode(Auth::id()),
                 'from' => url($params['from']),   ]
//                'userInfo' => array_only($user->toArray(),['id', 'nickName', 'avatarUrl', 'openId'])
            ]);
        }


        throw new HttpException(401,'手机号码或密码有误');


    }












    private function reg(string $mobile)
    {
        return preg_match("/^1[345678]{1}\d{9}$/", $mobile);
    }






}