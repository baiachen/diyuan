<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/7/2 0002
 * Time: 上午 9:30
 */

namespace App\Services\Login;


interface LoginInterface
{
    /**
     * 登录
     * @param array $params
     * @return mixed
     */
    public function login(array $params);

}