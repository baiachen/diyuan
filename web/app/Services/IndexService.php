<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/28 0028
 * Time: 下午 5:26
 */

namespace App\Services;


use App\Repositories\BannerRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\CourseRepository;
use App\Repositories\GradeRepository;
use App\Repositories\TopicRepository;

class IndexService
{


    /**
     * 首页内容
     * @param array $param
     * @return array
     */
    public function get(array $param)
    {

        $Banner = app(BannerRepository::class)->get();
        $Grade = app(GradeRepository::class)->get();
        $Category = app(CategoryRepository::class)->get();
        $Course = app(CourseRepository::class)->get();
        $Topic = app(TopicRepository::class)->get($param);
        $Config = app(WeChatService::class)->make()->jsk();

        return compact('Banner', 'Grade', 'Category','Topic', 'Course', 'Config');
    }









}