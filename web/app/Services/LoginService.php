<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/7/2 0002
 * Time: 上午 9:23
 */

namespace App\Services;


use App\Services\Login\LoginInterface;
use App\Services\Login\Password;
use App\Services\Login\WeChat;
use App\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Vinkla\Hashids\Facades\Hashids;

class LoginService
{

    private $app;

    private $loginMode = [
        'wechat' => WeChat::class, 'password' => Password::class
    ];

    public function getLoginMode(string $mode)
    {
        if(! array_has($this->loginMode, $mode)){
            throw new HttpException(410, '登录方式有误');
        }

        return $mode;
    }


    public function make(string $mode = 'wechat')
    {
        $mode = $this->getLoginMode($mode);

        $this->bind($mode);

        return $this->app;

    }

    public function bind(string $mode)
    {
        $className = collect($this->loginMode)->get($mode, WeChat::class);

        App::bind(LoginInterface::class, $className);

        $this->app = app(LoginInterface::class);
    }

    public function fromUser($user)
    {
        return JWTAuth::fromUser($user);
    }

    public function refreshToken(string $u_id)
    {

        $user = User::findOrFail(array_first(Hashids::decode($u_id)));

        return $this->fromUser($user);
    }

    public function keyName(string $mobile) : string
    {
        return 'm_'.$mobile;
    }

}