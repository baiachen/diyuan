<?php

namespace App\Events;

use App\Account;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class LogAccountEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;


    protected $account;



    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Account $account)
    {
        $this->account = $account;
        //
    }

    public function get()
    {
        return $this->account;
    }

//event(new LogAccountEvent(new Account(['user_id'=>1,'money'=>11, 'change_type'=>0,'description'=>'fdfds', 'type' => 1,])));
}
