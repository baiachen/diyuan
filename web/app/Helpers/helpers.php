<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/29 0029
 * Time: 下午 5:43
 */


if(!function_exists('json_put')){
    function json_put (array $param, $status = 200) {
        return response()->json($param, $status,  ['Server'=>'bn/1.0']);
    }
}

if(!function_exists('view_put')){
    function view_put (string $name, array $data = []) {
        return response()->view($name, $data)->header('Server', 'bn/1.0');
    }
}





/**
 * 获取配置信息
 * @param string $key
 * @param null $default
 * @return null
 */
if(!function_exists('setting')){
    function setting(string $key, $default = null)
    {
        $SETTINGS = cache()->remember('SETTINGS', 60, function (){
            return \App\Config::query()->pluck('value', 'name');
        });
        $data = json_decode($SETTINGS,true);
        return $data[$key] ?? $default;
    }
}

if(!function_exists('is_mobile')){
    // 判断是否为手机端
    function is_mobile()
    {
        $agent = strtolower($_SERVER['HTTP_USER_AGENT']);
        $is_pc = (strpos($agent, 'windows nt')) ? true : false;
        $is_mac = (strpos($agent, 'mac os')) ? true : false;
        $is_iphone = (strpos($agent, 'iphone')) ? true : false;
        $is_android = (strpos($agent, 'android')) ? true : false;
        $is_ipad = (strpos($agent, 'ipad')) ? true : false;
        if($is_iphone){
            return  true;
        }
        if($is_android){
            return  true;
        }
        if($is_ipad){
            return  true;
        }
        if($is_pc){
            return  false;
        }
        if($is_mac){
            return  false;
        }
    }
}

