<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {

//        if($exception instanceof HttpException){
//            return json_put(['message' => $exception->getMessage() ?: 'Request Not Found',
//                'request_id' => session_create_id(),
//                'status' => false,
//            ], $exception->getStatusCode());
//        }else


        if ($exception instanceof ValidationException) {
            return json_put(['message' => $exception->getMessage(),
                'request_id' => session_create_id(),
                'status' => false,
            ], 422);
        }else{
            return parent::render($request, $exception);
        }
    }
}
