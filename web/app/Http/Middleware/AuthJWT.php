<?php

namespace App\Http\Middleware;


use Closure;

use Tymon\JWTAuth\Facades\JWTAuth;


class AuthJWT
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['message'=>'用户不存在', 'code'=>403], 403);
            }
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['message'=>'token已过期', 'code'=>$e->getStatusCode()],$e->getStatusCode());
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['message'=>'无效的token', 'code'=>$e->getStatusCode()],$e->getStatusCode());
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['message'=>'异常错误', 'code'=>$e->getStatusCode()],$e->getStatusCode());
        }



        return $next($request);
    }
}
