<?php

namespace App\Http\Controllers;

use App\Services\LoginService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Redis\RedisManager;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;
use Vinkla\Hashids\Facades\Hashids;

class RootController extends Controller
{
    public function root()
    {
        return view_put('welcome');

    }


}
