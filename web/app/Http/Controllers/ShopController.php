<?php

namespace App\Http\Controllers;

use App\Repositories\BookRepository;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    //


    public function search(Request $request)
    {

        if($request->kw == ''){
            return false;
        }

        $result = app(BookRepository::class)->search($request->kw);


        return json_put(['status' => true,'message' => $result]);
    }
}
