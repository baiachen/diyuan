<?php

namespace App\Http\Controllers;

use App\Account;
use App\Address;
use App\Events\LogAccountEvent;
use App\Policies\AddressPolicy;
use App\Services\IndexService;
use App\Services\WeChatService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AddressController extends Controller
{
    //
    private $address;

    public function __construct(Address $address)
    {
        $this->middleware('authJWT');
        $this->address = $address;
    }

    public function index()
    {
        $result = $this->address->newQuery()->where('user_id', Auth::id())->get();

        return json_put(['status' => true, 'message' => $result]);
    }

    public function store(Request $request)
    {
        $param = $this->validate($request, [
            'is_default' => 'required',
            'username' => 'required',
            'mobile' => 'required',
            'address' => 'required',
            'addressDetail' => 'required'
        ]);

        //如果提交的数据为默认地址，先把已经存在的数据默认状态全都改为0；
        if($param['is_default']){
            $this->address->newQuery()->where('user_id', Auth::id())->update(['is_default' => 0]);
        }

        $result = $this->address->create([
            'user_id' => Auth::id(),
            'consignee' => $param['username'],
            'mobile' => $param['mobile'],
            'province' => $param['address'][0] ?? '',
            'city' => $param['address'][1] ?? '',
            'area' => $param['address'][2] ?? '',
            'address' => $param['addressDetail'],
            'is_default' => $param['is_default']
        ]);

        return json_put(['status' => true, 'message' => $result]);
    }

    public function edit(Request $request)
    {
        $param = $this->validate($request,[
            'id' => [
                'required',
                'regex:/^[0-9]+$/'
            ],
        ]);

        $address = $this->address->findOrFail($param['id']);

        $this->authorize('show', $address);



        return json_put(['status' => true, 'message' => $address]);

    }

    public function update(Request $request)
    {

        $param = $this->validate($request, [
            'id' => [
                'required',
                'regex:/^[0-9]+$/'
            ],
            'is_default' => 'required',
            'username' => 'required',
            'mobile' => 'required',
            'address' => 'required',
            'addressDetail' => 'required'
        ]);

        $address = $this->address->findOrFail($param['id']);
        $this->authorize('update', $address);

        //如果提交的数据为默认地址，先把已经存在的数据默认状态全都改为0；
        if($param['is_default']){
            $this->address->newQuery()->where('user_id', Auth::id())->update(['is_default' => 0]);
        }

        $result = $address->update([
            'consignee' => $param['username'],
            'mobile' => $param['mobile'],
            'province' => $param['address'][0] ?? '',
            'city' => $param['address'][1] ?? '',
            'area' => $param['address'][2] ?? '',
            'address' => $param['addressDetail'],
            'is_default' => $param['is_default']
        ]);




        return json_put(['status' => true, 'message' => $result]);

    }

    public function destroy(Request $request)
    {
        $param = $this->validate($request,[
            'id' => [
                'required',
                'regex:/^[0-9]+$/'
            ],
        ]);

        $address = $this->address->findOrFail($param['id']);

        $this->authorize('destroy', $address);
        $address->delete();

        if($this->address->newQuery()->where('user_id', Auth::id())->count() == 1){
            $this->address->newQuery()->where('user_id', Auth::id())->update(['is_default' => 1]);
        }

        return json_put(['status' => true, 'message' => '删除成功']);
    }


    public function change(Request $request)
    {
        $param = $this->validate($request, [
            'id' => [
                'required',
                'regex:/^[0-9]+$/'
            ],
        ]);

        $address = $this->address->findOrFail($param['id']);
        $this->authorize('update', $address);
        if($address->is_default){
            return;
        }


        //如果提交的数据为默认地址，先把已经存在的数据默认状态全都改为0；
        $this->address->newQuery()->where('user_id', Auth::id())->update(['is_default' => 0]);

        $result = $address->update([
            'is_default' => 1,
        ]);




        return json_put(['status' => true, 'message' => $result]);

    }

}
