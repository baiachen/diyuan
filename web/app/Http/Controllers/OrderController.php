<?php

namespace App\Http\Controllers;

use App\Account;
use App\Events\LogAccountEvent;
use App\Services\IndexService;
use App\Services\OrderService;
use App\Services\WeChatService;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class OrderController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('authJWT');
    }

    public function pay(Request $request)
    {
        $params = $this->validate($request,[
            'id' => [
                'required',
                'regex:/^[0-9]+$/'
            ],
            'order_id' => [
                'required',
                'regex:/^[0-9]+$/'
            ],
            'type'=>['required','regex:/^[1|2]$/'],
            'remarks' => 'max:255'

        ]);





        $result = app(OrderService::class)->pay($params);

        return json_put(['status' => true, 'message' =>$result]);
    }

}
