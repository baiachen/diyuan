<?php

namespace App\Http\Controllers;


use App\Account;
use App\Order;
use App\Services\Traits\LogTrait;
use App\Services\WeChatService;
use App\User;
use App\UserOrder;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


/**
 * 支付成功通知
 * Class OrderNotifyController
 * @package App\Http\Controllers
 */
class OrderNotifyController extends Controller
{

    use LogTrait;
    //

    public function notify(Request $request)
    {

        $app = app(WeChatService::class)->make('payment')->getApp();



        $response = $app->handlePaidNotify(function($message, $fail){

//            $message = [
//                'appid' => 'wxfec1289d29a1ee60',
//                'bank_type' => 'CCB_CREDIT',
//                'cash_fee' => '1',
//                'fee_type' => 'CNY',
//                'is_subscribe' => 'Y',
//                'mch_id' => '1482521772',
//                'nonce_str' => '5b55da17e3f9d',
//                'openid' => 'o-FTAw9ViEldX2xNRxuuCV5NQBhs',
//                'out_trade_no' => '20180723723530479222795',
//                'result_code' => 'SUCCESS',
//                'return_code' => 'SUCCESS',
//                'sign' => 'F7D44DE0008B61ED023FB76A9F155A3D',
//                'time_end' => '20180723213732',
//                'total_fee' => '1',
//                'trade_type' => 'JSAPI',
//                'transaction_id' => '4200000140201807232306907454',
//            ]
            // 使用通知里的 "微信支付订单号" 或者 "商户订单号" 去自己的数据库找到订单
//            $message['out_trade_no']

            //查询对应订单记录
            $order = Order::where('order_sn', $message['out_trade_no'])->first();

//            $order = [
//                'id' => 50,
//                'user_id' => 8,
//                'openid' => 'o-FTAw9ViEldX2xNRxuuCV5NQBhs',
//                'title' => '彩色的翅膀',
//                'order_sn' => '20180723723530479222795',
//                'consignee' => 'fdsfdsfds',
//                'mobile' => '13111112222',
//                'address' => '北京市市辖区通州区fsdfdsfdsfdsfds',
//                'total_price' => '60.00',
//                'payment_method' => 'wechat',
//                'is_paid' => '0',
//                'ship_status' => '0',
//                'paid_at' => NULL,
//                'ship_at' => NULL,
//                'success_at' => NULL,
//                'is_comment' => 0,
//                'orderable_id' => 5,
//                'orderable_type' => 'App\\Topic',
//                'number' => 1,
//                'created_at' => '2018-07-23 21:37:27',
//                'updated_at' => '2018-07-23 21:37:27',
//            ]

            // 如果订单不存在 或者 订单已经支付过了告诉微信，我已经处理完了，订单没找到，别再通知我了
            if (!$order || $order->is_paid) {
                return true;
            }
            $user = User::find($order->user_id);

            if ($message['return_code'] === 'SUCCESS') { // return_code 表示通信状态，不代表支付状态

                // 用户是否支付成功
                if (array_get($message, 'result_code') === 'SUCCESS') {


                    DB::transaction(function () use( $order, $user ) {

                        $this->write(['user_id' => $order->user_id, 'user_name' => $user->nickName, 'description' =>
                            "购买订单支付成功:{$order->title},订单号:{$order->order_sn},价格:{$order->total_price},支付方式:{$order->payment_method}"]);


                        $order->paid_at = Carbon::now()->toDateTimeString(); // 更新支付时间为当前时间
                        $order->is_paid = Order::PAY_SUCCESS;  //更新成功支付状态

                        Account::create([
                            'user_id' => $order->user_id,
                            'money' => $order->total_price,
                            'current_money' => $user->money,
                            'change_type' => Account::PAY,
                            'order_sn' => $order->order_sn,
                            'description' =>"购买订单支付成功:{$order->title},订单号:{$order->order_sn},价格:{$order->total_price},支付方式:{$order->payment_method}"
                        ]);

                        //文章支付的通知
                        if($order->orderable_type == "App\\Topic"){
                            $topic = $order->orderable;

                            if($topic){
                                $user->topics()->attach($topic->id);
                                UserOrder::where('order_id', $order->id)->update(['status' => UserOrder::ORDER_STATUS_SUCCESS]);
                                $this->write(['user_id' => $order->user_id, 'user_name' => $user->nickName, 'description' =>
                                    "添加购买记录成功:{$order->title},订单号:{$order->order_sn},价格:{$order->total_price},支付方式:{$order->payment_method}"]);
                                //$topic->increment('read_counts', 1)   无法触发观察者模式更新事件；
                                $topic->update(['read_counts'=> $topic->read_counts +1]);


                                $order->ship_status = Order::SHIP_SUCCESS;
                                $order->ship_at = Carbon::now();
                                $order->success_at = Carbon::now();
                            }else{
                                $this->write(['user_id' => $order->user_id, 'user_name' => $user->nickName, 'description' =>
                                    "添加购买记录失败:{$order->title},订单号:{$order->order_sn},价格:{$order->total_price},支付方式:{$order->payment_method},失败原因:orderable_id:{$order->orderable_id}数据不存在"]);
                            }
                        }
                    });
                    // 用户支付失败
                }elseif (array_get($message, 'result_code') === 'FAIL') {
                    $this->write(['user_id' => $order->user_id, 'user_name' => $user->nickName, 'description' =>
                        "购买订单支付失败:{$order->title},订单号:{$order->order_sn},价格:{$order->total_price},支付方式:{$order->payment_method},失败原因:{$message['return_msg']}"]);
                    $order->is_pay = 2;
                }
            } else {
                $this->write(['user_id' => $order->user_id, 'user_name' => $user->nickName, 'description' =>
                    "购买订单支付失败:{$order->title},订单号:{$order->order_sn},价格:{$order->total_price},支付方式:{$order->payment_method},失败原因:{$message['return_msg']}"]);
                return $fail('通信失败，请稍后再通知我');
            }

            $order->save(); // 保存订单

            return true; // 返回处理完成
        });

        return $response; // return $response;
    }


}
