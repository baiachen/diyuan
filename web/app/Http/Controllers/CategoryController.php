<?php

namespace App\Http\Controllers;

use App\Account;
use App\Events\LogAccountEvent;
use App\Order;
use App\Repositories\CourseRepository;
use App\Repositories\GradeRepository;
use App\Repositories\TopicRepository;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    //

    public function index(Request $request)
    {
        $param = $this->validate($request,[
            'category_id'=> [
                'required',
                'regex:/^([0-9]+|all)$/'
            ],
            'page' => [
                'required',
                'regex:/^[0-9]+$/'
            ],
            'course_id' => [
                'required',
                'regex:/^([0-9]+|all)$/'
            ],

            'grade_id' => [
                'required',
                'regex:/^([0-9]+|all)$/'
            ]
        ]);


        $Grade = app(GradeRepository::class)->getChild();
        $Course = app(CourseRepository::class)->get();


        $result = [
            'Topic' => app(TopicRepository::class)->get($param),
            'Menu' => [$Course,$Grade]
        ];


        return response()->json(['status' => true, 'message' => $result]);
    }
}
