<?php

namespace App\Http\Controllers;

use App\Repositories\TopicRepository;
use Illuminate\Http\Request;

class TopicController extends Controller
{
    //
    private $topicRepository;

    public function __construct(TopicRepository $topicRepository)
    {
        $this->middleware('authJWT')->except( 'show',  'resources');
        $this->topicRepository = $topicRepository;
    }


    /**
     * 文章详情
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {
        $param = $this->validate($request,[
            'id' => [
                'required',
                'regex:/^[0-9]+$/'
            ],

        ]);

        $result = $this->topicRepository->show($param['id']);


        return json_put(['status' => true, 'message' => $result]);
    }

    public function resources(Request $request)
    {
        $param = $this->validate($request,[
            'id' => [
                'required',
                'regex:/^[0-9]+$/'
            ],

        ]);

        $result = $this->topicRepository->resources($param['id']);


        return json_put(['status' => true, 'message' => $result]);

    }


    /**
     * 文章收藏
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function collect(Request $request)
    {
        $params = $this->validate($request,[
            'id' => [
                'required',
                'regex:/^[0-9]+$/'
            ],
        ]);

        $result = $this->topicRepository->collect($params['id']);

        return json_put(['status' => true, 'message' => $result]);
    }

    /**
     * 支付确认信息页面，目前使用内容付费；
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function order(Request $request)
    {
        $param = $this->validate($request,[
            'id' => [
                'required',
                'regex:/^[0-9]+$/'
            ],

        ]);

        $result = $this->topicRepository->order($param['id']);

        return json_put(['status' => true, 'message' => $result]);

    }
}
