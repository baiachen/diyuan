<?php

namespace App\Http\Controllers;

use App\MsgCode;
use App\Services\LoginService;
use App\Services\WeChatService;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Redis\RedisManager;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use Vinkla\Hashids\Facades\Hashids;

class LoginController extends Controller
{

    public function __construct()
    {
        $this->middleware('authJWT')->except('login', 'login_callback', 'refreshToken', 'send_msg');
    }


    /**
     * 微信登录，手机号登录
     * @param Request $request
     * @param LoginService $loginService
     * @return mixed
     */
    public function login(Request $request, LoginService $loginService)
    {

        $params = $this->validate($request,[
            'mode' => 'required',
            'from' => 'required'
        ]);


//        if($params['mode'] == 'wechat'){
//
//            /**
//             *   test   delete todo
//             */
//
//            $user = User::find(9);
//            $token = app(LoginService::class)->fromUser($user);
//
//            $queries =  '?'.http_build_query([
//                    'status' => true,
//                    'redirect_uri' => $params['from'],
//                    'token' => $token,
//                ]);
//
//
//            return redirect(url($params['from']). $queries)
//                ->cookie('is_login', '1', time()+3600, "/", '',false, false)
//                ->cookie('token', $token, time()+3600, "/", '', false,false)
//                ->cookie('u_id', Hashids::encode($user->id), time()+3600, "/", '', false,false)
//                ->cookie('expires_in', time() + config('jwt.ttl') * 50, time()+3600, "/", '', false,false);
//        }



        return  $loginService->make($params['mode'])->login($request->all());
    }

    /**
     * 微信登录回调处理
     * @param Request $request
     * @param WeChatService $weChatService
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function login_callback(Request $request, WeChatService $weChatService)
    {
        if($request->has('code')){
            $from = $request->get('from', '/');
            return $weChatService->loginCallBack($from);
        }
    }

    /**
     * 刷新token
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function refreshToken(Request $request)
    {
        $params = $this->validate($request,[
            'u_id' => 'required|max:32'
        ]);

        $newToken = app(LoginService::class)->refreshToken($params['u_id']);

        return json_put(['status' => true, 'message' => ['token' => $newToken, 'u_id' => $params['u_id'], 'is_login' => 1, 'expires_in' => time() + config('jwt.ttl') *50]]);
    }

    /**
     * 发送手机验证码
     * @param Request $request
     * @param MsgCode $msgCode
     * @return \Illuminate\Http\JsonResponse
     */
    public function send_msg(Request $request, MsgCode $msgCode)
    {
        $param = $this->validate($request,[
            'mobile' => 'required|numeric'
        ]);

        if(User::where('mobile', $param['mobile'])->exists()){
            return json_put(['status' => false, 'message' => '手机号码已被注册']);
        }

        $keyName = app(LoginService::class)->keyName($param['mobile']);

        //存在的在有效期间 返回不能重复提交
        if($code = app(RedisManager::class)->get($keyName)){
            return json_put(['status' => false, 'message' => '过会儿再试', 'code' => $code]);
        }


        $code = rand(100000,999999);
        //缓存十分钟
        app(RedisManager::class)->setex($keyName, 60*10, $code);

        $msgCode->create([
            'mobile' => $param['mobile'],
            'code' => $code,
            'description' => ''
        ]);


        return json_put(['status' => true, 'message' => ['code' => $code]]);

    }

    public function bindMobile(Request $request)
    {
        return $request->all();

    }






}
