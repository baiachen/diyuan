<?php

namespace App\Http\Controllers;

use App\Repositories\BookRepository;
use Illuminate\Http\Request;

class BookController extends Controller
{
    //
    private $bookRepository;

    public function __construct(BookRepository $bookRepository)
    {
        $this->bookRepository = $bookRepository;


    }

    public function index(Request $request)
    {
        $param = $this->validate($request,[
            'sales'=> [
                'regex:/^(asc|desc)$/'
            ],
            'page' => [
                'required',
                'regex:/^[0-9]+$/'
            ],
            'price' => [
                'regex:/^(asc|desc)$/'
            ],
        ]);


        $result = $this->bookRepository->get($param);

        return response()->json(['status' => true, 'message' => $result]);
    }

    /**
     * 书本详情页
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {
        $param = $this->validate($request,[
            'id' => [
                'required',
                'regex:/^[0-9]+$/'
            ],
        ]);

        $result = $this->bookRepository->show($param['id']);

        return response()->json(['status' => true, 'message' => $result]);
    }


    /**
     * 书本收藏
     * @param Request $request
     * @param TopicRepository $topicRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function collect(Request $request)
    {
        $params = $this->validate($request,[
            'id' => [
                'required',
                'regex:/^[0-9]+$/'
            ],
        ]);

        $result = $this->bookRepository->collect($params['id']);

        return json_put(['status' => true, 'message' => $result]);
    }
}
