<?php

namespace App\Http\Controllers;

use App\Account;
use App\Events\LogAccountEvent;
use App\Order;
use App\Services\IndexService;
use App\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class IndexController extends Controller
{
    //
    public function __construct()
    {
//        $this->middleware('authJWT');
    }

    public function index(IndexService $indexService, Request $request)
    {



        $param = $this->validate($request,[
            'category_id'=> [
                'required',
                'regex:/^([0-9]+|all)$/'
            ],
            'page' => [
                'required',
                'regex:/^[0-9]+$/'
            ],
            'course_id' => [
                'required',
                'regex:/^([0-9]+|all)$/'
            ],

            'grade_id' => [
                'required',
                'regex:/^([0-9]+|all)$/'
            ]
        ]);


        $result = $indexService->get($param);

        return json_put(['status' => true, 'message' => $result]);
    }

}
