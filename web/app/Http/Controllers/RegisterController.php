<?php

namespace App\Http\Controllers;

use App\Services\LoginService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Redis\RedisManager;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;
use Vinkla\Hashids\Facades\Hashids;

class RegisterController extends Controller
{

    public function __construct()
    {
        $this->middleware('authJWT');
    }

    /**
     * 手机注册接口
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, User $user)
    {
        $params = $this->validate($request,[
            'mobile' => 'required|numeric',
//            'code' => 'required|numeric',
        ]);

//        $keyName = app(LoginService::class)->keyName($params['mobile']);
//
//        $code = app(RedisManager::class)->get($keyName);
//
//        if($code != $params['code']){
//            return json_put(['status' => false, 'message' => '验证码错误']);
//        }

        $User = $user->newQuery()->where('mobile', $params['mobile'])->first();

        if(! $User){
            Auth::user()->update(['mobile' => $params['mobile']]);

            return json_put(['status' => true, 'message' => '绑定成功']);
        }

        return json_put(['status' => false, 'message' => '该手机号码已被注册']);


    }
}
