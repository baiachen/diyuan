<?php

namespace App\Http\Controllers;

use App\Course;
use App\Note;
use App\User;
use App\UserOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\HttpException;

class UserCenterController extends Controller
{

    public function __construct()
    {
        $this->middleware('authJWT')->except('note');
    }


    //
    public function index()
    {

        $response['userInfo'] = array_only(Auth::user()->toArray(), ['id', 'nickName', 'avatarUrl', 'openId']);
        $response['phone'] = setting('PHONE');

        return json_put(['status' => true, 'message' => $response]);
    }

    /**
     * 所有订单
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function orders(Request $request)
    {
        $param = $this->validate($request,[
            'type' => [
                'required',
                'regex:/^(all|[0-9]+)$/'
            ],
        ]);

        $builder = $order = Auth::user()->userOrders();

        if($param['type'] == 'all'){
            $result['data'] = $builder->orderBy('created_at', 'desc')->simplePaginate(20);

        }else{
            $result['data'] = $builder->where('status', $param['type'])->orderBy('created_at', 'desc')->simplePaginate(20);
        }


        $result['order_status'] = $order->where('isLook', 0)->get()->groupBy(function ($item, $key) {
            return UserOrder::ORDER_STATUS[$item->status];
        })->map(function ($value,$key){
            return $value->count();
        });

        return json_put(['status' => true, 'message' => $result ]);

    }

    public function userInfo()
    {

        $user = Auth::user();
        $user['address'] = $user->addresses()->where('is_default', 1)->first();

        return json_put(['status' => true, 'message' => $user]);
    }


    public function material(Request $request)
    {
        $param = $this->validate($request,[
            'type' => [
                'required',
                'regex:/^(all|[0-9]+)$/'
            ],
        ]);

        $type = $param['type'];

        $builder = Auth::user()->topics();

        if($type == 'all'){
            $builder->with('grade:id,title', 'category:id,name', 'course:id,name', 'tags:id,name');
        }else{
            $builder->whereHas('course',function ($query) use($type){
                $query->where('id', $type);
            })->with('grade:id,title', 'category:id,name', 'course:id,name', 'tags:id,name');
        }



        return json_put(['status' => true, 'message' => $builder->orderBy('created_at', 'desc')->simplePaginate(20)]);
    }

    public function note(Note $note)
    {
        return json_put(['status' => true, 'message' => $note->whereIn('id', [1,2,3])->get()]);
    }

}
