<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/28 0028
 * Time: 下午 5:41
 */

namespace App\Repositories;


use App\Grade;
use Illuminate\Support\Facades\Cache;

class GradeRepository
{
    private $grade;

    public function __construct(Grade $grade)
    {
        $this->grade = $grade;
    }

    public function get()
    {
        return Cache::remember('grade', 60, function (){
            return $this->grade->toTree();
        });
    }

    public function getChild()
    {
        return Cache::remember('grade_child', 60, function (){
            return $this->grade->where('parent_id','<>', 0)->orderBy('order','asc')->get(['id', 'title as name', 'order']);
        });
    }



}