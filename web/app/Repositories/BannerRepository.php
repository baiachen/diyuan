<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/28 0028
 * Time: 下午 5:28
 */

namespace App\Repositories;


use App\Banner;
use Illuminate\Support\Facades\Cache;

class BannerRepository
{
    private $banner;

    public function __construct(Banner $banner)
    {
        $this->banner = $banner;
    }

    public function get()
    {
        return Cache::remember('banner', 60, function (){
            return $this->banner->orderBy('order', 'asc')->get()->pluck('imgUrl', 'id')->values();
        });
    }

}