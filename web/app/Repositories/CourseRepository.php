<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/7/6 0006
 * Time: 下午 4:29
 */

namespace App\Repositories;


use App\Course;
use Illuminate\Support\Facades\Cache;

class CourseRepository
{

    private $course;

    public function __construct(Course $course)
    {
        $this->course = $course;
    }

    public function get()
    {
        return Cache::remember('course', 60, function (){
            return $this->course->orderBy('order', 'asc')->get(['id', 'name', 'order']);
        });
    }

}