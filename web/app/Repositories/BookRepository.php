<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/29 0029
 * Time: 下午 2:55
 */

namespace App\Repositories;


use App\Book;
use Illuminate\Support\Facades\Cache;

class BookRepository
{

    private $book;

    public function __construct(Book $book)
    {
        $this->book = $book;
    }

    public function get(array $param)
    {

        return Cache::tags('books')->remember(join('-', $param), 60, function () use($param){
            $builder = $this->book->newQuery()->where('status', Book::STATUS_ON);

            $builder->with('grade:id,title', 'course:id,name');

            if(isset($param['sales'])){
                $builder->orderBy('sales', $param['sales']);
            }

            if(isset($param['price'])){
                $builder->orderBy('sales', $param['price']);
            }

            return $builder->simplePaginate(20);
        });
    }

    public function show(int $id)
    {
//        Cache::tags('books')->flush();
        $book =  Cache::tags('books')->remember('bd_'.$id, 60, function () use($id){
            $Book =  $this->book->findOrFail($id);

            return [
                'out' => [
                    'id' =>$Book->id,
                ],
                'instance' => $Book,
            ];

        });


        $book['out']['isCollect'] = $book['instance']->collect()->where('user_id', 1)->exists();

        return $book['out'];
    }


    public function search(string $kw)
    {
        return $this->book->newQuery()->where('status', Book::STATUS_ON)
            ->where(function ($query) use($kw){
                $query->where('title', 'like', "%{$kw}%")
                    ->orWhere('description', 'like', "%{$kw}%")
                    ->orWhereHas('grade',function ($query) use($kw){
                        $query->where('title', 'like', "%{$kw}%");
                    })->orWhereHas('course', function ($query) use($kw){
                        $query->where('name', 'like', "%{$kw}%");
                    });
            })->limit(10)->latest('sales')->get();
    }


    public function collect(int $id)
    {
        $book = $this->book->findOrFail($id);
        if($book->collect()->where('user_id', 1)->exists()){
            return $book->collect()->where('user_id', 1)->delete() ? '取消收藏' : '取消收藏失败';
        }

        return $book->collect()->create(['user_id'=> 1]) ? '收藏成功' : '收藏失败';
    }



}