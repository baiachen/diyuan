<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/28 0028
 * Time: 下午 6:03
 */

namespace App\Repositories;


use App\Services\WeChatService;
use App\Topic;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\Facades\JWTAuth;

class TopicRepository
{

    private $topic;

    public function __construct(Topic $topic)
    {
        $this->topic = $topic;
    }

    public function get(array $param)
    {
//        Cache::tags('topic')->flush();

        return Cache::tags('topic')->remember(join('-', $param), 60, function () use($param){



            $builder = $this->topic->newQuery()
                ->select('id','title','grade_id',
                    'description','category_id', 'course_id','price','read_counts','resource_type','teacher_name')
                ->where('status', Topic::STATUS_ON);

            $builder->with('grade:id,title', 'category:id,name', 'course:id,name', 'tags:id,name');

            $builder->withCount('resources');

            $param = $this->IsAll($param);


            if(!$param['course_all']){
                $builder->where('course_id', $param['course_id']);
            }

            if(!$param['category_all']){
                $builder->where('category_id', $param['category_id']);
            }

            if(!$param['grade_all']){
                $builder->where('grade_id', $param['grade_id']);
            }

            return $builder->latest()->simplePaginate(20);
        });
    }

    public function show(int $id)
    {

        $user = '';

        if(request()->hasHeader('Authorization')){

            try {
                if (! $user = JWTAuth::parseToken()->authenticate()) {
                    return response()->json(['message'=>'用户不存在', 'code'=>403], 403);
                }

            } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
                return response()->json(['message'=>'token已过期', 'code'=>$e->getStatusCode()],$e->getStatusCode());
            } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
                return response()->json(['message'=>'无效的token', 'code'=>$e->getStatusCode()],$e->getStatusCode());
            } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
                return response()->json(['message'=>'异常错误', 'code'=>$e->getStatusCode()],$e->getStatusCode());
            }

        }




//        Cache::tags('topic')->flush();
        $topic =  Cache::tags('topic')->remember('td_'.$id, 60, function () use($id){
            $Topic =  $this->topic->findOrFail($id);

//            $maxTopic = $this->topic->newQuery()->max('id');
//            $numbers = range (1,$maxTopic);
////shuffle 将数组顺序随即打乱
//            shuffle ($numbers);
////array_slice 取该数组中的某一段
//            $num=3;
//            $numsWhere = array_slice($numbers,0,$num);
//


            return [
                'out' => [
                    'id' =>$Topic->id,
                    'thumbnail' => $Topic->thumbnail,  //缩略图
                    'resource_type' => $Topic->resource_type, //类型
                    'description' => $Topic->description,  //描述
                    'price' => $Topic->price,    //价格
                    'read_counts' => $Topic->read_counts,   //购买人数
                    'status' => $Topic->status,   //状态
                    'teacher_name' => $Topic->teacher_name,  //教师名称
                    'title' => $Topic->title,   //文章标题
                    'grade' => $Topic->grade->title,   //年级
                    'category' => $Topic->category->name,  //分类
                    'course' => $Topic->course->name,  //课程
                    'tags' => $Topic->tags,  //标签
                    'updated_at' => $Topic->updated_at->toDateString(),
//                    'recommend' => $this->topic->newQuery()->where('id','<>', $Topic->id)->whereIn('id',$numsWhere)->get(['id', 'title', 'price'])
                    'recommend' => $this->topic->newQuery()->where('id','<>', $Topic->id)->inRandomOrder()->limit(3)->get(['id','category_id', 'title', 'price'])
                ],
                'instance' => $Topic,
            ];

        });

        $topic['out']['isCollect'] = false;
        $topic['out']['isBuy'] = false;
        $_topic = $topic['instance'];
        $topic['out']['resources'] = $_topic->resources->each(function($item) use($_topic, $user ){

            if($item->is_free){
                if($user){
                    if(! $_topic->users()->where('user_id',$user->id)->exists()){
                        $item->resource_path = null;
                    }
                }else{
                    $item->resource_path = null;
                }

            }
        });   //资源
        

        if($user){
            //实时更新是否收藏状态；
            $topic['out']['isCollect'] = $_topic->collect()->where('user_id', $user->id)->exists();
            $topic['out']['isBuy'] = $_topic->users()->where('user_id',$user->id)->exists();
        }

        $topic['out']['phone'] = setting('PHONE');

        return $topic['out'];
    }

    public function collect(int $id)
    {

        $topic = $this->topic->findOrFail($id);
        if($topic->collect()->where('user_id', Auth::id())->exists()){
            return $topic->collect()->where('user_id', Auth::id())->delete() ? '取消收藏' : '取消收藏失败';
        }

        return $topic->collect()->create(['user_id'=> Auth::id()]) ? '收藏成功' : '收藏失败';
    }

    public function resources(int $id)
    {
        return Cache::tags('topic')->remember('ts'.$id, 60, function () use($id) {
            $topic = $this->topic->findOrFail($id);
            return $topic->resources;
        });

    }

    public function order(int $id)
    {
        $user = Auth::user();

        if(! $user){
            throw new HttpException(410, '用户不存在');
        }

        $result =   $this->topic->findOrFail($id);
        $result->address = $user->addresses()->where('is_default', 1)->first() ?: '';
        $result->hasDefaultAddress = $result->address? true : false;
        $result->config = app(WeChatService::class)->make()->jsk();
        return $result;

    }


    protected function IsAll(array $param) : array
    {
        $param['category_all'] = $param['category_id'] === 'all' ?: false;
        $param['course_all'] = $param['course_id'] === 'all' ?: false;
        $param['grade_all'] = $param['grade_id'] === 'all' ?: false;

        return $param;
    }



}