<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/6/28 0028
 * Time: 下午 5:41
 */

namespace App\Repositories;


use App\Category;
use Illuminate\Support\Facades\Cache;

class CategoryRepository
{

    private $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function get()
    {
        return Cache::remember('category', 60, function (){
            return $this->category->orderBy('order', 'asc')->get(['id', 'name', 'icon', 'order']);
        });
    }

}