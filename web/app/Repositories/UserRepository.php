<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/7/2 0002
 * Time: 上午 11:29
 */

namespace App\Repositories;


use App\User;
use Illuminate\Support\Facades\Log;

class UserRepository
{
    private $user;

    public function __construct(User $user)
    {
        $this->user  = $user;
    }





//[2018-07-02 12:28:38] local.INFO: array (
//'openid' => 'ofB4esxXi_KdVqzI7O4t3dkSLKlA',
//'nickname' => '陈江',
//'sex' => 1,
//'language' => 'zh_CN',
//'city' => '南京',
//'province' => '江苏',
//'country' => '中国',
//'headimgurl' => 'http://thirdwx.qlogo.cn/mmopen/vi_32/ajNVdqHZLLBfZ5XicNG3O5q3U7kQFo0NzIH16rsjoqib4YFiaPibYemvaHLkS7h9FiadAEUOrKgDd38OchrHmSEFYgw/132',
//'privilege' =>
//array (
//),
//'unionid' => 'oqYdc0v5B6xuxNZz5Bxq_JtP8zdU',
//)

    public function save(array $user )
    {

        $User = $this->user->where('openId' , $user['openid'])->first();

        if(! $User){
            $User =  $this->user->create([
                'openId' => $user['openid'],
                'unionid' => $user['unionid'],
                'nickName' => $user['nickname'],
                'realName' => '',
                'avatarUrl' => $user['headimgurl'],
                'mobile' => '',
                'sex' => $user['sex'],
                'password' => '',
                'money' => 0,
            ]);
        }

        return $User;
    }
}