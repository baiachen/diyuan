# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.19-log)
# Database: diyuan_bnxxjs_c
# Generation Time: 2018-07-21 09:37:48 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table bn_accounts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bn_accounts`;

CREATE TABLE `bn_accounts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户id',
  `money` decimal(10,2) NOT NULL COMMENT '交易金额',
  `current_money` decimal(10,2) NOT NULL COMMENT '当前结余',
  `change_type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '改变类型0支付1收入',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '描述',
  `type` tinyint(3) unsigned NOT NULL COMMENT '资金类型0支付，1提现，2退回',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `accounts_user_id_foreign` (`user_id`),
  CONSTRAINT `accounts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `bn_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table bn_addresses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bn_addresses`;

CREATE TABLE `bn_addresses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户id',
  `consignee` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '收货人',
  `mobile` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '联系号码',
  `province` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '省份',
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '城市',
  `area` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '区',
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '详细地址',
  `is_default` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否默认',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `addresses_user_id_foreign` (`user_id`),
  KEY `addresses_is_default_index` (`is_default`),
  CONSTRAINT `addresses_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `bn_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `bn_addresses` WRITE;
/*!40000 ALTER TABLE `bn_addresses` DISABLE KEYS */;

INSERT INTO `bn_addresses` (`id`, `user_id`, `consignee`, `mobile`, `province`, `city`, `area`, `address`, `is_default`, `created_at`, `updated_at`)
VALUES
	(1,2,'咸鱼','13111111111','浙江省','金华市','婺城区','发生大幅收费',1,NULL,NULL);

/*!40000 ALTER TABLE `bn_addresses` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table bn_admin_menu
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bn_admin_menu`;

CREATE TABLE `bn_admin_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uri` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `bn_admin_menu` WRITE;
/*!40000 ALTER TABLE `bn_admin_menu` DISABLE KEYS */;

INSERT INTO `bn_admin_menu` (`id`, `parent_id`, `order`, `title`, `icon`, `uri`, `created_at`, `updated_at`)
VALUES
	(1,0,1,'Index','fa-bar-chart','/',NULL,NULL),
	(2,0,2,'Admin','fa-tasks','',NULL,NULL),
	(3,2,3,'Users','fa-users','auth/users',NULL,NULL),
	(4,2,4,'Roles','fa-user','auth/roles',NULL,NULL),
	(5,2,5,'Permission','fa-ban','auth/permissions',NULL,NULL),
	(6,2,6,'Menu','fa-bars','auth/menu',NULL,NULL),
	(7,2,7,'Operation log','fa-history','auth/logs',NULL,NULL),
	(8,11,10,'年级设置','fa-stop','grade','2018-06-30 08:56:26','2018-06-30 09:48:40'),
	(9,11,9,'分类设置','fa-hashtag','category','2018-06-30 09:05:36','2018-06-30 09:48:40'),
	(10,11,11,'课目设置','fa-server','course','2018-06-30 09:16:24','2018-06-30 09:48:40'),
	(11,0,8,'系统配置','fa-sun-o',NULL,'2018-06-30 09:47:37','2018-06-30 09:48:40'),
	(12,11,0,'参数设置','fa-bars','config','2018-06-30 09:49:04','2018-06-30 09:49:04'),
	(13,0,0,'内容管理','fa-align-left','topic','2018-06-30 09:50:47','2018-06-30 09:50:47'),
	(14,11,0,'标签设置','fa-bars','tag','2018-06-30 10:00:57','2018-06-30 10:00:57'),
	(15,11,0,'轮播图','fa-image','banner','2018-07-06 09:20:04','2018-07-06 09:20:04');

/*!40000 ALTER TABLE `bn_admin_menu` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table bn_admin_operation_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bn_admin_operation_log`;

CREATE TABLE `bn_admin_operation_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `input` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_operation_log_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `bn_admin_operation_log` WRITE;
/*!40000 ALTER TABLE `bn_admin_operation_log` DISABLE KEYS */;

INSERT INTO `bn_admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`)
VALUES
	(1,1,'admin','GET','220.188.83.134','[]','2018-06-30 11:49:05','2018-06-30 11:49:05'),
	(2,1,'admin/category','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 11:49:14','2018-06-30 11:49:14'),
	(3,1,'admin/grade','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 11:49:15','2018-06-30 11:49:15'),
	(4,1,'admin/course','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 11:49:16','2018-06-30 11:49:16'),
	(5,1,'admin/config','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 11:49:16','2018-06-30 11:49:16'),
	(6,1,'admin/tag','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 11:49:17','2018-06-30 11:49:17'),
	(7,1,'admin/topic','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 11:49:18','2018-06-30 11:49:18'),
	(8,1,'admin/config','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 13:03:21','2018-06-30 13:03:21'),
	(9,1,'admin/tag','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 13:03:23','2018-06-30 13:03:23'),
	(10,1,'admin/config','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 13:03:23','2018-06-30 13:03:23'),
	(11,1,'admin/course','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 13:03:24','2018-06-30 13:03:24'),
	(12,1,'admin/grade','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 13:03:24','2018-06-30 13:03:24'),
	(13,1,'admin/category','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 13:03:25','2018-06-30 13:03:25'),
	(14,1,'admin/category','GET','220.188.83.134','[]','2018-06-30 13:07:45','2018-06-30 13:07:45'),
	(15,1,'admin','GET','220.188.83.134','[]','2018-06-30 13:49:05','2018-06-30 13:49:05'),
	(16,1,'admin/category','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 13:50:18','2018-06-30 13:50:18'),
	(17,1,'admin/grade','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 13:51:23','2018-06-30 13:51:23'),
	(18,1,'admin/category','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 13:51:24','2018-06-30 13:51:24'),
	(19,1,'admin/grade','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 13:51:31','2018-06-30 13:51:31'),
	(20,1,'admin/course','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 13:51:33','2018-06-30 13:51:33'),
	(21,1,'admin/config','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 13:51:34','2018-06-30 13:51:34'),
	(22,1,'admin/tag','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 13:51:35','2018-06-30 13:51:35'),
	(23,1,'admin/config','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 13:51:37','2018-06-30 13:51:37'),
	(24,1,'admin/course','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 13:51:38','2018-06-30 13:51:38'),
	(25,1,'admin/grade','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 13:51:39','2018-06-30 13:51:39'),
	(26,1,'admin/category','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 13:51:39','2018-06-30 13:51:39'),
	(27,1,'admin/grade','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 13:51:55','2018-06-30 13:51:55'),
	(28,1,'admin/course','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 13:51:56','2018-06-30 13:51:56'),
	(29,1,'admin/config','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 13:51:58','2018-06-30 13:51:58'),
	(30,1,'admin/tag','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 13:51:59','2018-06-30 13:51:59'),
	(31,1,'admin/topic','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 13:52:00','2018-06-30 13:52:00'),
	(32,1,'admin/category','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:01:48','2018-06-30 14:01:48'),
	(33,1,'admin/category/create','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:01:50','2018-06-30 14:01:50'),
	(34,1,'admin/category','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:02:07','2018-06-30 14:02:07'),
	(35,1,'admin/grade','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:02:08','2018-06-30 14:02:08'),
	(36,1,'admin/grade/create','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:02:15','2018-06-30 14:02:15'),
	(37,1,'admin/grade','POST','220.188.83.134','{\"parent_id\":\"0\",\"title\":\"\\u5c0f\\u5b66\\u9636\\u6bb5\",\"order\":\"0\",\"_token\":\"JpBWyqgthQfv8acGyrJWNhWVHwjwMI08fnGBRwif\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/grade\"}','2018-06-30 14:02:26','2018-06-30 14:02:26'),
	(38,1,'admin/grade','GET','220.188.83.134','[]','2018-06-30 14:02:27','2018-06-30 14:02:27'),
	(39,1,'admin/grade/create','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:02:28','2018-06-30 14:02:28'),
	(40,1,'admin/grade','POST','220.188.83.134','{\"parent_id\":\"1\",\"title\":\"\\u4e00\\u5e74\\u7ea7\",\"order\":\"0\",\"_token\":\"JpBWyqgthQfv8acGyrJWNhWVHwjwMI08fnGBRwif\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/grade\"}','2018-06-30 14:02:34','2018-06-30 14:02:34'),
	(41,1,'admin/grade','GET','220.188.83.134','[]','2018-06-30 14:02:35','2018-06-30 14:02:35'),
	(42,1,'admin/course','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:02:37','2018-06-30 14:02:37'),
	(43,1,'admin/course/create','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:02:40','2018-06-30 14:02:40'),
	(44,1,'admin/course','POST','220.188.83.134','{\"name\":\"\\u8bed\\u6587\",\"order\":\"0\",\"_token\":\"JpBWyqgthQfv8acGyrJWNhWVHwjwMI08fnGBRwif\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/course\"}','2018-06-30 14:02:43','2018-06-30 14:02:43'),
	(45,1,'admin/course','GET','220.188.83.134','[]','2018-06-30 14:02:43','2018-06-30 14:02:43'),
	(46,1,'admin/config','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:02:45','2018-06-30 14:02:45'),
	(47,1,'admin/tag','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:02:46','2018-06-30 14:02:46'),
	(48,1,'admin/tag/create','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:02:47','2018-06-30 14:02:47'),
	(49,1,'admin/tag','POST','220.188.83.134','{\"name\":\"\\u5f88\\u4e0d\\u9519\",\"_token\":\"JpBWyqgthQfv8acGyrJWNhWVHwjwMI08fnGBRwif\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/tag\"}','2018-06-30 14:02:56','2018-06-30 14:02:56'),
	(50,1,'admin/tag','GET','220.188.83.134','[]','2018-06-30 14:02:56','2018-06-30 14:02:56'),
	(51,1,'admin/topic','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:02:58','2018-06-30 14:02:58'),
	(52,1,'admin/topic','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:03:08','2018-06-30 14:03:08'),
	(53,1,'admin/topic/create','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:03:09','2018-06-30 14:03:09'),
	(54,1,'admin/topic/create','GET','220.188.83.134','[]','2018-06-30 14:03:12','2018-06-30 14:03:12'),
	(55,1,'admin/act/clean_cache','POST','220.188.83.134','{\"_token\":\"JpBWyqgthQfv8acGyrJWNhWVHwjwMI08fnGBRwif\"}','2018-06-30 14:03:17','2018-06-30 14:03:17'),
	(56,1,'admin/topic/create','GET','220.188.83.134','[]','2018-06-30 14:03:18','2018-06-30 14:03:18'),
	(57,1,'admin/category','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:03:27','2018-06-30 14:03:27'),
	(58,1,'admin/category/create','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:03:29','2018-06-30 14:03:29'),
	(59,1,'admin/category','POST','220.188.83.134','{\"name\":\"\\u89c6\\u9891\",\"_token\":\"JpBWyqgthQfv8acGyrJWNhWVHwjwMI08fnGBRwif\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/category\"}','2018-06-30 14:03:42','2018-06-30 14:03:42'),
	(60,1,'admin/category/create','GET','220.188.83.134','[]','2018-06-30 14:03:42','2018-06-30 14:03:42'),
	(61,1,'admin/category','POST','220.188.83.134','{\"name\":\"\\u89c6\\u9891\",\"_token\":\"JpBWyqgthQfv8acGyrJWNhWVHwjwMI08fnGBRwif\"}','2018-06-30 14:05:56','2018-06-30 14:05:56'),
	(62,1,'admin/category','GET','220.188.83.134','[]','2018-06-30 14:05:56','2018-06-30 14:05:56'),
	(63,1,'admin/topic','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:05:59','2018-06-30 14:05:59'),
	(64,1,'admin/topic/create','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:06:01','2018-06-30 14:06:01'),
	(65,1,'admin/act/clean_cache','POST','220.188.83.134','{\"_token\":\"JpBWyqgthQfv8acGyrJWNhWVHwjwMI08fnGBRwif\"}','2018-06-30 14:06:06','2018-06-30 14:06:06'),
	(66,1,'admin/topic/create','GET','220.188.83.134','[]','2018-06-30 14:06:07','2018-06-30 14:06:07'),
	(67,1,'admin/topic','POST','220.188.83.134','{\"category_id\":\"1\",\"grade_id\":\"2\",\"course_id\":\"1\",\"title\":\"\\u4e00\\u5e74\\u7ea7\\u4e0a\\u518c\\u82f1\\u8bed\\u6613\\u6df7\\u6dc6\\u53e5\\u5b50\\u603b\\u7ed3\",\"price\":\"10.00\",\"teacher_name\":\"sss\",\"description\":\"fsdfsdfds\",\"tags\":[\"1\",null],\"status\":\"off\",\"resources\":{\"new_1\":{\"title\":\"\\u5c0f\\u8717\\u725b\",\"resource_path\":\"http:\\/\\/data.73305.com\\/1\\/rjbyys\\/108.mp4\",\"is_free\":\"off\",\"order\":\"0\",\"id\":null,\"_remove_\":\"0\"}},\"_token\":\"JpBWyqgthQfv8acGyrJWNhWVHwjwMI08fnGBRwif\"}','2018-06-30 14:11:44','2018-06-30 14:11:44'),
	(68,1,'admin/topic','GET','220.188.83.134','[]','2018-06-30 14:11:44','2018-06-30 14:11:44'),
	(69,1,'admin/topic','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:11:59','2018-06-30 14:11:59'),
	(70,1,'admin/category','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:12:27','2018-06-30 14:12:27'),
	(71,1,'admin/grade','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:12:31','2018-06-30 14:12:31'),
	(72,1,'admin/category','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:12:32','2018-06-30 14:12:32'),
	(73,1,'admin/category/create','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:12:34','2018-06-30 14:12:34'),
	(74,1,'admin/category','POST','220.188.83.134','{\"name\":\"\\u97f3\\u9891\",\"_token\":\"JpBWyqgthQfv8acGyrJWNhWVHwjwMI08fnGBRwif\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/category\"}','2018-06-30 14:12:45','2018-06-30 14:12:45'),
	(75,1,'admin/category/create','GET','220.188.83.134','[]','2018-06-30 14:12:46','2018-06-30 14:12:46'),
	(76,1,'admin/category','POST','220.188.83.134','{\"name\":\"\\u97f3\\u9891\",\"_token\":\"JpBWyqgthQfv8acGyrJWNhWVHwjwMI08fnGBRwif\"}','2018-06-30 14:12:52','2018-06-30 14:12:52'),
	(77,1,'admin/category','GET','220.188.83.134','[]','2018-06-30 14:12:52','2018-06-30 14:12:52'),
	(78,1,'admin/category/create','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:13:35','2018-06-30 14:13:35'),
	(79,1,'admin/category','POST','220.188.83.134','{\"name\":\"\\u7535\\u5b50\\u4e66\",\"_token\":\"JpBWyqgthQfv8acGyrJWNhWVHwjwMI08fnGBRwif\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/category\"}','2018-06-30 14:13:45','2018-06-30 14:13:45'),
	(80,1,'admin/category','GET','220.188.83.134','[]','2018-06-30 14:13:46','2018-06-30 14:13:46'),
	(81,1,'admin/category','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:13:48','2018-06-30 14:13:48'),
	(82,1,'admin/grade','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:13:52','2018-06-30 14:13:52'),
	(83,1,'admin/grade/create','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:13:54','2018-06-30 14:13:54'),
	(84,1,'admin/grade','POST','220.188.83.134','{\"parent_id\":\"1\",\"title\":\"\\u4e8c\\u5e74\\u7ea7\",\"order\":\"0\",\"_token\":\"JpBWyqgthQfv8acGyrJWNhWVHwjwMI08fnGBRwif\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/grade\"}','2018-06-30 14:14:04','2018-06-30 14:14:04'),
	(85,1,'admin/grade','GET','220.188.83.134','[]','2018-06-30 14:14:04','2018-06-30 14:14:04'),
	(86,1,'admin/grade/create','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:20:14','2018-06-30 14:20:14'),
	(87,1,'admin/grade','POST','220.188.83.134','{\"parent_id\":\"1\",\"title\":\"\\u4e09\\u5e74\\u7ea7\",\"order\":\"0\",\"_token\":\"JpBWyqgthQfv8acGyrJWNhWVHwjwMI08fnGBRwif\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/grade\"}','2018-06-30 14:20:21','2018-06-30 14:20:21'),
	(88,1,'admin/grade','GET','220.188.83.134','[]','2018-06-30 14:20:21','2018-06-30 14:20:21'),
	(89,1,'admin/grade/create','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:20:22','2018-06-30 14:20:22'),
	(90,1,'admin/grade','POST','220.188.83.134','{\"parent_id\":\"1\",\"title\":\"\\u56db\\u5e74\\u7ea7\",\"order\":\"0\",\"_token\":\"JpBWyqgthQfv8acGyrJWNhWVHwjwMI08fnGBRwif\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/grade\"}','2018-06-30 14:20:28','2018-06-30 14:20:28'),
	(91,1,'admin/grade','GET','220.188.83.134','[]','2018-06-30 14:20:28','2018-06-30 14:20:28'),
	(92,1,'admin/grade/create','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:20:29','2018-06-30 14:20:29'),
	(93,1,'admin/grade','POST','220.188.83.134','{\"parent_id\":\"1\",\"title\":\"\\u4e94\\u5e74\\u7ea7\",\"order\":\"0\",\"_token\":\"JpBWyqgthQfv8acGyrJWNhWVHwjwMI08fnGBRwif\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/grade\"}','2018-06-30 14:20:36','2018-06-30 14:20:36'),
	(94,1,'admin/grade','GET','220.188.83.134','[]','2018-06-30 14:20:36','2018-06-30 14:20:36'),
	(95,1,'admin/grade/create','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:20:38','2018-06-30 14:20:38'),
	(96,1,'admin/grade','POST','220.188.83.134','{\"parent_id\":\"1\",\"title\":\"\\u516d\\u5e74\\u7ea7\",\"order\":\"0\",\"_token\":\"JpBWyqgthQfv8acGyrJWNhWVHwjwMI08fnGBRwif\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/grade\"}','2018-06-30 14:20:44','2018-06-30 14:20:44'),
	(97,1,'admin/grade','GET','220.188.83.134','[]','2018-06-30 14:20:45','2018-06-30 14:20:45'),
	(98,1,'admin/course','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:22:02','2018-06-30 14:22:02'),
	(99,1,'admin/course/create','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:22:04','2018-06-30 14:22:04'),
	(100,1,'admin/course','POST','220.188.83.134','{\"name\":\"\\u6570\\u5b66\",\"order\":\"0\",\"_token\":\"JpBWyqgthQfv8acGyrJWNhWVHwjwMI08fnGBRwif\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/course\"}','2018-06-30 14:22:09','2018-06-30 14:22:09'),
	(101,1,'admin/course','GET','220.188.83.134','[]','2018-06-30 14:22:10','2018-06-30 14:22:10'),
	(102,1,'admin/course/create','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:22:11','2018-06-30 14:22:11'),
	(103,1,'admin/course','POST','220.188.83.134','{\"name\":\"\\u82f1\\u8bed\",\"order\":\"0\",\"_token\":\"JpBWyqgthQfv8acGyrJWNhWVHwjwMI08fnGBRwif\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/course\"}','2018-06-30 14:22:15','2018-06-30 14:22:15'),
	(104,1,'admin/course','GET','220.188.83.134','[]','2018-06-30 14:22:15','2018-06-30 14:22:15'),
	(105,1,'admin/config','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:22:17','2018-06-30 14:22:17'),
	(106,1,'admin/tag','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:22:19','2018-06-30 14:22:19'),
	(107,1,'admin/tag/1/edit','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:23:44','2018-06-30 14:23:44'),
	(108,1,'admin/tag/1','PUT','220.188.83.134','{\"name\":\"\\u6691\\u5047\\u9884\\u4e60\",\"_token\":\"JpBWyqgthQfv8acGyrJWNhWVHwjwMI08fnGBRwif\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/tag\"}','2018-06-30 14:23:52','2018-06-30 14:23:52'),
	(109,1,'admin/tag','GET','220.188.83.134','[]','2018-06-30 14:23:53','2018-06-30 14:23:53'),
	(110,1,'admin/tag/create','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:24:31','2018-06-30 14:24:31'),
	(111,1,'admin/tag','POST','220.188.83.134','{\"name\":\"\\u8bfe\\u4ef6\",\"_token\":\"JpBWyqgthQfv8acGyrJWNhWVHwjwMI08fnGBRwif\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/tag\"}','2018-06-30 14:24:36','2018-06-30 14:24:36'),
	(112,1,'admin/tag','GET','220.188.83.134','[]','2018-06-30 14:24:36','2018-06-30 14:24:36'),
	(113,1,'admin/tag/create','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:24:38','2018-06-30 14:24:38'),
	(114,1,'admin/tag','POST','220.188.83.134','{\"name\":\"\\u4f1a\\u5458\",\"_token\":\"JpBWyqgthQfv8acGyrJWNhWVHwjwMI08fnGBRwif\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/tag\"}','2018-06-30 14:24:42','2018-06-30 14:24:42'),
	(115,1,'admin/tag','GET','220.188.83.134','[]','2018-06-30 14:24:42','2018-06-30 14:24:42'),
	(116,1,'admin/topic','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:24:44','2018-06-30 14:24:44'),
	(117,1,'admin/tag','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:25:12','2018-06-30 14:25:12'),
	(118,1,'admin/tag/2/edit','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:25:14','2018-06-30 14:25:14'),
	(119,1,'admin/tag/2','PUT','220.188.83.134','{\"name\":\"PPT\",\"_token\":\"JpBWyqgthQfv8acGyrJWNhWVHwjwMI08fnGBRwif\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/tag\"}','2018-06-30 14:25:24','2018-06-30 14:25:24'),
	(120,1,'admin/tag','GET','220.188.83.134','[]','2018-06-30 14:25:24','2018-06-30 14:25:24'),
	(121,1,'admin','GET','220.188.83.134','[]','2018-06-30 14:26:03','2018-06-30 14:26:03'),
	(122,1,'admin/topic','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:26:12','2018-06-30 14:26:12'),
	(123,1,'admin/category','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:26:33','2018-06-30 14:26:33'),
	(124,1,'admin/course','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:26:37','2018-06-30 14:26:37'),
	(125,1,'admin/config','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:26:38','2018-06-30 14:26:38'),
	(126,1,'admin/tag','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:26:39','2018-06-30 14:26:39'),
	(127,1,'admin/category','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:26:44','2018-06-30 14:26:44'),
	(128,1,'admin/topic','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:33:32','2018-06-30 14:33:32'),
	(129,1,'admin/topic/create','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:33:43','2018-06-30 14:33:43'),
	(130,1,'admin/topic','POST','220.188.83.134','{\"category_id\":\"1\",\"grade_id\":\"2\",\"course_id\":\"1\",\"title\":\"\\u4e4c\\u9e26\\u559d\\u6c34\",\"price\":null,\"teacher_name\":\"\\u5f20\\u8001\\u5e08\",\"description\":\"\\u4e4c\\u9e26\\u559d\\u6c34\\u7684\\u6545\\u4e8b\",\"tags\":[\"1\",\"2\",null],\"status\":\"off\",\"resources\":{\"new_1\":{\"title\":\"\\u4e4c\\u9e26\\u559d\\u6c34\",\"resource_path\":\"http:\\/\\/data.73305.com\\/1\\/rjbyys\\/106.mp4\",\"is_free\":\"on\",\"order\":\"10\",\"id\":null,\"_remove_\":\"0\"}},\"_token\":\"JpBWyqgthQfv8acGyrJWNhWVHwjwMI08fnGBRwif\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/topic\"}','2018-06-30 14:37:32','2018-06-30 14:37:32'),
	(131,1,'admin/topic/create','GET','220.188.83.134','[]','2018-06-30 14:37:33','2018-06-30 14:37:33'),
	(132,1,'admin/topic','POST','220.188.83.134','{\"category_id\":\"1\",\"grade_id\":\"2\",\"course_id\":\"1\",\"title\":\"\\u4e4c\\u9e26\\u559d\\u6c34\",\"price\":\"10.00\",\"teacher_name\":\"\\u5f20\\u8001\\u5e08\",\"description\":\"\\u4e4c\\u9e26\\u559d\\u6c34\\u7684\\u6545\\u4e8b\",\"tags\":[\"1\",\"2\",null],\"status\":\"off\",\"resources\":{\"new_1\":{\"title\":\"\\u4e4c\\u9e26\\u559d\\u6c34\",\"resource_path\":\"http:\\/\\/data.73305.com\\/1\\/rjbyys\\/106.mp4\",\"is_free\":\"on\",\"order\":\"0\",\"id\":null,\"_remove_\":\"0\"}},\"_token\":\"JpBWyqgthQfv8acGyrJWNhWVHwjwMI08fnGBRwif\"}','2018-06-30 14:37:45','2018-06-30 14:37:45'),
	(133,1,'admin/topic/create','GET','220.188.83.134','[]','2018-06-30 14:37:45','2018-06-30 14:37:45'),
	(134,1,'admin/topic','POST','220.188.83.134','{\"category_id\":\"1\",\"grade_id\":\"2\",\"course_id\":\"1\",\"title\":\"\\u4e4c\\u9e26\\u559d\\u6c34\",\"price\":\"10.00\",\"teacher_name\":\"\\u5f20\\u8001\\u5e08\",\"description\":\"\\u4e4c\\u9e26\\u559d\\u6c34\\u7684\\u6545\\u4e8b\",\"tags\":[\"1\",\"2\",null],\"status\":\"off\",\"resources\":{\"new_1\":{\"title\":\"\\u4e4c\\u9e26\\u559d\\u6c34\",\"resource_path\":\"http:\\/\\/data.73305.com\\/1\\/rjbyys\\/106.mp4\",\"is_free\":\"on\",\"order\":\"0\",\"id\":null,\"_remove_\":\"0\"}},\"_token\":\"JpBWyqgthQfv8acGyrJWNhWVHwjwMI08fnGBRwif\"}','2018-06-30 14:37:57','2018-06-30 14:37:57'),
	(135,1,'admin/topic','GET','220.188.83.134','[]','2018-06-30 14:37:57','2018-06-30 14:37:57'),
	(136,1,'admin/topic/1/edit','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:38:04','2018-06-30 14:38:04'),
	(137,1,'admin/topic','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:38:31','2018-06-30 14:38:31'),
	(138,1,'admin/topic/create','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:38:32','2018-06-30 14:38:32'),
	(139,1,'admin/topic','POST','220.188.83.134','{\"category_id\":\"1\",\"grade_id\":\"2\",\"course_id\":\"1\",\"title\":\"\\u72d0\\u72f8\\u517b\\u9e21\",\"price\":null,\"teacher_name\":\"\\u65e0\",\"description\":\"\\u72d0\\u72f8\\u517b\\u9e21 \\u4e0a\\u4e0b\",\"tags\":[\"1\",\"2\",\"3\",null],\"status\":\"off\",\"resources\":{\"new_1\":{\"title\":\"\\u72d0\\u72f8\\u517b\\u9e21 \\u4e0a\",\"resource_path\":\"http:\\/\\/data.73305.com\\/2\\/newrjbyws\\/111.mp4\",\"is_free\":\"off\",\"order\":\"0\",\"id\":null,\"_remove_\":\"0\"},\"new_2\":{\"title\":\"\\u72d0\\u72f8\\u517b\\u9e21 \\u4e0b\",\"resource_path\":\"http:\\/\\/data.73305.com\\/2\\/newrjbyws\\/108.mp4\",\"is_free\":\"off\",\"order\":\"0\",\"id\":null,\"_remove_\":\"0\"}},\"_token\":\"JpBWyqgthQfv8acGyrJWNhWVHwjwMI08fnGBRwif\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/topic\"}','2018-06-30 14:40:31','2018-06-30 14:40:31'),
	(140,1,'admin/topic/create','GET','220.188.83.134','[]','2018-06-30 14:40:31','2018-06-30 14:40:31'),
	(141,1,'admin/topic','POST','220.188.83.134','{\"category_id\":\"1\",\"grade_id\":\"2\",\"course_id\":\"1\",\"title\":\"\\u72d0\\u72f8\\u517b\\u9e21\",\"price\":null,\"teacher_name\":\"\\u65e0\",\"description\":\"\\u72d0\\u72f8\\u517b\\u9e21 \\u4e0a\\u4e0b\",\"tags\":[\"1\",\"2\",\"3\",null],\"status\":\"off\",\"resources\":{\"new_1\":{\"title\":\"\\u72d0\\u72f8\\u517b\\u9e21\",\"resource_path\":\"http:\\/\\/data.73305.com\\/2\\/newrjbyws\\/111.mp4\",\"is_free\":\"off\",\"order\":\"0\",\"id\":null,\"_remove_\":\"0\"},\"new_2\":{\"title\":\"\\u72d0\\u72f8\\u517b\\u9e21\",\"resource_path\":\"http:\\/\\/data.73305.com\\/2\\/newrjbyws\\/108.mp4\",\"is_free\":\"off\",\"order\":\"0\",\"id\":null,\"_remove_\":\"0\"}},\"_token\":\"JpBWyqgthQfv8acGyrJWNhWVHwjwMI08fnGBRwif\"}','2018-06-30 14:40:49','2018-06-30 14:40:49'),
	(142,1,'admin/topic/create','GET','220.188.83.134','[]','2018-06-30 14:40:49','2018-06-30 14:40:49'),
	(143,1,'admin/topic','POST','220.188.83.134','{\"category_id\":\"1\",\"grade_id\":\"2\",\"course_id\":\"1\",\"title\":\"\\u72d0\\u72f8\\u517b\\u9e21\",\"price\":\"10.00\",\"teacher_name\":\"\\u65e0\",\"description\":\"\\u72d0\\u72f8\\u517b\\u9e21 \\u4e0a\\u4e0b\",\"tags\":[\"1\",\"2\",\"3\",null],\"status\":\"off\",\"resources\":{\"new_1\":{\"title\":\"\\u72d0\\u72f8\\u517b\\u9e21\",\"resource_path\":\"http:\\/\\/data.73305.com\\/2\\/newrjbyws\\/111.mp4\",\"is_free\":\"off\",\"order\":\"0\",\"id\":null,\"_remove_\":\"0\"},\"new_2\":{\"title\":\"\\u72d0\\u72f8\\u517b\\u9e21\",\"resource_path\":\"http:\\/\\/data.73305.com\\/2\\/newrjbyws\\/108.mp4\",\"is_free\":\"on\",\"order\":\"0\",\"id\":null,\"_remove_\":\"0\"}},\"_token\":\"JpBWyqgthQfv8acGyrJWNhWVHwjwMI08fnGBRwif\"}','2018-06-30 14:41:14','2018-06-30 14:41:14'),
	(144,1,'admin/topic','GET','220.188.83.134','[]','2018-06-30 14:41:14','2018-06-30 14:41:14'),
	(145,1,'admin/topic/1/edit','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:41:16','2018-06-30 14:41:16'),
	(146,1,'admin/topic','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:41:23','2018-06-30 14:41:23'),
	(147,1,'admin/topic/1/edit','GET','220.188.83.134','[]','2018-06-30 14:42:14','2018-06-30 14:42:14'),
	(148,1,'admin/topic','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:42:17','2018-06-30 14:42:17'),
	(149,1,'admin/topic/create','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:42:19','2018-06-30 14:42:19'),
	(150,1,'admin','GET','220.188.83.134','[]','2018-06-30 14:43:38','2018-06-30 14:43:38'),
	(151,1,'admin/topic','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:43:40','2018-06-30 14:43:40'),
	(152,1,'admin/topic/create','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 14:43:42','2018-06-30 14:43:42'),
	(153,1,'admin/topic','POST','220.188.83.134','{\"category_id\":\"2\",\"grade_id\":\"3\",\"course_id\":\"2\",\"title\":\"\\u4e4c\\u9e26\\u559d\\u6c34\",\"price\":\"5.00\",\"teacher_name\":\"\\u5f20\",\"description\":\"\\u7761\\u524d\\u6545\\u4e8b\\uff1a\\u4e4c\\u9e26\\u559d\\u6c34\\u7684\\u6545\\u4e8b\",\"tags\":[\"3\",null],\"status\":\"off\",\"resources\":{\"new_1\":{\"title\":\"\\u4e4c\\u9e26\\u559d\\u6c34\",\"resource_path\":\"http:\\/\\/wx.51zjedu.com\\/mp3\\/1\\/rjb1\\/38.mp3\",\"is_free\":\"off\",\"order\":\"0\",\"id\":null,\"_remove_\":\"0\"}},\"_token\":\"JpBWyqgthQfv8acGyrJWNhWVHwjwMI08fnGBRwif\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/topic\"}','2018-06-30 14:45:10','2018-06-30 14:45:10'),
	(154,1,'admin/topic','GET','220.188.83.134','[]','2018-06-30 14:45:10','2018-06-30 14:45:10'),
	(155,1,'admin/grade','GET','220.188.83.134','[]','2018-06-30 17:03:52','2018-06-30 17:03:52'),
	(156,1,'admin/course','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 17:03:56','2018-06-30 17:03:56'),
	(157,1,'admin/config','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 17:03:58','2018-06-30 17:03:58'),
	(158,1,'admin/tag','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 17:03:59','2018-06-30 17:03:59'),
	(159,1,'admin/topic','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 17:04:01','2018-06-30 17:04:01'),
	(160,1,'admin/topic','GET','220.188.83.134','{\"_pjax\":\"#pjax-container\"}','2018-06-30 17:05:02','2018-06-30 17:05:02'),
	(161,1,'admin/topic/create','GET','220.188.83.134','[]','2018-06-30 17:23:57','2018-06-30 17:23:57'),
	(162,1,'admin/topic','POST','220.188.83.134','{\"category_id\":\"1\",\"grade_id\":\"2\",\"course_id\":\"3\",\"title\":\"\\u5f69\\u8272\\u7684\\u7fc5\\u8180\",\"price\":\"60.00\",\"teacher_name\":\"\\u9a6c\",\"description\":\"\\u770b\\u56fe\\u5199\\u8bdd\\u662f\\u4f4e\\u5e74\\u7ea7\\u4e00\\u4e2a\\u91cd\\u8981\\u7684\\u8bad\\u7ec3\\u9879\\u76ee\\uff0c\\u662f\\u4f4e\\u5e74\\u7ea7\\u5b66\\u751f\\u8fdb\\u884c\\u5199\\u8bdd\\u8bad\\u7ec3\\u7684\\u91cd\\u8981\\u9014\\u5f84\\u3002\",\"tags\":[\"1\",null],\"status\":\"off\",\"resources\":{\"new_1\":{\"title\":\"\\u5f69\\u8272\\u7684\\u7fc5\\u8180\",\"resource_path\":\"http:\\/\\/data.73305.com\\/6\\/rjbyys\\/27.mp4\",\"is_free\":\"off\",\"order\":\"0\",\"id\":null,\"_remove_\":\"0\"}},\"_token\":\"GDx2q2wSXa1LywMeYrn77ueEbtC83caB8Ce9wGsN\"}','2018-06-30 17:29:48','2018-06-30 17:29:48'),
	(163,1,'admin/topic','GET','220.188.83.134','[]','2018-06-30 17:29:48','2018-06-30 17:29:48'),
	(164,1,'admin','GET','60.182.111.40','[]','2018-07-02 11:41:13','2018-07-02 11:41:13'),
	(165,1,'admin/config','GET','60.182.111.40','{\"_pjax\":\"#pjax-container\"}','2018-07-02 11:41:15','2018-07-02 11:41:15'),
	(166,1,'admin/config/create','GET','60.182.111.40','{\"_pjax\":\"#pjax-container\"}','2018-07-02 11:41:22','2018-07-02 11:41:22'),
	(167,1,'admin/config','POST','60.182.111.40','{\"name\":\"APP_ID\",\"value\":\"wx0bddaf84c8415eef\",\"description\":null,\"_token\":\"gSKGFF91QIH9SkdOr4knikoSYuGifTJDYnZNF7zd\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/config\"}','2018-07-02 11:41:29','2018-07-02 11:41:29'),
	(168,1,'admin/config','GET','60.182.111.40','[]','2018-07-02 11:41:29','2018-07-02 11:41:29'),
	(169,1,'admin/config/create','GET','60.182.111.40','{\"_pjax\":\"#pjax-container\"}','2018-07-02 11:41:33','2018-07-02 11:41:33'),
	(170,1,'admin/config','POST','60.182.111.40','{\"name\":\"APP_SECRET\",\"value\":\"dfbcb3c345cbb6437dbe4b182ceda868\",\"description\":null,\"_token\":\"gSKGFF91QIH9SkdOr4knikoSYuGifTJDYnZNF7zd\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/config\"}','2018-07-02 11:41:36','2018-07-02 11:41:36'),
	(171,1,'admin/config','GET','60.182.111.40','[]','2018-07-02 11:41:36','2018-07-02 11:41:36'),
	(172,1,'admin/act/clean_cache','POST','60.182.111.40','{\"_token\":\"gSKGFF91QIH9SkdOr4knikoSYuGifTJDYnZNF7zd\"}','2018-07-02 11:41:42','2018-07-02 11:41:42'),
	(173,1,'admin','GET','220.188.53.82','[]','2018-07-06 09:15:01','2018-07-06 09:15:01'),
	(174,1,'admin/topic','GET','220.188.53.82','{\"_pjax\":\"#pjax-container\"}','2018-07-06 09:15:05','2018-07-06 09:15:05'),
	(175,1,'admin/topic','GET','220.188.53.82','{\"_pjax\":\"#pjax-container\"}','2018-07-06 09:15:10','2018-07-06 09:15:10'),
	(176,1,'admin','GET','220.188.53.82','{\"_pjax\":\"#pjax-container\"}','2018-07-06 09:15:18','2018-07-06 09:15:18'),
	(177,1,'admin/topic','GET','220.188.53.82','{\"_pjax\":\"#pjax-container\"}','2018-07-06 09:15:19','2018-07-06 09:15:19'),
	(178,1,'admin','GET','220.188.53.82','[]','2018-07-06 09:19:33','2018-07-06 09:19:33'),
	(179,1,'admin/auth/menu','GET','220.188.53.82','{\"_pjax\":\"#pjax-container\"}','2018-07-06 09:19:40','2018-07-06 09:19:40'),
	(180,1,'admin/auth/menu','POST','220.188.53.82','{\"parent_id\":\"11\",\"title\":\"\\u8f6e\\u64ad\\u56fe\",\"icon\":\"fa-image\",\"uri\":\"banner\",\"roles\":[null],\"_token\":\"87IkO5OWRaS50kCDrne1DgI1qpGXltHpIn6LADJs\"}','2018-07-06 09:20:04','2018-07-06 09:20:04'),
	(181,1,'admin/auth/menu','GET','220.188.53.82','[]','2018-07-06 09:20:04','2018-07-06 09:20:04'),
	(182,1,'admin/auth/menu','GET','220.188.53.82','[]','2018-07-06 09:20:09','2018-07-06 09:20:09'),
	(183,1,'admin/banner','GET','220.188.53.82','{\"_pjax\":\"#pjax-container\"}','2018-07-06 09:20:12','2018-07-06 09:20:12'),
	(184,1,'admin/banner/create','GET','220.188.53.82','{\"_pjax\":\"#pjax-container\"}','2018-07-06 09:20:16','2018-07-06 09:20:16'),
	(185,1,'admin/topic','GET','220.188.53.82','[]','2018-07-06 09:20:22','2018-07-06 09:20:22'),
	(186,1,'admin/topic','GET','220.188.53.82','{\"_pjax\":\"#pjax-container\"}','2018-07-06 09:20:24','2018-07-06 09:20:24'),
	(187,1,'admin/topic','GET','220.188.53.82','{\"_pjax\":\"#pjax-container\"}','2018-07-06 09:20:26','2018-07-06 09:20:26'),
	(188,1,'admin/banner','GET','220.188.53.82','{\"_pjax\":\"#pjax-container\"}','2018-07-06 09:20:29','2018-07-06 09:20:29'),
	(189,1,'admin/banner/create','GET','220.188.53.82','{\"_pjax\":\"#pjax-container\"}','2018-07-06 09:20:31','2018-07-06 09:20:31'),
	(190,1,'admin/banner','POST','220.188.53.82','{\"title\":\"1\",\"order\":\"0\",\"_token\":\"d7LQjhPAXpJZeKp60Y5KCbA0OJiKQEJPTdf1hnpF\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/banner\"}','2018-07-06 09:21:22','2018-07-06 09:21:22'),
	(191,1,'admin/banner','GET','220.188.53.82','[]','2018-07-06 09:21:22','2018-07-06 09:21:22'),
	(192,1,'admin/banner/create','GET','220.188.53.82','{\"_pjax\":\"#pjax-container\"}','2018-07-06 09:21:26','2018-07-06 09:21:26'),
	(193,1,'admin/banner','POST','220.188.53.82','{\"title\":\"2\",\"order\":\"0\",\"_token\":\"d7LQjhPAXpJZeKp60Y5KCbA0OJiKQEJPTdf1hnpF\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/banner\"}','2018-07-06 09:21:38','2018-07-06 09:21:38'),
	(194,1,'admin/banner','GET','220.188.53.82','[]','2018-07-06 09:21:39','2018-07-06 09:21:39'),
	(195,1,'admin/category','GET','220.188.53.82','{\"_pjax\":\"#pjax-container\"}','2018-07-06 09:59:25','2018-07-06 09:59:25'),
	(196,1,'admin/category/create','GET','220.188.53.82','{\"_pjax\":\"#pjax-container\"}','2018-07-06 09:59:32','2018-07-06 09:59:32'),
	(197,1,'admin/category','POST','220.188.53.82','{\"name\":\"\\u5168\\u90e8\",\"_token\":\"d7LQjhPAXpJZeKp60Y5KCbA0OJiKQEJPTdf1hnpF\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/category\"}','2018-07-06 10:00:01','2018-07-06 10:00:01'),
	(198,1,'admin/category','GET','220.188.53.82','[]','2018-07-06 10:00:02','2018-07-06 10:00:02'),
	(199,1,'admin/act/clean_cache','POST','220.188.53.82','{\"_token\":\"d7LQjhPAXpJZeKp60Y5KCbA0OJiKQEJPTdf1hnpF\"}','2018-07-06 10:00:11','2018-07-06 10:00:11'),
	(200,1,'admin/banner','GET','220.188.53.82','{\"_pjax\":\"#pjax-container\"}','2018-07-06 11:04:40','2018-07-06 11:04:40'),
	(201,1,'admin/banner/1/edit','GET','220.188.53.82','[]','2018-07-06 14:31:44','2018-07-06 14:31:44'),
	(202,1,'admin/banner/1','PUT','220.188.53.82','{\"title\":\"1\",\"order\":\"0\",\"_token\":\"H3XdisO3uGMemtfTRb02Ist7bTTW46P29kN5CQfI\",\"_method\":\"PUT\"}','2018-07-06 14:31:53','2018-07-06 14:31:53'),
	(203,1,'admin/banner','GET','220.188.53.82','[]','2018-07-06 14:31:54','2018-07-06 14:31:54'),
	(204,1,'admin/banner/2/edit','GET','220.188.53.82','{\"_pjax\":\"#pjax-container\"}','2018-07-06 14:38:01','2018-07-06 14:38:01'),
	(205,1,'admin/banner/2','PUT','220.188.53.82','{\"title\":\"2\",\"order\":\"0\",\"_token\":\"H3XdisO3uGMemtfTRb02Ist7bTTW46P29kN5CQfI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/banner\"}','2018-07-06 14:38:07','2018-07-06 14:38:07'),
	(206,1,'admin/banner','GET','220.188.53.82','[]','2018-07-06 14:38:07','2018-07-06 14:38:07'),
	(207,1,'admin/act/clean_cache','POST','220.188.53.82','{\"_token\":\"H3XdisO3uGMemtfTRb02Ist7bTTW46P29kN5CQfI\"}','2018-07-06 14:38:14','2018-07-06 14:38:14'),
	(208,1,'admin/banner','GET','220.188.53.82','[]','2018-07-06 15:08:09','2018-07-06 15:08:09'),
	(209,1,'admin','GET','125.117.151.217','[]','2018-07-09 09:34:40','2018-07-09 09:34:40'),
	(210,1,'admin/act/clean_cache','POST','125.117.151.217','{\"_token\":\"qJJ2NB8QwPonb8gmkEme3rfL3lQUFKrW0wjggnmB\"}','2018-07-09 09:34:48','2018-07-09 09:34:48'),
	(211,1,'admin/act/clean_cache','POST','125.117.151.217','{\"_token\":\"qJJ2NB8QwPonb8gmkEme3rfL3lQUFKrW0wjggnmB\"}','2018-07-09 09:36:43','2018-07-09 09:36:43'),
	(212,1,'admin/act/clean_cache','POST','125.117.151.217','{\"_token\":\"qJJ2NB8QwPonb8gmkEme3rfL3lQUFKrW0wjggnmB\"}','2018-07-09 11:06:51','2018-07-09 11:06:51'),
	(213,1,'admin/act/clean_cache','POST','125.117.151.217','{\"_token\":\"qJJ2NB8QwPonb8gmkEme3rfL3lQUFKrW0wjggnmB\"}','2018-07-09 11:10:45','2018-07-09 11:10:45'),
	(214,1,'admin','GET','125.117.151.217','[]','2018-07-09 13:54:32','2018-07-09 13:54:32'),
	(215,1,'admin/topic','GET','125.117.151.217','{\"_pjax\":\"#pjax-container\"}','2018-07-09 13:54:36','2018-07-09 13:54:36'),
	(216,1,'admin/topic/2/edit','GET','125.117.151.217','{\"_pjax\":\"#pjax-container\"}','2018-07-09 13:54:50','2018-07-09 13:54:50'),
	(217,1,'admin/topic','GET','125.117.151.217','{\"_pjax\":\"#pjax-container\"}','2018-07-09 13:54:55','2018-07-09 13:54:55'),
	(218,1,'admin/topic/5/edit','GET','125.117.151.217','{\"_pjax\":\"#pjax-container\"}','2018-07-09 13:54:59','2018-07-09 13:54:59'),
	(219,1,'admin/topic','GET','125.117.151.217','{\"_pjax\":\"#pjax-container\"}','2018-07-09 13:55:05','2018-07-09 13:55:05'),
	(220,1,'admin','GET','125.117.151.217','{\"_pjax\":\"#pjax-container\"}','2018-07-09 13:55:05','2018-07-09 13:55:05'),
	(221,1,'admin/topic','GET','125.117.151.217','{\"_pjax\":\"#pjax-container\"}','2018-07-09 13:55:07','2018-07-09 13:55:07'),
	(222,1,'admin/topic/2/edit','GET','125.117.151.217','{\"_pjax\":\"#pjax-container\"}','2018-07-09 13:55:11','2018-07-09 13:55:11'),
	(223,1,'admin/topic/2/edit','GET','125.117.151.217','{\"_pjax\":\"#pjax-container\"}','2018-07-09 13:55:11','2018-07-09 13:55:11'),
	(224,1,'admin/topic/2','PUT','125.117.151.217','{\"category_id\":\"1\",\"grade_id\":\"2\",\"course_id\":\"1\",\"title\":\"\\u4e4c\\u9e26\\u559d\\u6c34\",\"price\":\"10.00\",\"teacher_name\":\"\\u5f20\\u8001\\u5e08\",\"description\":\"\\u4e4c\\u9e26\\u559d\\u6c34\\u7684\\u6545\\u4e8b\",\"tags\":[\"1\",\"2\",null],\"status\":\"off\",\"resources\":{\"2\":{\"title\":\"\\u4e4c\\u9e26\\u559d\\u6c34\",\"resource_path\":\"http:\\/\\/data.73305.com\\/1\\/rjbyys\\/106.mp4\",\"is_free\":\"on\",\"order\":\"0\",\"id\":\"2\",\"_remove_\":\"0\"},\"new_1\":{\"title\":\"\\u4e4c\\u9e26\\u559d\\u6c34 \\u4e0b\",\"resource_path\":\"http:\\/\\/data.73305.com\\/1\\/rjbyys\\/106.mp4\",\"is_free\":\"on\",\"order\":\"0\",\"id\":null,\"_remove_\":\"0\"}},\"_token\":\"jPbUFWROBZziW7f27qWiMMdoX4Ln1N5nvjkq92JI\",\"_method\":\"PUT\"}','2018-07-09 13:55:38','2018-07-09 13:55:38'),
	(225,1,'admin/topic','GET','125.117.151.217','[]','2018-07-09 13:55:38','2018-07-09 13:55:38'),
	(226,1,'admin/topic/5/edit','GET','125.117.151.217','{\"_pjax\":\"#pjax-container\"}','2018-07-09 13:55:42','2018-07-09 13:55:42'),
	(227,1,'admin/topic','GET','125.117.151.217','{\"_pjax\":\"#pjax-container\"}','2018-07-09 13:55:47','2018-07-09 13:55:47'),
	(228,1,'admin/topic/1/edit','GET','125.117.151.217','{\"_pjax\":\"#pjax-container\"}','2018-07-09 13:55:50','2018-07-09 13:55:50'),
	(229,1,'admin/topic','GET','125.117.151.217','{\"_pjax\":\"#pjax-container\"}','2018-07-09 13:55:54','2018-07-09 13:55:54'),
	(230,1,'admin/topic/2/edit','GET','125.117.151.217','{\"_pjax\":\"#pjax-container\"}','2018-07-09 13:55:56','2018-07-09 13:55:56'),
	(231,1,'admin/topic','GET','125.117.151.217','{\"_pjax\":\"#pjax-container\"}','2018-07-09 13:56:02','2018-07-09 13:56:02'),
	(232,1,'admin/topic/3/edit','GET','125.117.151.217','{\"_pjax\":\"#pjax-container\"}','2018-07-09 13:56:04','2018-07-09 13:56:04'),
	(233,1,'admin/topic','GET','125.117.151.217','{\"_pjax\":\"#pjax-container\"}','2018-07-09 13:56:07','2018-07-09 13:56:07'),
	(234,1,'admin/topic/3/edit','GET','125.117.151.217','{\"_pjax\":\"#pjax-container\"}','2018-07-09 13:56:17','2018-07-09 13:56:17'),
	(235,1,'admin/topic/3','PUT','125.117.151.217','{\"category_id\":\"1\",\"grade_id\":\"2\",\"course_id\":\"1\",\"title\":\"\\u72d0\\u72f8\\u517b\\u9e21\",\"price\":\"10.00\",\"teacher_name\":\"\\u65e0\",\"description\":\"\\u72d0\\u72f8\\u517b\\u9e21 \\u4e0a\\u4e0b\",\"tags\":[\"1\",\"2\",\"3\",null],\"status\":\"off\",\"resources\":{\"3\":{\"title\":\"\\u72d0\\u72f8\\u517b\\u9e21\",\"resource_path\":\"http:\\/\\/data.73305.com\\/2\\/newrjbyws\\/111.mp4\",\"is_free\":\"off\",\"order\":\"0\",\"id\":\"3\",\"_remove_\":\"0\"},\"4\":{\"title\":\"\\u72d0\\u72f8\\u517b\\u9e21\",\"resource_path\":\"http:\\/\\/data.73305.com\\/2\\/newrjbyws\\/108.mp4\",\"is_free\":\"on\",\"order\":\"0\",\"id\":\"4\",\"_remove_\":\"0\"}},\"_token\":\"jPbUFWROBZziW7f27qWiMMdoX4Ln1N5nvjkq92JI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/topic\"}','2018-07-09 13:56:36','2018-07-09 13:56:36'),
	(236,1,'admin/topic','GET','125.117.151.217','[]','2018-07-09 13:56:36','2018-07-09 13:56:36'),
	(237,1,'admin/topic/2/edit','GET','125.117.151.217','{\"_pjax\":\"#pjax-container\"}','2018-07-09 13:56:40','2018-07-09 13:56:40'),
	(238,1,'admin/topic/2','PUT','125.117.151.217','{\"category_id\":\"1\",\"grade_id\":\"2\",\"course_id\":\"1\",\"title\":\"\\u4e4c\\u9e26\\u559d\\u6c34\",\"price\":\"10.00\",\"teacher_name\":\"\\u5f20\\u8001\\u5e08\",\"description\":\"\\u4e4c\\u9e26\\u559d\\u6c34\\u7684\\u6545\\u4e8b\",\"tags\":[\"1\",\"2\",null],\"status\":\"off\",\"resources\":{\"2\":{\"title\":\"\\u4e4c\\u9e26\\u559d\\u6c34\",\"resource_path\":\"http:\\/\\/data.73305.com\\/1\\/rjbyys\\/106.mp4\",\"is_free\":\"on\",\"order\":\"0\",\"id\":\"2\",\"_remove_\":\"0\"},\"7\":{\"title\":\"\\u4e4c\\u9e26\\u559d\\u6c34 \\u4e0b\",\"resource_path\":\"http:\\/\\/data.73305.com\\/2\\/newrjbyws\\/111.mp4\",\"is_free\":\"on\",\"order\":\"0\",\"id\":\"7\",\"_remove_\":\"0\"}},\"_token\":\"jPbUFWROBZziW7f27qWiMMdoX4Ln1N5nvjkq92JI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/topic\"}','2018-07-09 13:56:44','2018-07-09 13:56:44'),
	(239,1,'admin/topic','GET','125.117.151.217','[]','2018-07-09 13:56:44','2018-07-09 13:56:44'),
	(240,1,'admin/topic/4/edit','GET','125.117.151.217','{\"_pjax\":\"#pjax-container\"}','2018-07-09 13:57:05','2018-07-09 13:57:05'),
	(241,1,'admin/topic','GET','125.117.151.217','{\"_pjax\":\"#pjax-container\"}','2018-07-09 13:57:11','2018-07-09 13:57:11'),
	(242,1,'admin/topic/1/edit','GET','125.117.151.217','{\"_pjax\":\"#pjax-container\"}','2018-07-09 13:57:15','2018-07-09 13:57:15'),
	(243,1,'admin/topic','GET','125.117.151.217','{\"_pjax\":\"#pjax-container\"}','2018-07-09 13:57:19','2018-07-09 13:57:19'),
	(244,1,'admin/topic/3/edit','GET','125.117.151.217','{\"_pjax\":\"#pjax-container\"}','2018-07-09 13:57:27','2018-07-09 13:57:27'),
	(245,1,'admin/topic/3','PUT','125.117.151.217','{\"category_id\":\"1\",\"grade_id\":\"2\",\"course_id\":\"1\",\"title\":\"\\u72d0\\u72f8\\u517b\\u9e21\",\"price\":\"10.00\",\"teacher_name\":\"\\u65e0\",\"description\":\"\\u72d0\\u72f8\\u517b\\u9e21 \\u4e0a\\u4e0b\",\"tags\":[\"1\",\"2\",\"3\",null],\"status\":\"off\",\"resources\":{\"3\":{\"title\":\"\\u72d0\\u72f8\\u517b\\u9e21\\u4e00\",\"resource_path\":\"http:\\/\\/data.73305.com\\/2\\/newrjbyws\\/111.mp4\",\"is_free\":\"off\",\"order\":\"0\",\"id\":\"3\",\"_remove_\":\"0\"},\"4\":{\"title\":\"\\u72d0\\u72f8\\u517b\\u9e21\\u4e8c\",\"resource_path\":\"http:\\/\\/data.73305.com\\/2\\/newrjbyws\\/108.mp4\",\"is_free\":\"off\",\"order\":\"0\",\"id\":\"4\",\"_remove_\":\"0\"},\"new_1\":{\"title\":\"\\u517b\\u9e21\\u4e09\",\"resource_path\":\"http:\\/\\/data.73305.com\\/1\\/rjbyys\\/108.mp4\",\"is_free\":\"on\",\"order\":\"0\",\"id\":null,\"_remove_\":\"0\"}},\"_token\":\"jPbUFWROBZziW7f27qWiMMdoX4Ln1N5nvjkq92JI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/topic\"}','2018-07-09 13:57:55','2018-07-09 13:57:55'),
	(246,1,'admin/topic','GET','125.117.151.217','[]','2018-07-09 13:57:55','2018-07-09 13:57:55'),
	(247,1,'admin/act/clean_cache','POST','125.117.151.217','{\"_token\":\"jPbUFWROBZziW7f27qWiMMdoX4Ln1N5nvjkq92JI\"}','2018-07-09 13:58:01','2018-07-09 13:58:01'),
	(248,1,'admin/topic/3/edit','GET','125.117.151.217','{\"_pjax\":\"#pjax-container\"}','2018-07-09 13:58:05','2018-07-09 13:58:05'),
	(249,1,'admin/topic/3','PUT','125.117.151.217','{\"category_id\":\"1\",\"grade_id\":\"2\",\"course_id\":\"1\",\"title\":\"\\u72d0\\u72f8\\u517b\\u9e21\",\"price\":\"10.00\",\"teacher_name\":\"\\u65e0\",\"description\":\"\\u72d0\\u72f8\\u517b\\u9e21 \\u4e0a\\u4e0b\",\"tags\":[\"1\",\"2\",\"3\",null],\"status\":\"off\",\"resources\":{\"3\":{\"title\":\"\\u72d0\\u72f8\\u517b\\u9e21\\u4e00\",\"resource_path\":\"http:\\/\\/data.73305.com\\/2\\/newrjbyws\\/111.mp4\",\"is_free\":\"off\",\"order\":\"0\",\"id\":\"3\",\"_remove_\":\"0\"},\"4\":{\"title\":\"\\u72d0\\u72f8\\u517b\\u9e21\\u4e8c\",\"resource_path\":\"http:\\/\\/data.73305.com\\/2\\/newrjbyws\\/108.mp4\",\"is_free\":\"off\",\"order\":\"0\",\"id\":\"4\",\"_remove_\":\"0\"},\"8\":{\"title\":\"\\u517b\\u9e21\\u4e09\",\"resource_path\":\"http:\\/\\/data.73305.com\\/1\\/rjbyys\\/108.mp4\",\"is_free\":\"on\",\"order\":\"0\",\"id\":\"8\",\"_remove_\":\"0\"}},\"_token\":\"jPbUFWROBZziW7f27qWiMMdoX4Ln1N5nvjkq92JI\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/topic\"}','2018-07-09 13:58:10','2018-07-09 13:58:10'),
	(250,1,'admin/topic','GET','125.117.151.217','[]','2018-07-09 13:58:10','2018-07-09 13:58:10'),
	(251,1,'admin/topic/3/edit','GET','125.117.151.217','{\"_pjax\":\"#pjax-container\"}','2018-07-09 13:58:28','2018-07-09 13:58:28'),
	(252,1,'admin/topic','GET','125.117.151.217','{\"_pjax\":\"#pjax-container\"}','2018-07-09 13:58:58','2018-07-09 13:58:58'),
	(253,1,'admin','GET','220.188.49.220','[]','2018-07-20 10:07:46','2018-07-20 10:07:46'),
	(254,1,'admin/config','GET','220.188.49.220','{\"_pjax\":\"#pjax-container\"}','2018-07-20 10:07:55','2018-07-20 10:07:55'),
	(255,1,'admin/config/1/edit','GET','220.188.49.220','{\"_pjax\":\"#pjax-container\"}','2018-07-20 10:07:57','2018-07-20 10:07:57'),
	(256,1,'admin/config/1','PUT','220.188.49.220','{\"value\":\"wxfec1289d29a1ee60\",\"description\":null,\"_token\":\"M1739CaQjcD3yw5c5FLS7kkKPP0yeV034qpl8waN\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/config\"}','2018-07-20 10:08:38','2018-07-20 10:08:38'),
	(257,1,'admin/config','GET','220.188.49.220','[]','2018-07-20 10:08:38','2018-07-20 10:08:38'),
	(258,1,'admin/config/2/edit','GET','220.188.49.220','{\"_pjax\":\"#pjax-container\"}','2018-07-20 10:08:44','2018-07-20 10:08:44'),
	(259,1,'admin/config/2','PUT','220.188.49.220','{\"value\":\"c498bee259c619147bac33720219bfb9\",\"description\":null,\"_token\":\"M1739CaQjcD3yw5c5FLS7kkKPP0yeV034qpl8waN\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/config\"}','2018-07-20 10:08:46','2018-07-20 10:08:46'),
	(260,1,'admin/config','GET','220.188.49.220','[]','2018-07-20 10:08:46','2018-07-20 10:08:46'),
	(261,1,'admin/config/1/edit','GET','220.188.49.220','{\"_pjax\":\"#pjax-container\"}','2018-07-20 10:11:20','2018-07-20 10:11:20'),
	(262,1,'admin/config/1','PUT','220.188.49.220','{\"value\":\"wx0bddaf84c8415eef\",\"description\":null,\"_token\":\"M1739CaQjcD3yw5c5FLS7kkKPP0yeV034qpl8waN\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/config\"}','2018-07-20 10:11:23','2018-07-20 10:11:23'),
	(263,1,'admin/config','GET','220.188.49.220','[]','2018-07-20 10:11:23','2018-07-20 10:11:23'),
	(264,1,'admin/config/2/edit','GET','220.188.49.220','{\"_pjax\":\"#pjax-container\"}','2018-07-20 10:11:31','2018-07-20 10:11:31'),
	(265,1,'admin/config/2','PUT','220.188.49.220','{\"value\":\"dfbcb3c345cbb6437dbe4b182ceda868\",\"description\":\"\\u5bc6\\u94a5c498bee259c619147bac33720219bfb9\",\"_token\":\"M1739CaQjcD3yw5c5FLS7kkKPP0yeV034qpl8waN\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/config\"}','2018-07-20 10:11:48','2018-07-20 10:11:48'),
	(266,1,'admin/config','GET','220.188.49.220','[]','2018-07-20 10:11:48','2018-07-20 10:11:48'),
	(267,1,'admin/config/1/edit','GET','220.188.49.220','{\"_pjax\":\"#pjax-container\"}','2018-07-20 10:11:53','2018-07-20 10:11:53'),
	(268,1,'admin/config/1','PUT','220.188.49.220','{\"value\":\"wx0bddaf84c8415eef\",\"description\":\"wxfec1289d29a1ee60\",\"_token\":\"M1739CaQjcD3yw5c5FLS7kkKPP0yeV034qpl8waN\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/config\"}','2018-07-20 10:11:58','2018-07-20 10:11:58'),
	(269,1,'admin/config','GET','220.188.49.220','[]','2018-07-20 10:11:59','2018-07-20 10:11:59'),
	(270,1,'admin/tag','GET','220.188.49.220','{\"_pjax\":\"#pjax-container\"}','2018-07-20 10:26:02','2018-07-20 10:26:02'),
	(271,1,'admin/config','GET','220.188.49.220','{\"_pjax\":\"#pjax-container\"}','2018-07-20 10:26:03','2018-07-20 10:26:03'),
	(272,1,'admin/course','GET','220.188.49.220','{\"_pjax\":\"#pjax-container\"}','2018-07-20 10:26:04','2018-07-20 10:26:04'),
	(273,1,'admin/grade','GET','220.188.49.220','{\"_pjax\":\"#pjax-container\"}','2018-07-20 10:26:04','2018-07-20 10:26:04'),
	(274,1,'admin/category','GET','220.188.49.220','{\"_pjax\":\"#pjax-container\"}','2018-07-20 10:26:05','2018-07-20 10:26:05'),
	(275,1,'admin/config','GET','220.188.49.220','{\"_pjax\":\"#pjax-container\"}','2018-07-20 10:28:35','2018-07-20 10:28:35'),
	(276,1,'admin/config/1/edit','GET','220.188.49.220','{\"_pjax\":\"#pjax-container\"}','2018-07-20 10:28:37','2018-07-20 10:28:37'),
	(277,1,'admin/config/1','PUT','220.188.49.220','{\"value\":\"wxfec1289d29a1ee60\",\"description\":\"wxfec1289d29a1ee60 wx0bddaf84c8415eef\",\"_token\":\"M1739CaQjcD3yw5c5FLS7kkKPP0yeV034qpl8waN\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/config\"}','2018-07-20 10:28:55','2018-07-20 10:28:55'),
	(278,1,'admin/config','GET','220.188.49.220','[]','2018-07-20 10:28:55','2018-07-20 10:28:55'),
	(279,1,'admin/config/2/edit','GET','220.188.49.220','{\"_pjax\":\"#pjax-container\"}','2018-07-20 10:28:57','2018-07-20 10:28:57'),
	(280,1,'admin/config/2','PUT','220.188.49.220','{\"value\":\"c498bee259c619147bac33720219bfb9\",\"description\":\"\\u5bc6\\u94a5c498bee259c619147bac33720219bfb9 dfbcb3c345cbb6437dbe4b182ceda868\",\"_token\":\"M1739CaQjcD3yw5c5FLS7kkKPP0yeV034qpl8waN\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/diyuan.bnxxjs.cn\\/admin\\/config\"}','2018-07-20 10:29:07','2018-07-20 10:29:07'),
	(281,1,'admin/config','GET','220.188.49.220','[]','2018-07-20 10:29:07','2018-07-20 10:29:07');

/*!40000 ALTER TABLE `bn_admin_operation_log` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table bn_admin_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bn_admin_permissions`;

CREATE TABLE `bn_admin_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `http_method` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `http_path` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_permissions_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `bn_admin_permissions` WRITE;
/*!40000 ALTER TABLE `bn_admin_permissions` DISABLE KEYS */;

INSERT INTO `bn_admin_permissions` (`id`, `name`, `slug`, `http_method`, `http_path`, `created_at`, `updated_at`)
VALUES
	(1,'All permission','*','','*',NULL,NULL),
	(2,'Dashboard','dashboard','GET','/',NULL,NULL),
	(3,'Login','auth.login','','/auth/login\r\n/auth/logout',NULL,NULL),
	(4,'User setting','auth.setting','GET,PUT','/auth/setting',NULL,NULL),
	(5,'Auth management','auth.management','','/auth/roles\r\n/auth/permissions\r\n/auth/menu\r\n/auth/logs',NULL,NULL);

/*!40000 ALTER TABLE `bn_admin_permissions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table bn_admin_role_menu
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bn_admin_role_menu`;

CREATE TABLE `bn_admin_role_menu` (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_menu_role_id_menu_id_index` (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `bn_admin_role_menu` WRITE;
/*!40000 ALTER TABLE `bn_admin_role_menu` DISABLE KEYS */;

INSERT INTO `bn_admin_role_menu` (`role_id`, `menu_id`, `created_at`, `updated_at`)
VALUES
	(1,2,NULL,NULL);

/*!40000 ALTER TABLE `bn_admin_role_menu` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table bn_admin_role_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bn_admin_role_permissions`;

CREATE TABLE `bn_admin_role_permissions` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_permissions_role_id_permission_id_index` (`role_id`,`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `bn_admin_role_permissions` WRITE;
/*!40000 ALTER TABLE `bn_admin_role_permissions` DISABLE KEYS */;

INSERT INTO `bn_admin_role_permissions` (`role_id`, `permission_id`, `created_at`, `updated_at`)
VALUES
	(1,1,NULL,NULL);

/*!40000 ALTER TABLE `bn_admin_role_permissions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table bn_admin_role_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bn_admin_role_users`;

CREATE TABLE `bn_admin_role_users` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_users_role_id_user_id_index` (`role_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `bn_admin_role_users` WRITE;
/*!40000 ALTER TABLE `bn_admin_role_users` DISABLE KEYS */;

INSERT INTO `bn_admin_role_users` (`role_id`, `user_id`, `created_at`, `updated_at`)
VALUES
	(1,1,NULL,NULL);

/*!40000 ALTER TABLE `bn_admin_role_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table bn_admin_roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bn_admin_roles`;

CREATE TABLE `bn_admin_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_roles_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `bn_admin_roles` WRITE;
/*!40000 ALTER TABLE `bn_admin_roles` DISABLE KEYS */;

INSERT INTO `bn_admin_roles` (`id`, `name`, `slug`, `created_at`, `updated_at`)
VALUES
	(1,'Administrator','administrator','2018-06-28 14:02:45','2018-06-28 14:02:45');

/*!40000 ALTER TABLE `bn_admin_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table bn_admin_user_permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bn_admin_user_permissions`;

CREATE TABLE `bn_admin_user_permissions` (
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_user_permissions_user_id_permission_id_index` (`user_id`,`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table bn_admin_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bn_admin_users`;

CREATE TABLE `bn_admin_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_users_username_unique` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `bn_admin_users` WRITE;
/*!40000 ALTER TABLE `bn_admin_users` DISABLE KEYS */;

INSERT INTO `bn_admin_users` (`id`, `username`, `password`, `name`, `avatar`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(1,'admin','$2y$10$DV.AEhQnX31MLivndSRMU.ljBKYCrprG1bobVtDT2S7yoe2IrH8.e','Administrator',NULL,NULL,'2018-06-28 14:02:45','2018-06-28 14:02:45');

/*!40000 ALTER TABLE `bn_admin_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table bn_banners
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bn_banners`;

CREATE TABLE `bn_banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(10) unsigned NOT NULL DEFAULT '0',
  `imgUrl` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `bn_banners` WRITE;
/*!40000 ALTER TABLE `bn_banners` DISABLE KEYS */;

INSERT INTO `bn_banners` (`id`, `title`, `order`, `imgUrl`, `created_at`, `updated_at`)
VALUES
	(1,'1',0,'images/7f14c3b809b3b0f8ab35515790705f05.jpg','2018-07-06 09:21:22','2018-07-06 14:31:53'),
	(2,'2',0,'images/286930224e5d0e34e5b0b0f0035436c5.jpg','2018-07-06 09:21:38','2018-07-06 14:38:07');

/*!40000 ALTER TABLE `bn_banners` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table bn_book_images
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bn_book_images`;

CREATE TABLE `bn_book_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `book_id` int(10) unsigned NOT NULL COMMENT '书本id',
  `imgUrl` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '图片地址',
  `order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `book_images_book_id_foreign` (`book_id`),
  CONSTRAINT `book_images_book_id_foreign` FOREIGN KEY (`book_id`) REFERENCES `bn_books` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table bn_books
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bn_books`;

CREATE TABLE `bn_books` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `grade_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '年级id',
  `course_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '课目id',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '书本标题',
  `thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '书本缩略图',
  `content` text COLLATE utf8mb4_unicode_ci COMMENT '书本介绍',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '描述',
  `sales` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '销售数量',
  `likes` double NOT NULL DEFAULT '0' COMMENT '好评',
  `price` decimal(8,2) NOT NULL DEFAULT '0.00' COMMENT '价格',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '上线状态',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `books_grade_id_index` (`grade_id`),
  KEY `books_course_id_index` (`course_id`),
  KEY `books_status_index` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table bn_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bn_categories`;

CREATE TABLE `bn_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '分类名称',
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '图片',
  `order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `bn_categories` WRITE;
/*!40000 ALTER TABLE `bn_categories` DISABLE KEYS */;

INSERT INTO `bn_categories` (`id`, `name`, `icon`, `order`, `created_at`, `updated_at`)
VALUES
	(1,'视频','images/fdb004e8ec7c82ade80ee44b3a69b75a.png',0,'2018-06-30 14:05:56','2018-06-30 14:05:56'),
	(2,'音频','images/e6c65c41177bb097454b832de4cc47c4.png',0,'2018-06-30 14:12:52','2018-06-30 14:12:52'),
	(3,'电子书','images/136388f1d233743303ff4a6093ee4ca3.png',0,'2018-06-30 14:13:45','2018-06-30 14:13:45'),
	(4,'全部','images/dbec07598963edff7128517f3bc88e92.png',0,'2018-07-06 10:00:02','2018-07-06 10:00:02');

/*!40000 ALTER TABLE `bn_categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table bn_collects
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bn_collects`;

CREATE TABLE `bn_collects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `collectable_id` int(10) unsigned NOT NULL,
  `collectable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `collects_user_id_index` (`user_id`),
  KEY `collects_collectable_id_index` (`collectable_id`),
  CONSTRAINT `collects_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `bn_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `bn_collects` WRITE;
/*!40000 ALTER TABLE `bn_collects` DISABLE KEYS */;

INSERT INTO `bn_collects` (`id`, `user_id`, `collectable_id`, `collectable_type`, `created_at`, `updated_at`)
VALUES
	(2,1,1,'App\\Book','2018-07-06 16:08:22','2018-07-06 16:08:22'),
	(8,1,4,'App\\Topic','2018-07-09 14:54:12','2018-07-09 14:54:12');

/*!40000 ALTER TABLE `bn_collects` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table bn_comments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bn_comments`;

CREATE TABLE `bn_comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `commentable_id` int(10) unsigned NOT NULL,
  `commentable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `comments_user_id_foreign` (`user_id`),
  CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `bn_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table bn_configs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bn_configs`;

CREATE TABLE `bn_configs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `configs_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `bn_configs` WRITE;
/*!40000 ALTER TABLE `bn_configs` DISABLE KEYS */;

INSERT INTO `bn_configs` (`id`, `name`, `value`, `description`, `created_at`, `updated_at`)
VALUES
	(1,'APP_ID','wxfec1289d29a1ee60','wxfec1289d29a1ee60 wx0bddaf84c8415eef','2018-07-02 11:41:29','2018-07-20 10:28:55'),
	(2,'APP_SECRET','c498bee259c619147bac33720219bfb9','密钥c498bee259c619147bac33720219bfb9 dfbcb3c345cbb6437dbe4b182ceda868','2018-07-02 11:41:36','2018-07-20 10:29:07');

/*!40000 ALTER TABLE `bn_configs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table bn_courses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bn_courses`;

CREATE TABLE `bn_courses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `bn_courses` WRITE;
/*!40000 ALTER TABLE `bn_courses` DISABLE KEYS */;

INSERT INTO `bn_courses` (`id`, `name`, `order`, `created_at`, `updated_at`)
VALUES
	(1,'语文',0,'2018-06-30 14:02:43','2018-06-30 14:02:43'),
	(2,'数学',0,'2018-06-30 14:22:09','2018-06-30 14:22:09'),
	(3,'英语',0,'2018-06-30 14:22:15','2018-06-30 14:22:15');

/*!40000 ALTER TABLE `bn_courses` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table bn_grades
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bn_grades`;

CREATE TABLE `bn_grades` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `order` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `bn_grades` WRITE;
/*!40000 ALTER TABLE `bn_grades` DISABLE KEYS */;

INSERT INTO `bn_grades` (`id`, `parent_id`, `order`, `title`, `created_at`, `updated_at`)
VALUES
	(1,0,0,'小学阶段','2018-06-30 14:02:26','2018-06-30 14:02:26'),
	(2,1,0,'一年级','2018-06-30 14:02:34','2018-06-30 14:02:34'),
	(3,1,0,'二年级','2018-06-30 14:14:04','2018-06-30 14:14:04'),
	(4,1,0,'三年级','2018-06-30 14:20:21','2018-06-30 14:20:21'),
	(5,1,0,'四年级','2018-06-30 14:20:28','2018-06-30 14:20:28'),
	(6,1,0,'五年级','2018-06-30 14:20:36','2018-06-30 14:20:36'),
	(7,1,0,'六年级','2018-06-30 14:20:44','2018-06-30 14:20:44');

/*!40000 ALTER TABLE `bn_grades` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table bn_migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bn_migrations`;

CREATE TABLE `bn_migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `bn_migrations` WRITE;
/*!40000 ALTER TABLE `bn_migrations` DISABLE KEYS */;

INSERT INTO `bn_migrations` (`id`, `migration`, `batch`)
VALUES
	(746,'2014_10_12_000000_create_users_table',1),
	(747,'2014_10_12_100000_create_password_resets_table',1),
	(748,'2018_06_28_141906_create_categories_table',1),
	(749,'2018_06_28_142325_create_grades_table',1),
	(750,'2018_06_28_142721_create_courses_table',1),
	(751,'2018_06_28_142901_create_topics_table',1),
	(752,'2018_06_28_144544_create_topic_tags_table',1),
	(753,'2018_06_28_144753_create_tags_table',1),
	(754,'2018_06_28_144852_create_banners_table',1),
	(755,'2018_06_28_145133_create_books_table',1),
	(756,'2018_06_28_151203_create_resources_table',1),
	(757,'2018_06_28_153018_create_book_images_table',1),
	(758,'2018_06_28_153335_create_comments_table',1),
	(759,'2018_06_28_155035_create_addresses_table',1),
	(760,'2018_06_28_155752_create_orders_table',1),
	(761,'2018_06_28_162200_create_accounts_table',1),
	(762,'2018_06_28_163341_create_withdraws_table',1),
	(763,'2018_06_28_163709_create_msg_codes_table',1),
	(764,'2018_06_29_181755_create_configs_table',1);

/*!40000 ALTER TABLE `bn_migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table bn_msg_codes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bn_msg_codes`;

CREATE TABLE `bn_msg_codes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `code` int(11) NOT NULL DEFAULT '0',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '发送短信回执信息',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `bn_msg_codes` WRITE;
/*!40000 ALTER TABLE `bn_msg_codes` DISABLE KEYS */;

INSERT INTO `bn_msg_codes` (`id`, `user_id`, `code`, `description`, `created_at`, `updated_at`)
VALUES
	(1,0,926655,'','2018-07-05 20:37:07','2018-07-05 20:37:07'),
	(2,0,590758,'','2018-07-05 20:47:14','2018-07-05 20:47:14'),
	(3,0,141810,'','2018-07-05 20:49:09','2018-07-05 20:49:09'),
	(4,0,686658,'','2018-07-05 21:46:36','2018-07-05 21:46:36'),
	(5,0,829959,'','2018-07-06 14:39:39','2018-07-06 14:39:39');

/*!40000 ALTER TABLE `bn_msg_codes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table bn_orders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bn_orders`;

CREATE TABLE `bn_orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户id',
  `order_sn` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '订单编号',
  `consignee` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '收货人',
  `mobile` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '联系号码',
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '详细地址',
  `total_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '订单总价格',
  `payment_method` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '支付方式',
  `is_paid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '是否支付成功0未支付，1支付成功，2支付失败，3取消订单',
  `ship_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '货运状态0未发货，1发货中，2已确认',
  `paid_at` timestamp NULL DEFAULT NULL COMMENT '支付时间',
  `ship_at` timestamp NULL DEFAULT NULL COMMENT '发货时间',
  `success_at` timestamp NULL DEFAULT NULL COMMENT '确认收获时间',
  `is_comment` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否评价',
  `orderable_id` int(11) NOT NULL,
  `orderable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` int(11) NOT NULL DEFAULT '0' COMMENT '件数',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `orders_order_sn_unique` (`order_sn`),
  KEY `orders_user_id_foreign` (`user_id`),
  CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `bn_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table bn_password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bn_password_resets`;

CREATE TABLE `bn_password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table bn_resources
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bn_resources`;

CREATE TABLE `bn_resources` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `topic_id` int(10) unsigned NOT NULL COMMENT '文章id',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '文章标题',
  `resource_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '资源地址',
  `is_free` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '是否收费',
  `read_counts` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '阅读数',
  `order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `resources_topic_id_foreign` (`topic_id`),
  CONSTRAINT `resources_topic_id_foreign` FOREIGN KEY (`topic_id`) REFERENCES `bn_topics` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `bn_resources` WRITE;
/*!40000 ALTER TABLE `bn_resources` DISABLE KEYS */;

INSERT INTO `bn_resources` (`id`, `topic_id`, `title`, `resource_path`, `is_free`, `read_counts`, `order`, `created_at`, `updated_at`)
VALUES
	(1,1,'小蜗牛','http://data.73305.com/1/rjbyys/108.mp4',0,0,0,'2018-06-30 14:11:44','2018-06-30 14:11:44'),
	(2,2,'乌鸦喝水','http://data.73305.com/1/rjbyys/106.mp4',1,0,0,'2018-06-30 14:37:57','2018-06-30 14:37:57'),
	(3,3,'狐狸养鸡一','http://data.73305.com/2/newrjbyws/111.mp4',0,0,0,'2018-06-30 14:41:14','2018-07-09 13:57:55'),
	(4,3,'狐狸养鸡二','http://data.73305.com/2/newrjbyws/108.mp4',0,0,0,'2018-06-30 14:41:14','2018-07-09 13:57:55'),
	(5,4,'乌鸦喝水','http://wx.51zjedu.com/mp3/1/rjb1/38.mp3',0,0,0,'2018-06-30 14:45:10','2018-06-30 14:45:10'),
	(6,5,'彩色的翅膀','http://data.73305.com/6/rjbyys/27.mp4',0,0,0,'2018-06-30 17:29:48','2018-06-30 17:29:48'),
	(7,2,'乌鸦喝水 下','http://data.73305.com/2/newrjbyws/111.mp4',1,0,0,'2018-07-09 13:55:38','2018-07-09 13:56:44'),
	(8,3,'养鸡三','http://data.73305.com/1/rjbyys/108.mp4',1,0,0,'2018-07-09 13:57:55','2018-07-09 13:57:55');

/*!40000 ALTER TABLE `bn_resources` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table bn_tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bn_tags`;

CREATE TABLE `bn_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `bn_tags` WRITE;
/*!40000 ALTER TABLE `bn_tags` DISABLE KEYS */;

INSERT INTO `bn_tags` (`id`, `name`, `created_at`, `updated_at`)
VALUES
	(1,'暑假预习','2018-06-30 14:02:56','2018-06-30 14:23:52'),
	(2,'PPT','2018-06-30 14:24:36','2018-06-30 14:25:24'),
	(3,'会员','2018-06-30 14:24:42','2018-06-30 14:24:42');

/*!40000 ALTER TABLE `bn_tags` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table bn_topic_tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bn_topic_tags`;

CREATE TABLE `bn_topic_tags` (
  `topic_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文章id',
  `tag_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '标签id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `bn_topic_tags` WRITE;
/*!40000 ALTER TABLE `bn_topic_tags` DISABLE KEYS */;

INSERT INTO `bn_topic_tags` (`topic_id`, `tag_id`)
VALUES
	(1,1),
	(2,1),
	(2,2),
	(3,1),
	(3,2),
	(3,3),
	(4,3),
	(5,1);

/*!40000 ALTER TABLE `bn_topic_tags` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table bn_topics
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bn_topics`;

CREATE TABLE `bn_topics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '分类id',
  `grade_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '年级id',
  `course_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '课目id',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '标题',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '描述',
  `teacher_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '教师名称',
  `thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '缩略图',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '价格',
  `read_counts` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '阅读数',
  `resource_type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '资源类型',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否有效0有效，1下架',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `topics_category_id_index` (`category_id`),
  KEY `topics_grade_id_index` (`grade_id`),
  KEY `topics_course_id_index` (`course_id`),
  KEY `topics_status_index` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `bn_topics` WRITE;
/*!40000 ALTER TABLE `bn_topics` DISABLE KEYS */;

INSERT INTO `bn_topics` (`id`, `category_id`, `grade_id`, `course_id`, `title`, `description`, `teacher_name`, `thumbnail`, `price`, `read_counts`, `resource_type`, `status`, `created_at`, `updated_at`)
VALUES
	(1,1,2,1,'一年级上册英语易混淆句子总结','fsdfsdfds','sss','images/16772b8973d0da10bf34478d1ce7dce1.png',10.00,0,1,0,'2018-06-30 14:11:44','2018-06-30 14:11:44'),
	(2,1,2,1,'乌鸦喝水','乌鸦喝水的故事','张老师','images/35ba7af44f8eeab85765b96d5a9ae596.jpg',10.00,0,1,0,'2018-06-30 14:37:57','2018-07-09 13:56:44'),
	(3,1,2,1,'狐狸养鸡','狐狸养鸡 上下','无','images/b6ed94f6f4f3df437b77c3ae23964d20.jpg',10.00,0,1,0,'2018-06-30 14:41:14','2018-07-09 13:58:10'),
	(4,2,3,2,'乌鸦喝水','睡前故事：乌鸦喝水的故事','张','images/9fea1e4af648da92bf3f695e1cfc0f54.jpg',5.00,0,2,0,'2018-06-30 14:45:10','2018-06-30 14:45:10'),
	(5,1,2,3,'彩色的翅膀','看图写话是低年级一个重要的训练项目，是低年级学生进行写话训练的重要途径。','马','images/ccefc9ddd5899aecc3ee853794758eb9.png',60.00,0,1,0,'2018-06-30 17:29:48','2018-06-30 17:29:48');

/*!40000 ALTER TABLE `bn_topics` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table bn_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bn_users`;

CREATE TABLE `bn_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '推荐人id',
  `openId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'openid',
  `nickName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '昵称',
  `realName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '真实姓名',
  `avatarUrl` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '头像',
  `sex` tinyint(3) unsigned NOT NULL COMMENT '性别',
  `mobile` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '手机号码',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '密码',
  `money` decimal(10,2) NOT NULL COMMENT '余额',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_openid_unique` (`openId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `bn_users` WRITE;
/*!40000 ALTER TABLE `bn_users` DISABLE KEYS */;

INSERT INTO `bn_users` (`id`, `pid`, `openId`, `nickName`, `realName`, `avatarUrl`, `sex`, `mobile`, `password`, `money`, `created_at`, `updated_at`)
VALUES
	(1,0,'ofB4esxXi_KdVqzI7O4t3dkSLKlA','陈江','','http://thirdwx.qlogo.cn/mmopen/vi_32/ajNVdqHZLLBfZ5XicNG3O5q3U7kQFo0NzIH16rsjoqib4YFiaPibYemvaHLkS7h9FiadAEUOrKgDd38OchrHmSEFYgw/132',1,'','',0.00,'2018-07-02 12:45:35','2018-07-02 12:45:35'),
	(2,0,'ofB4es9qHSWC_Q81F09Sq2xgpBVQ','Yuto','','http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJkDroJG2O4RlXkrIvv7149QBQA9tdibE46leZElWJLzQDG19B342m72HVfKWnI7ne8MJkbrzCJPWg/132',1,'','',0.00,'2018-07-02 17:03:53','2018-07-02 17:03:53'),
	(3,0,'lnk81ktduoj5kk2gnlj1paur89','18767999952','18767999952','',0,'18767999952','$2y$10$KEN1/YpdBCpGU/EpS.BC9.s/aFKr6ezgU7pT2RC0B5G07AmsZRZKy',0.00,'2018-07-05 20:42:13','2018-07-05 20:42:13'),
	(4,0,'819oi3bu47fg8ska2agalinqcf','18767999953','18767999953','',0,'18767999953','$2y$10$gkhaz3uxBcJKE8ukQNrbeu6/kZC5t9uYJ90HmGTf4OUhcUgmnbsMe',0.00,'2018-07-05 20:49:23','2018-07-05 20:49:23'),
	(5,0,'utlin6j828to6j8kqh7qk6m0fh','13575680107','13575680107','',0,'13575680107','$2y$10$mMhlscqXonsd1hAHBe9trucU9RKIIgKXxneR9Su6YBR7LbZaQg8qy',0.00,'2018-07-06 14:39:54','2018-07-06 14:39:54');

/*!40000 ALTER TABLE `bn_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table bn_withdraws
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bn_withdraws`;

CREATE TABLE `bn_withdraws` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `money` decimal(10,2) NOT NULL,
  `bank` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
