/*
MySQL Data Transfer
Source Host: 192.168.10.10
Source Database: diyuan
Target Host: 192.168.10.10
Target Database: diyuan
Date: 2018/6/29 ������ ���� 4:55:52
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for bn_accounts
-- ----------------------------
DROP TABLE IF EXISTS `bn_accounts`;
CREATE TABLE `bn_accounts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户id',
  `money` decimal(10,2) NOT NULL COMMENT '交易金额',
  `current_money` decimal(10,2) NOT NULL COMMENT '当前结余',
  `change_type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '改变类型0支付1收入',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '描述',
  `type` tinyint(3) unsigned NOT NULL COMMENT '资金类型0支付，1提现，2退回',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `accounts_user_id_foreign` (`user_id`),
  CONSTRAINT `accounts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `bn_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for bn_addresses
-- ----------------------------
DROP TABLE IF EXISTS `bn_addresses`;
CREATE TABLE `bn_addresses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户id',
  `consignee` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '收货人',
  `mobile` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '联系号码',
  `province` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '省份',
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '城市',
  `area` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '区',
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '详细地址',
  `is_default` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否默认',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `addresses_user_id_foreign` (`user_id`),
  KEY `addresses_is_default_index` (`is_default`),
  CONSTRAINT `addresses_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `bn_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for bn_admin_menu
-- ----------------------------
DROP TABLE IF EXISTS `bn_admin_menu`;
CREATE TABLE `bn_admin_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uri` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for bn_admin_operation_log
-- ----------------------------
DROP TABLE IF EXISTS `bn_admin_operation_log`;
CREATE TABLE `bn_admin_operation_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `input` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_operation_log_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for bn_admin_permissions
-- ----------------------------
DROP TABLE IF EXISTS `bn_admin_permissions`;
CREATE TABLE `bn_admin_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `http_method` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `http_path` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for bn_admin_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `bn_admin_role_menu`;
CREATE TABLE `bn_admin_role_menu` (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_menu_role_id_menu_id_index` (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for bn_admin_role_permissions
-- ----------------------------
DROP TABLE IF EXISTS `bn_admin_role_permissions`;
CREATE TABLE `bn_admin_role_permissions` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_permissions_role_id_permission_id_index` (`role_id`,`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for bn_admin_role_users
-- ----------------------------
DROP TABLE IF EXISTS `bn_admin_role_users`;
CREATE TABLE `bn_admin_role_users` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_users_role_id_user_id_index` (`role_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for bn_admin_roles
-- ----------------------------
DROP TABLE IF EXISTS `bn_admin_roles`;
CREATE TABLE `bn_admin_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for bn_admin_user_permissions
-- ----------------------------
DROP TABLE IF EXISTS `bn_admin_user_permissions`;
CREATE TABLE `bn_admin_user_permissions` (
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_user_permissions_user_id_permission_id_index` (`user_id`,`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for bn_admin_users
-- ----------------------------
DROP TABLE IF EXISTS `bn_admin_users`;
CREATE TABLE `bn_admin_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_users_username_unique` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for bn_banners
-- ----------------------------
DROP TABLE IF EXISTS `bn_banners`;
CREATE TABLE `bn_banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(10) unsigned NOT NULL DEFAULT '0',
  `imgUrl` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for bn_book_images
-- ----------------------------
DROP TABLE IF EXISTS `bn_book_images`;
CREATE TABLE `bn_book_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `book_id` int(10) unsigned NOT NULL COMMENT '书本id',
  `imgUrl` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '图片地址',
  `order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `book_images_book_id_foreign` (`book_id`),
  CONSTRAINT `book_images_book_id_foreign` FOREIGN KEY (`book_id`) REFERENCES `bn_books` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for bn_books
-- ----------------------------
DROP TABLE IF EXISTS `bn_books`;
CREATE TABLE `bn_books` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `grade_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '年级id',
  `course_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '课目id',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '书本标题',
  `thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '书本缩略图',
  `content` text COLLATE utf8mb4_unicode_ci COMMENT '书本介绍',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '描述',
  `sales` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '销售数量',
  `likes` double NOT NULL DEFAULT '0' COMMENT '好评',
  `price` decimal(8,2) NOT NULL DEFAULT '0.00' COMMENT '价格',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '上线状态',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `books_grade_id_index` (`grade_id`),
  KEY `books_course_id_index` (`course_id`),
  KEY `books_status_index` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for bn_categories
-- ----------------------------
DROP TABLE IF EXISTS `bn_categories`;
CREATE TABLE `bn_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '分类名称',
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '图片',
  `order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for bn_comments
-- ----------------------------
DROP TABLE IF EXISTS `bn_comments`;
CREATE TABLE `bn_comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `commentable_id` int(10) unsigned NOT NULL,
  `commentable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `comments_user_id_foreign` (`user_id`),
  CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `bn_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for bn_courses
-- ----------------------------
DROP TABLE IF EXISTS `bn_courses`;
CREATE TABLE `bn_courses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for bn_grades
-- ----------------------------
DROP TABLE IF EXISTS `bn_grades`;
CREATE TABLE `bn_grades` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `order` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for bn_migrations
-- ----------------------------
DROP TABLE IF EXISTS `bn_migrations`;
CREATE TABLE `bn_migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=727 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for bn_msg_codes
-- ----------------------------
DROP TABLE IF EXISTS `bn_msg_codes`;
CREATE TABLE `bn_msg_codes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `code` int(11) NOT NULL DEFAULT '0',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '发送短信回执信息',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for bn_orders
-- ----------------------------
DROP TABLE IF EXISTS `bn_orders`;
CREATE TABLE `bn_orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT '用户id',
  `order_sn` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '订单编号',
  `consignee` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '收货人',
  `mobile` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '联系号码',
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '详细地址',
  `total_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '订单总价格',
  `payment_method` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '支付方式',
  `is_paid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '是否支付成功0未支付，1支付成功，2支付失败，3取消订单',
  `ship_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '货运状态0未发货，1发货中，2已确认',
  `paid_at` timestamp NULL DEFAULT NULL COMMENT '支付时间',
  `ship_at` timestamp NULL DEFAULT NULL COMMENT '发货时间',
  `success_at` timestamp NULL DEFAULT NULL COMMENT '确认收获时间',
  `is_comment` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否评价',
  `orderable_id` int(11) NOT NULL,
  `orderable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` int(11) NOT NULL DEFAULT '0' COMMENT '件数',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `orders_order_sn_unique` (`order_sn`),
  KEY `orders_user_id_foreign` (`user_id`),
  CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `bn_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for bn_resources
-- ----------------------------
DROP TABLE IF EXISTS `bn_resources`;
CREATE TABLE `bn_resources` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `topic_id` int(10) unsigned NOT NULL COMMENT '文章id',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '文章标题',
  `resource_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '资源地址',
  `is_free` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '是否收费',
  `read_counts` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '阅读数',
  `order` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `resources_topic_id_foreign` (`topic_id`),
  CONSTRAINT `resources_topic_id_foreign` FOREIGN KEY (`topic_id`) REFERENCES `bn_topics` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for bn_tags
-- ----------------------------
DROP TABLE IF EXISTS `bn_tags`;
CREATE TABLE `bn_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for bn_topic_tags
-- ----------------------------
DROP TABLE IF EXISTS `bn_topic_tags`;
CREATE TABLE `bn_topic_tags` (
  `topic_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文章id',
  `tag_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '标签id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for bn_topics
-- ----------------------------
DROP TABLE IF EXISTS `bn_topics`;
CREATE TABLE `bn_topics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '分类id',
  `grade_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '年级id',
  `course_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '课目id',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '标题',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '描述',
  `teacher_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '教师名称',
  `thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '缩略图',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '价格',
  `read_counts` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '阅读数',
  `resource_type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '资源类型',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否有效0有效，1下架',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `topics_category_id_index` (`category_id`),
  KEY `topics_grade_id_index` (`grade_id`),
  KEY `topics_course_id_index` (`course_id`),
  KEY `topics_status_index` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for bn_users
-- ----------------------------
DROP TABLE IF EXISTS `bn_users`;
CREATE TABLE `bn_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '推荐人id',
  `openId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'openid',
  `nickName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '昵称',
  `realName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '真实姓名',
  `avatarUrl` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '头像',
  `sex` tinyint(3) unsigned NOT NULL COMMENT '性别',
  `mobile` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '手机号码',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '密码',
  `money` decimal(10,2) NOT NULL COMMENT '余额',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_openid_unique` (`openId`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Table structure for bn_withdraws
-- ----------------------------
DROP TABLE IF EXISTS `bn_withdraws`;
CREATE TABLE `bn_withdraws` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `money` decimal(10,2) NOT NULL,
  `bank` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records 
-- ----------------------------
INSERT INTO `bn_admin_menu` VALUES ('1', '0', '1', 'Index', 'fa-bar-chart', '/', null, null);
INSERT INTO `bn_admin_menu` VALUES ('2', '0', '2', 'Admin', 'fa-tasks', '', null, null);
INSERT INTO `bn_admin_menu` VALUES ('3', '2', '3', 'Users', 'fa-users', 'auth/users', null, null);
INSERT INTO `bn_admin_menu` VALUES ('4', '2', '4', 'Roles', 'fa-user', 'auth/roles', null, null);
INSERT INTO `bn_admin_menu` VALUES ('5', '2', '5', 'Permission', 'fa-ban', 'auth/permissions', null, null);
INSERT INTO `bn_admin_menu` VALUES ('6', '2', '6', 'Menu', 'fa-bars', 'auth/menu', null, null);
INSERT INTO `bn_admin_menu` VALUES ('7', '2', '7', 'Operation log', 'fa-history', 'auth/logs', null, null);
INSERT INTO `bn_admin_operation_log` VALUES ('1', '1', 'admin', 'GET', '192.168.10.1', '[]', '2018-06-29 10:38:45', '2018-06-29 10:38:45');
INSERT INTO `bn_admin_operation_log` VALUES ('2', '1', 'admin', 'GET', '192.168.10.1', '[]', '2018-06-29 10:40:13', '2018-06-29 10:40:13');
INSERT INTO `bn_admin_operation_log` VALUES ('3', '1', 'admin/topic', 'GET', '192.168.10.1', '[]', '2018-06-29 10:40:18', '2018-06-29 10:40:18');
INSERT INTO `bn_admin_operation_log` VALUES ('4', '1', 'admin/topic/1', 'PUT', '192.168.10.1', '{\"name\":\"title\",\"value\":\"tes12t1\",\"pk\":\"1\",\"_token\":\"6ZDe7SBIv8Uoz1o2uNCSOyWMnbwE4401ZKRknI2r\",\"_editable\":\"1\",\"_method\":\"PUT\"}', '2018-06-29 10:40:25', '2018-06-29 10:40:25');
INSERT INTO `bn_admin_operation_log` VALUES ('5', '1', 'admin/topic', 'GET', '192.168.10.1', '[]', '2018-06-29 10:41:05', '2018-06-29 10:41:05');
INSERT INTO `bn_admin_operation_log` VALUES ('6', '1', 'admin/topic/1', 'PUT', '192.168.10.1', '{\"name\":\"title\",\"value\":\"tes12t1\",\"pk\":\"1\",\"_token\":\"6ZDe7SBIv8Uoz1o2uNCSOyWMnbwE4401ZKRknI2r\",\"_editable\":\"1\",\"_method\":\"PUT\"}', '2018-06-29 10:41:11', '2018-06-29 10:41:11');
INSERT INTO `bn_admin_operation_log` VALUES ('7', '1', 'admin/topic', 'GET', '192.168.10.1', '[]', '2018-06-29 10:41:13', '2018-06-29 10:41:13');
INSERT INTO `bn_admin_operation_log` VALUES ('8', '1', 'admin/topic', 'GET', '192.168.10.1', '[]', '2018-06-29 10:41:30', '2018-06-29 10:41:30');
INSERT INTO `bn_admin_operation_log` VALUES ('9', '1', 'admin/topic/1', 'PUT', '192.168.10.1', '{\"name\":\"title\",\"value\":\"tes12t1\",\"pk\":\"1\",\"_token\":\"6ZDe7SBIv8Uoz1o2uNCSOyWMnbwE4401ZKRknI2r\",\"_editable\":\"1\",\"_method\":\"PUT\"}', '2018-06-29 10:41:33', '2018-06-29 10:41:33');
INSERT INTO `bn_admin_operation_log` VALUES ('10', '1', 'admin/topic', 'GET', '192.168.10.1', '[]', '2018-06-29 10:41:35', '2018-06-29 10:41:35');
INSERT INTO `bn_admin_operation_log` VALUES ('11', '1', 'admin/topic/1', 'PUT', '192.168.10.1', '{\"name\":\"title\",\"value\":\"tes12t12\",\"pk\":\"1\",\"_token\":\"6ZDe7SBIv8Uoz1o2uNCSOyWMnbwE4401ZKRknI2r\",\"_editable\":\"1\",\"_method\":\"PUT\"}', '2018-06-29 10:43:00', '2018-06-29 10:43:00');
INSERT INTO `bn_admin_operation_log` VALUES ('12', '1', 'admin/topic/1', 'PUT', '192.168.10.1', '{\"name\":\"title\",\"value\":\"tes12t121\",\"pk\":\"1\",\"_token\":\"6ZDe7SBIv8Uoz1o2uNCSOyWMnbwE4401ZKRknI2r\",\"_editable\":\"1\",\"_method\":\"PUT\"}', '2018-06-29 10:43:31', '2018-06-29 10:43:31');
INSERT INTO `bn_admin_operation_log` VALUES ('13', '1', 'admin/topic', 'GET', '192.168.10.1', '[]', '2018-06-29 13:26:54', '2018-06-29 13:26:54');
INSERT INTO `bn_admin_operation_log` VALUES ('14', '1', 'admin/topic/1', 'PUT', '192.168.10.1', '{\"name\":\"title\",\"value\":\"tes\",\"pk\":\"1\",\"_token\":\"lqJnbXwlRcTIHLgnDQJYiagu9Gfb6pV2Rtd5MzZr\",\"_editable\":\"1\",\"_method\":\"PUT\"}', '2018-06-29 13:27:00', '2018-06-29 13:27:00');
INSERT INTO `bn_admin_operation_log` VALUES ('15', '1', 'admin/topic/1', 'PUT', '192.168.10.1', '{\"name\":\"title\",\"value\":\"test\",\"pk\":\"1\",\"_token\":\"lqJnbXwlRcTIHLgnDQJYiagu9Gfb6pV2Rtd5MzZr\",\"_editable\":\"1\",\"_method\":\"PUT\"}', '2018-06-29 13:35:28', '2018-06-29 13:35:28');
INSERT INTO `bn_admin_permissions` VALUES ('1', 'All permission', '*', '', '*', null, null);
INSERT INTO `bn_admin_permissions` VALUES ('2', 'Dashboard', 'dashboard', 'GET', '/', null, null);
INSERT INTO `bn_admin_permissions` VALUES ('3', 'Login', 'auth.login', '', '/auth/login\r\n/auth/logout', null, null);
INSERT INTO `bn_admin_permissions` VALUES ('4', 'User setting', 'auth.setting', 'GET,PUT', '/auth/setting', null, null);
INSERT INTO `bn_admin_permissions` VALUES ('5', 'Auth management', 'auth.management', '', '/auth/roles\r\n/auth/permissions\r\n/auth/menu\r\n/auth/logs', null, null);
INSERT INTO `bn_admin_role_menu` VALUES ('1', '2', null, null);
INSERT INTO `bn_admin_role_permissions` VALUES ('1', '1', null, null);
INSERT INTO `bn_admin_role_users` VALUES ('1', '1', null, null);
INSERT INTO `bn_admin_roles` VALUES ('1', 'Administrator', 'administrator', '2018-06-28 14:02:45', '2018-06-28 14:02:45');
INSERT INTO `bn_admin_users` VALUES ('1', 'admin', '$2y$10$DV.AEhQnX31MLivndSRMU.ljBKYCrprG1bobVtDT2S7yoe2IrH8.e', 'Administrator', null, null, '2018-06-28 14:02:45', '2018-06-28 14:02:45');
INSERT INTO `bn_banners` VALUES ('1', 'Margot Heathcote IV', '0', 'ErCn2UHvAI', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_banners` VALUES ('2', 'Norbert Feil', '0', 'zSHT96mZmT', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_banners` VALUES ('3', 'Chasity Braun PhD', '0', 'YVpGEj5AO0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_categories` VALUES ('1', 'Justice Johns', 'GtbFgSLtB4', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_categories` VALUES ('2', 'Gay Schmitt', 'o7EEpLsYDf', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_categories` VALUES ('3', 'Mr. Waldo Gutkowski V', 'pKb75TZtqs', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_categories` VALUES ('4', 'Miss Dariana Waelchi', 'LdSgSiCPTC', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_categories` VALUES ('5', 'Carlo Denesik', 'jLpW7Uf8vb', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_courses` VALUES ('1', 'Reid Ernser', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_courses` VALUES ('2', 'Brisa Lueilwitz', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_courses` VALUES ('3', 'Jabari Ratke', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_courses` VALUES ('4', 'Joshua Schmitt', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_courses` VALUES ('5', 'Amie Blick', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_grades` VALUES ('1', '0', '0', 'jL7Tn7Dk5h', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_grades` VALUES ('2', '1', '0', 'eJJ5WEBFKa', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_grades` VALUES ('3', '1', '0', '5ADScg6MGT', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_grades` VALUES ('4', '1', '0', 'OAF7v2r1QF', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_grades` VALUES ('5', '1', '0', 'QfXPn3ZFyD', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_grades` VALUES ('6', '1', '0', 'pdl8FQNVp3', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_grades` VALUES ('7', '1', '0', 'QD8jAsk4mR', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_migrations` VALUES ('710', '2014_10_12_000000_create_users_table', '1');
INSERT INTO `bn_migrations` VALUES ('711', '2018_06_28_141906_create_categories_table', '1');
INSERT INTO `bn_migrations` VALUES ('712', '2018_06_28_142325_create_grades_table', '1');
INSERT INTO `bn_migrations` VALUES ('713', '2018_06_28_142721_create_courses_table', '1');
INSERT INTO `bn_migrations` VALUES ('714', '2018_06_28_142901_create_topics_table', '1');
INSERT INTO `bn_migrations` VALUES ('715', '2018_06_28_144544_create_topic_tags_table', '1');
INSERT INTO `bn_migrations` VALUES ('716', '2018_06_28_144753_create_tags_table', '1');
INSERT INTO `bn_migrations` VALUES ('717', '2018_06_28_144852_create_banners_table', '1');
INSERT INTO `bn_migrations` VALUES ('718', '2018_06_28_145133_create_books_table', '1');
INSERT INTO `bn_migrations` VALUES ('719', '2018_06_28_151203_create_resources_table', '1');
INSERT INTO `bn_migrations` VALUES ('720', '2018_06_28_153018_create_book_images_table', '1');
INSERT INTO `bn_migrations` VALUES ('721', '2018_06_28_153335_create_comments_table', '1');
INSERT INTO `bn_migrations` VALUES ('722', '2018_06_28_155035_create_addresses_table', '1');
INSERT INTO `bn_migrations` VALUES ('723', '2018_06_28_155752_create_orders_table', '1');
INSERT INTO `bn_migrations` VALUES ('724', '2018_06_28_162200_create_accounts_table', '1');
INSERT INTO `bn_migrations` VALUES ('725', '2018_06_28_163341_create_withdraws_table', '1');
INSERT INTO `bn_migrations` VALUES ('726', '2018_06_28_163709_create_msg_codes_table', '1');
INSERT INTO `bn_resources` VALUES ('1', '27', 'hyK18CshbF', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('2', '5', 'SeTqylWM10', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('3', '9', '3G9VGZrI1m', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('4', '44', 'eg7pN4ONOX', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('5', '51', 'RcfEHPMHqZ', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('6', '53', 'zFi2aKbAtu', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('7', '12', 'RFMNgx8kpB', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('8', '34', 'QVqAsVs9um', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('9', '53', 'Ic1kbAeqHL', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('10', '6', 'cEzMawdLXw', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('11', '6', 'lK8sLV8VnK', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('12', '11', 'g0NPTV3K82', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('13', '47', 'KauqhWeGRd', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('14', '37', 'vQzaZZNLHB', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('15', '48', 'hufDiEdO1E', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('16', '46', 'YU1M4wrBHC', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('17', '47', 'WPFQ5T69A1', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('18', '49', 'UPLGNUR5rz', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('19', '39', 'XfoXvHuv6o', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('20', '41', 'RzsqfJRCxR', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('21', '31', 'VN2Nj8TSgp', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('22', '5', '1CIQ1e91Gd', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('23', '31', '7IRX0tC19L', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('24', '37', 'DZhMXk7fG5', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('25', '37', 'Uk3zdy4sxO', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('26', '18', '7ZC7dfZTo1', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('27', '7', 'rU2KXu1BHY', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('28', '5', 'QJnB6xPFMD', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('29', '50', 'djgwDneyCp', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('30', '21', '8bPJxSiqnZ', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('31', '16', 'UJXPuNKcR2', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('32', '15', 'YQNFrbR3br', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('33', '4', '9lnSoJUf6G', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('34', '10', 'w2w9jQyy0D', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('35', '4', 'PqcCeMG0h1', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('36', '45', 'Pd9wF3STu7', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('37', '13', '7LBMy5Xvlg', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('38', '43', 'kS2PVNB9nw', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('39', '33', 'UhpIUfFoPq', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('40', '45', 'bSHgoDfq9Y', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('41', '30', 'VApFd62mLR', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('42', '23', 'lTqiLZYxUJ', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('43', '42', 'cqhR8jc5wM', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('44', '19', 'QnM5ze9jEB', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('45', '28', 'jPniNbs9Tc', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('46', '29', 'rCHJ3s3tC7', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('47', '52', 'MZdrNWb3cI', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('48', '48', 'j6meuWXohM', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('49', '46', 'v6RcXKTgeT', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('50', '20', 'v0GUwvyHDJ', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('51', '27', 'b8JppbERjm', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('52', '35', 'smz4rVJFq1', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('53', '11', 'kFFOIIbUEO', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('54', '45', 'Viw3QaL4aA', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_resources` VALUES ('55', '11', '6f3Riq66Sm', 'sss', '1', '33', '1', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('1', '5', '7', '1', 'Miss', 'Aspernatur aperiam maxime cum et deleniti deleniti atque. Dolores exercitationem et ab. Ea quia esse nobis consequatur itaque sed voluptatibus.', 'Prof. Jace Marquardt', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('2', '5', '3', '3', 'Dr.', 'Esse aliquam molestiae aut aspernatur. Earum nostrum aut ea ipsa. Cum voluptate ullam veniam perferendis. Velit quo odit nisi laudantium asperiores.', 'Devan McLaughlin', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('3', '4', '4', '2', 'Mrs.', 'Adipisci cum iusto illum. Qui omnis quod iste doloribus repellat aut itaque voluptatem. Rerum nemo aliquam eveniet sit et quisquam consectetur.', 'Mrs. Era Jakubowski MD', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('4', '1', '2', '1', 'Dr.', 'Asperiores ipsum quis minus repudiandae neque aut. Hic animi quia et excepturi voluptatem itaque. Omnis mollitia dolores maiores sint id. Recusandae harum sed sequi quia.', 'Mrs. Pinkie Jones MD', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('5', '3', '2', '4', 'Mr.', 'Quod dolorem quam magnam ut et. Qui impedit fugit consequuntur illo animi molestiae. Sed omnis voluptatum dignissimos debitis sed pariatur.', 'Elta Swaniawski', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('6', '3', '5', '5', 'Ms.', 'Rem qui molestiae eum et. Explicabo corrupti est asperiores incidunt aperiam. Beatae voluptates consequatur neque eum doloremque omnis. Harum dolores est necessitatibus sint ut necessitatibus.', 'Gus Watsica', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('7', '4', '5', '4', 'Miss', 'Iste porro quia recusandae natus. Id qui veritatis quisquam et aliquam non cumque. Quod sunt fugiat autem. Sint recusandae quo sed voluptas.', 'Maureen Fritsch', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('8', '2', '3', '1', 'Dr.', 'Earum quam dolorem reprehenderit voluptatem qui natus et. Ipsa temporibus id cumque est. Deleniti rerum voluptatem asperiores cum sed omnis. Aut molestiae hic eos quas et accusamus.', 'Mark Mohr Jr.', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('9', '1', '1', '4', 'Prof.', 'Quis ut placeat aut porro placeat impedit facilis sint. Qui earum voluptate et. Excepturi dolore numquam culpa repudiandae autem dolores. Blanditiis non laudantium harum ut aut.', 'Miss Delphine Kshlerin', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('10', '4', '1', '1', 'Mr.', 'Id perspiciatis molestiae et et. Totam sed corporis dolores rem voluptas. Totam atque nulla non minima et. Quasi nesciunt repellat explicabo exercitationem officiis accusantium.', 'Shea Macejkovic', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('11', '4', '1', '2', 'Prof.', 'Aut qui hic voluptas maiores voluptatem omnis. Vero tenetur eius ut iste aperiam. Inventore consequatur et veritatis dolor. Earum neque a in earum et.', 'Marielle Berge', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('12', '2', '6', '1', 'Mrs.', 'Et cumque nesciunt sed reiciendis repellendus non. Explicabo iste inventore non non aut voluptas.', 'Cielo Larkin', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('13', '2', '3', '3', 'Dr.', 'Sed vel a illum quod maxime deleniti possimus. Non aut temporibus blanditiis ut non non et. Incidunt aliquid tenetur cumque beatae. Dolorem rerum officiis error distinctio est doloribus.', 'Agustina Stroman', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('14', '2', '2', '1', 'Prof.', 'Consectetur at eligendi fugit qui optio consequuntur. Mollitia nesciunt est placeat iusto accusamus itaque. Consectetur nobis ipsam et est eligendi eius officiis.', 'Ms. Gabrielle Rau', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('15', '5', '7', '5', 'Miss', 'Voluptates architecto quia porro eos incidunt. Non sint quia ullam sed rerum. Qui sint impedit aut.', 'Kaleb Stehr', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('16', '1', '4', '4', 'Ms.', 'Qui et mollitia libero. Distinctio ut unde minima est id. Ut rerum et illum adipisci est facere. Qui recusandae numquam sed optio similique porro incidunt.', 'Garret Grady', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('17', '5', '5', '5', 'Prof.', 'Non minus qui iste est. Sit autem est laudantium quidem. Molestiae harum sit ut in ex voluptatem reprehenderit.', 'Craig Kub MD', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('18', '1', '6', '5', 'Dr.', 'Aliquam ut sapiente excepturi expedita. Quam nam qui voluptas necessitatibus velit ipsum. Nostrum est voluptas et exercitationem aut.', 'Verna Glover', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('19', '3', '1', '1', 'Prof.', 'Sunt omnis distinctio placeat qui sequi reprehenderit ut. Aliquid quis quos dicta velit. Illo laboriosam maiores perferendis velit eum magni.', 'Dr. Tracey Anderson Jr.', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('20', '1', '1', '3', 'Dr.', 'Dolorum quam quae saepe quia natus. Commodi excepturi molestiae sint consequuntur. Molestiae ut occaecati minima aut ut dicta aut omnis.', 'Jasmin Cartwright', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('21', '1', '5', '5', 'Mr.', 'Et dolores hic ea quis tempore sequi qui. Doloremque deserunt sed minus culpa. Est excepturi sapiente molestiae iste odio. Voluptas dolor hic harum optio.', 'Wyman Legros', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('22', '5', '7', '4', 'Ms.', 'Vitae sit dolore expedita sunt. Dicta rem mollitia dolores recusandae tenetur. Sapiente quod et occaecati beatae quam sed. Et et ea non aut.', 'Jailyn Russel', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('23', '1', '1', '1', 'Ms.', 'Tempore est omnis sit molestiae aspernatur laborum. Enim dolorum ad accusamus earum sed doloremque. Amet qui cum id iusto sint placeat.', 'Jazlyn Kulas', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('24', '3', '4', '2', 'Dr.', 'Quia quia nihil voluptas cumque. Et sunt omnis optio dolorum dolore. Dolor sed soluta quod qui est id. Explicabo minus dignissimos similique voluptas asperiores atque.', 'Amani Balistreri', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('25', '4', '5', '3', 'Dr.', 'Quasi molestiae et veritatis sint quae blanditiis et porro. Debitis deserunt accusamus eius magnam qui magni doloremque. Nesciunt fugit magni est dignissimos fugiat. Unde sed vero qui aut.', 'Alysa Skiles', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('26', '2', '7', '1', 'Ms.', 'Quod explicabo consequatur omnis ratione sint et. Consectetur voluptas expedita adipisci tempore autem incidunt et. Odio commodi vel voluptate ullam sit aut.', 'Rebeka Howell', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('27', '4', '4', '3', 'Mr.', 'Velit neque et enim natus totam. Aut est ipsam odio corrupti et quia. Nihil corrupti recusandae perferendis ut. Eligendi occaecati repellendus vitae.', 'Neha Cormier', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('28', '3', '4', '1', 'Prof.', 'Deleniti beatae magni qui. Neque corrupti qui recusandae et maxime. Occaecati odio voluptatum impedit et placeat et tempore impedit. Reiciendis expedita et totam omnis odio consequatur recusandae.', 'Dr. Miles Mante', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('29', '5', '7', '2', 'Prof.', 'Voluptatem adipisci dicta eaque qui in. Dolores ipsum earum dolorum repudiandae. Sapiente fuga voluptatibus aspernatur voluptatem. Sit quis modi dolor officia.', 'Dr. Axel Hammes', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('30', '4', '5', '1', 'Dr.', 'Sunt consequatur quas dolor libero. Autem corrupti delectus molestiae saepe qui eum. Ex asperiores aut ex.', 'Sasha Keebler V', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('31', '1', '7', '5', 'Miss', 'Sit culpa in maxime nobis. Repudiandae iusto modi dolorum ea totam qui pariatur. Molestias maxime debitis vel nemo dolores dolores.', 'Dr. Wilfredo Hermiston', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('32', '2', '7', '1', 'Dr.', 'Dolorum et recusandae vel deserunt suscipit quod. Ducimus pariatur adipisci omnis corporis qui. Quo occaecati corporis accusamus velit.', 'Lucio Rutherford PhD', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('33', '4', '7', '4', 'Prof.', 'Excepturi rerum dignissimos sint dolore ea. Corporis eligendi magnam pariatur qui quas. Dignissimos consequatur veniam illum dolorem eaque doloribus. Qui alias corporis dolores itaque libero.', 'Laurel Emmerich', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('34', '5', '1', '3', 'Mrs.', 'Aut quo labore ad sit minima nam ad. Enim et pariatur architecto magni. Est qui modi adipisci dolorem exercitationem repellat. Voluptas minima in libero exercitationem labore aliquid autem.', 'Mrs. Cheyenne Ruecker', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('35', '1', '7', '2', 'Mrs.', 'Accusamus cum quia omnis cumque at quis ea itaque. Quo non fuga atque numquam fugiat mollitia. Saepe recusandae corporis et quam natus.', 'Tiffany Kovacek', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('36', '5', '5', '5', 'Mr.', 'Rerum et aut delectus laudantium. Distinctio dignissimos est doloremque consequatur enim neque necessitatibus. Omnis placeat et non aut earum voluptatem non.', 'Ambrose Grant', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('37', '4', '5', '3', 'Mr.', 'Qui ut explicabo laborum quis distinctio non. Alias totam rem ut quibusdam sit esse. Hic facere rerum fugiat. Est tempora corrupti odit quos et velit et quia.', 'Mckayla Runte', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('38', '5', '4', '4', 'Mrs.', 'Dolorem exercitationem qui cupiditate cupiditate. Debitis qui dolor et reprehenderit. Et ab delectus qui ut dolorum.', 'Macie O\'Kon', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('39', '1', '7', '1', 'Dr.', 'Sed voluptas magni neque modi in aut. Et voluptas reiciendis numquam. Iusto labore eius quia iste in consequatur.', 'Harold Deckow', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('40', '5', '2', '4', 'Ms.', 'Suscipit ipsam numquam facere quo qui excepturi. Id natus qui maiores explicabo. Rerum corrupti totam quia praesentium autem commodi.', 'Mrs. Delta Heathcote II', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('41', '2', '7', '1', 'Prof.', 'Consequatur aut quia laborum beatae molestiae fugiat. Possimus sed voluptatum debitis eum pariatur quasi ad est. Aspernatur atque autem id est.', 'Dr. Monroe Murray DVM', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('42', '4', '5', '4', 'Ms.', 'Quidem earum deleniti debitis eius sed deserunt. Ut tempore molestiae vel consectetur placeat velit. Dicta animi minima aut et ex nostrum non aut. Aut exercitationem est non nemo architecto qui.', 'Mr. Tremayne Borer II', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('43', '5', '1', '3', 'Mrs.', 'Quibusdam ipsum deserunt qui ratione consectetur deserunt dolorum. Quibusdam vel facilis et et assumenda. Rerum aut dolorem pariatur et velit impedit. Aperiam asperiores aut quisquam ea debitis.', 'Kennedi Zulauf', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('44', '3', '5', '5', 'Ms.', 'Non delectus velit hic quis sunt nemo. Provident dolorem ex velit debitis dolor amet. Dolorum voluptatem nostrum corporis maxime commodi sed ipsam quia. Quo in qui itaque.', 'Leonor Rice MD', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('45', '2', '3', '1', 'Prof.', 'Eaque labore quos esse et omnis. Ea magnam eius impedit minima esse. Eligendi provident excepturi et dolores nisi ad.', 'Jayne Fisher', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('46', '2', '3', '5', 'Mr.', 'Rerum ea quam ut nihil aperiam. Autem voluptatem vero sunt dicta id officiis. Ut eos non inventore doloribus sint voluptatem.', 'Bette Will', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('47', '1', '6', '1', 'Miss', 'Quia qui blanditiis est vero aut natus magnam. Occaecati perspiciatis eum ipsa consequatur id possimus.', 'Mrs. Jazmyn Huels DVM', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('48', '4', '4', '4', 'Mrs.', 'Ut mollitia sapiente possimus ut. Inventore molestias ut in tenetur. Ratione omnis nemo fuga maiores non sint. Vero nihil libero sapiente est rem.', 'Ms. Carolina Lueilwitz', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('49', '5', '5', '3', 'Ms.', 'Omnis ab soluta unde ducimus libero minus incidunt. Aut magni quia deleniti. Corrupti quia praesentium labore est.', 'Madisyn Mohr', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('50', '4', '7', '3', 'Miss', 'Et provident praesentium possimus. Velit labore sit et sed ut quae non. Et deserunt eius sed explicabo perspiciatis.', 'Prof. Reagan Macejkovic DVM', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('51', '2', '1', '2', 'Mrs.', 'Cum sequi quia officia officia voluptatem dolorem. Et nesciunt voluptatem hic quam perferendis rerum itaque. Nemo sit dignissimos dolorem qui sit accusantium.', 'Wava Schinner', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('52', '3', '4', '3', 'Prof.', 'Alias ut veniam voluptates. Perspiciatis cupiditate provident consequatur et tempore. Nemo ipsam dolorem incidunt odio mollitia maxime. Nihil enim nostrum velit officia quo ratione.', 'Wilburn Ruecker', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('53', '2', '3', '3', 'Miss', 'Voluptatem corrupti minus in quo. Voluptatum ut sit neque. Rerum eum ab voluptas omnis consectetur. Architecto sint id eligendi ab doloribus dolor.', 'Jody Krajcik', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('54', '1', '1', '3', 'Mr.', 'Et eos dolorem a. Tempora laudantium placeat dignissimos eum voluptas.', 'Josh Connelly', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_topics` VALUES ('55', '4', '4', '1', 'Mr.', 'Aliquam temporibus aliquam eos quidem doloremque excepturi repellendus. Et est quaerat beatae omnis et voluptas.', 'Mrs. Janice Schneider', 'ss', '30.00', '1', '1', '0', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('1', '23', 'RQA1ZjUYy7', 'Stone Ryan', 'Trisha Grimes', '0guVPE4uPX', '0', '13111111111', 'eipjwyXbQs', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('2', '40', '4cgMiUWDaZ', 'Palma Johnson', 'Adella Pfannerstill II', 'AUs5uMOBmJ', '1', '13111111111', 'HFrSjHF295', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('3', '85', 'nlN1kzQJMI', 'Margarete Stehr Jr.', 'Dr. Samir Sauer', '8eeTtnmLZ8', '1', '13111111111', 'GTZaT9Ucdo', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('4', '4', 'qe9nt9VNSZ', 'Fidel Champlin', 'Rod Strosin', 'mqi4cBCqG1', '0', '13111111111', 'W1i2WXaiB0', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('5', '54', 'S1DRncKqsS', 'Pansy Price PhD', 'Noe Schuppe', 'jJFgr6BX5i', '1', '13111111111', 'CKZqlmY0pj', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('6', '71', 'fqWLDyv3cz', 'Erna Weimann', 'Edgar Langosh Sr.', 'NxVsYfN46q', '0', '13111111111', 'lLMCi1Dieo', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('7', '97', '6RkG5UH5A6', 'Loyal Grady', 'Sylvester Roberts', '5DBcgKlQTf', '1', '13111111111', 'iRdhitisiA', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('8', '32', 'o56fQZstoO', 'River Thompson', 'Jaqueline Koch', '8UGsaLj368', '1', '13111111111', 'mnPQ9BFd8Y', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('9', '67', 'fAj4hVKXoQ', 'Kenyatta Stroman I', 'Ike Cronin', 'gZX56xZXur', '1', '13111111111', 'z7mpoUmFck', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('10', '91', 'MPDnVAiwVw', 'Prof. Serena Wuckert Sr.', 'Jettie Torphy', 'eeaxYvyZ06', '0', '13111111111', 'G4zz0i5YNQ', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('11', '93', 'x8dBOaKPig', 'Clemens Quitzon', 'Joan Botsford', '7FiqgnVDb6', '0', '13111111111', 'jcjgYDk0C6', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('12', '4', 'bJ1MCbM46P', 'Dr. Emmet Doyle', 'Lavada Borer', '55i2huvVgl', '1', '13111111111', 'TfZnQ3THvA', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('13', '51', 'fskmvSqDio', 'Mr. Kendall Walter', 'Amanda Wisozk', 'wzfUJRju33', '0', '13111111111', 'nLH1FFeI4S', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('14', '73', 'XFf0SNQque', 'Kianna Reilly', 'Mason Turner III', 'MzqLnWlctU', '0', '13111111111', 'p43A5oNbRp', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('15', '67', '7IpegenrFT', 'Brandon Smitham', 'Mateo Carter', '3fGdNX5Qwt', '1', '13111111111', 'QOAkEhbPgM', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('16', '69', '04TbZgOgGT', 'Dr. Elvis Klocko', 'Avery Bruen', 'ZTBSRvaPzA', '0', '13111111111', 'Y3nigo4Jup', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('17', '36', 'Efyw84A5Ph', 'Alford Rolfson IV', 'Amelie Yost', 'M76IGXZRAr', '0', '13111111111', 'zFjLpKZp6x', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('18', '14', 'Lv2sJSuP8Q', 'Quincy McDermott', 'Dewayne Skiles', 'w0tg9vmK2A', '0', '13111111111', 'rUECkhQ0MY', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('19', '12', 'T1ZS8KqcGG', 'Bryce Stokes', 'Alejandrin Weissnat Sr.', 'uVdYfPbu7d', '0', '13111111111', 'fgn3w5vno7', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('20', '20', 'f3zb89xpTT', 'Marques Haag PhD', 'Daisha Howe', 'G6vBRuRfcx', '0', '13111111111', 'cJLIf3MGQt', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('21', '63', 'wBfMKzNARo', 'Fabiola Mosciski', 'Dayna Goodwin', '3k6Q0fUyVZ', '1', '13111111111', 'X6i5NJ1ahw', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('22', '18', 'xED8ch3Twe', 'Rosamond Runolfsson', 'Fred Nader', 'rvsDGE1m8B', '0', '13111111111', 'QDU1oqReVi', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('23', '76', 'Y2EuvVJwdE', 'Jorge Vandervort', 'Justine Braun', 'oSRw0HFU2D', '0', '13111111111', 'eGnX7ToGi2', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('24', '58', 'pIpLmylVUz', 'Mr. Dayne Medhurst', 'Beth Denesik', 'U9QiB4d4z2', '0', '13111111111', 'HkqUUbYTLr', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('25', '67', 'spb6zEcRJd', 'Dr. Turner Little', 'Miss Stacey Wiza IV', 'lKmKiBwhhp', '1', '13111111111', 'tOYNGa9iRh', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('26', '78', 'vKRnGRoGiU', 'Trenton Koss', 'Sincere Cronin III', 'o2BbH11q91', '0', '13111111111', 'FCOH6oTUWm', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('27', '31', 'I2xdZvEMjR', 'Harold Bartoletti', 'Emory Senger DVM', 'AP68oxX6YM', '0', '13111111111', 'JdtBhH3sOl', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('28', '64', 'zFoB1ZZWRm', 'Adolf Mayert MD', 'Kaleigh Weissnat', 'FWD7B1s6cb', '0', '13111111111', 'z1aXzSCMFs', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('29', '11', 'iuLmhNVyk5', 'Ms. Hannah Walker III', 'Dr. Heather Daugherty', 'UnE6iRr1ii', '0', '13111111111', 'I5xdPh8G81', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('30', '43', 'fpiW5KfkVB', 'Leslie Kunze', 'Prof. Martine Hayes Sr.', 'vkWliM3DNP', '0', '13111111111', 'maAaUfBdMU', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('31', '56', 'MtTvrOJXlb', 'Pierre Hudson', 'Adelia Hackett', 'BSiD5uLG8a', '0', '13111111111', 'kXKpfXpd3Z', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('32', '91', 'TQtstz9iwi', 'Mr. Logan Baumbach MD', 'Miss Dora Rath DDS', 'HRfKjwB2ks', '1', '13111111111', 'rveu6qJplL', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('33', '51', '4pPHa3lTsM', 'Declan Grant', 'Malvina Kassulke', 'Qqx6Pvvywy', '0', '13111111111', 'pvawL9F3EP', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('34', '86', 'slnGsHhN7A', 'Ashly Durgan', 'Prof. Maybelle Lowe', 'osqoPWlmmx', '1', '13111111111', 'eptTPwzyR9', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('35', '70', '0dvCT2wx3J', 'Nico Tremblay', 'Zaria McLaughlin', 'iSSgL7QD0M', '1', '13111111111', 'wPnHlhTvsL', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('36', '10', 'gXKmYeFGER', 'Boris Carroll', 'Adam Zulauf', 'Cd4D3O2JQF', '1', '13111111111', 'f7GIik4IlF', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('37', '57', '6RTcGY8h35', 'Newell Kuhlman', 'Mr. Emanuel Jones', '2Z7L60Hi34', '1', '13111111111', 'QNozuLofe3', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('38', '34', 'bJKSPeBD5C', 'Prof. Brennon Schuster', 'Orlando Christiansen', '7JQRBIgL6q', '1', '13111111111', 'o9uBgnwFsY', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('39', '30', 'sW2ticwrq8', 'Assunta West IV', 'Patsy Sawayn', '6mZRnYIytA', '0', '13111111111', 'tMI6hZ91M3', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('40', '29', '2xmTQjIoA7', 'Ms. Jayne Nicolas', 'Prof. Jarrell Schuster', 'CpbANRkyA4', '0', '13111111111', 'zD8szsWDsN', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('41', '69', '5Mw2Y53xms', 'Brenna Volkman', 'Barbara Kozey', 'JKjwIxhLwt', '0', '13111111111', 'nTxWsLuRGH', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('42', '64', 's107Mv04rk', 'Ellen Ondricka', 'Melyna Luettgen', 'T8poS9gaIL', '1', '13111111111', 'Eh5LbMISdK', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('43', '42', 'X1jvlFzFgB', 'Marjorie Frami II', 'May Pfeffer', 'Vz6xD3mSZK', '1', '13111111111', '6YdNKIfFnn', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('44', '81', 'RTgqF98I4o', 'Prof. Deanna Pfeffer II', 'Athena Rolfson', 'WSTLlVUyTV', '0', '13111111111', 'zZEi6LeXLd', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('45', '37', 'kdS4cMTu20', 'Elenora Bogisich', 'Montana Bernier II', 'yeYp0pAaCY', '0', '13111111111', 'lTQPWeV1ez', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('46', '63', 'V7VOoPFJNs', 'Margot Bradtke', 'Alvis Bednar', 'rU2odovsC5', '0', '13111111111', 'Rmbv8nJNNb', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('47', '82', '6sp4UdrRlT', 'Mr. Graham Muller', 'Orie Watsica', 'b2bybLiS6G', '1', '13111111111', 'rUkeZQiaQd', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('48', '62', 'JlBpxjQ2Im', 'Prof. Major Kling PhD', 'Torey Moore Sr.', 'fzcY1qGeCw', '0', '13111111111', '9x5VeBhzyr', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('49', '25', 'OaJJfaLUmA', 'Royce Spencer', 'Jayme Hermann', 'lfuusd3oZD', '1', '13111111111', 'cEvi6j4n9h', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
INSERT INTO `bn_users` VALUES ('50', '50', 'FeFkA8ui4F', 'Keenan Heathcote', 'Prof. Oran Runolfsdottir III', 'Nnfe0PkYhB', '1', '13111111111', 'H2jBfF2mg5', '0.00', '2018-06-29 14:58:30', '2018-06-29 14:58:30');
